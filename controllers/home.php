<?php
session_start();

class home extends Controller {

	function __construct() {
		parent::__construct();
	}

	public function home() {
		$this -> view -> render('', 'Front');
	}

	public function contact() {
		$this -> view -> render('', 'Contact');
	}

	public function hello() {
		echo "hello";
	}

	public function login() {
		$this -> view -> render('', 'Login');
	}

	public function register() {
		if (isset($_SESSION['registerfee'])) {
			session_unset($_SESSION['registerfee']);
		}

		if ($_POST['fromBank'] == 'Other') {
			$_SESSION['registerfee']['fromBank'] = $_POST['fromBank'] . ':' . $_POST['other'];
		} else {
			$_SESSION['registerfee']['fromBank'] = $_POST['fromBank'];
		}

		$_SESSION['registerfee']['toBank'] = $_POST['toBank'];
		$_SESSION['registerfee']['payment'] = $_POST['payment'];
		$_SESSION['registerfee']['dateInSlip'] = $_POST['dateInSlip'];
		$_SESSION['registerfee']['timeInSlip'] = $_POST['timeInSlip'];

		$_SESSION['registerfee']['picture'] = $this -> uploadFile('', 'views/upload/receipt/', $_FILES['imageupload']);

		$this -> view -> render('', 'Register');
	}

	public function registerPre() {
		if (isset($_GET['refer'])) {
			$data['refer'] = $this -> model -> referProfile($_GET['refer']);
		}
		$data = null;
		$this -> view -> render($data, 'RegisterPre');
	}

	public function buy() {
		$this -> view -> render('', 'Buy');
	}

	public function buyOrder() {
		$this -> view -> render('', 'BuyOrder');
	}

	public function receipt($no = NULL) {
		if ($no == NULL) {
			$this -> redirect('../receiptInvalid');
		}

		$data['receipt'] = $this -> model -> receiptPayment($no);

		if ($data['receipt'] == NULL) {
			$this -> redirect('../receiptInvalid');
		}

		$this -> view -> render($data, 'Receipt');

	}

	public function receiptInvalid() {
		$this -> view -> render('', 'ReceiptInvalid');
	}

	/*******************************************************************************************
	 *
	 * Below is operation for home controller. Anything about page(aka view) please add above
	 *
	 *******************************************************************************************/

	public function loginCheck() {
		$username = $_POST['username'];
		$password = $_POST['password'];

		$isLogin = $this -> model -> login($username, $password);

		if ($isLogin == TRUE) {
			if ($_SESSION['rank'] == 'Admin') {
				header("location:../admin");
			} else if ($_SESSION['rank'] == 'User') {
				header("location:../user");
			} else if ($_SESSION['rank'] == 'Pending') {
				header("location:../home/login?register=pending");
			} else {
				session_destroy();
				header("location:../home/login?display=error");
			}

		} else {
			header("location:../home/login?display=error");
		}
	}

	public function registerUser() {
		$checkUsername = $this -> model -> checkUsername($_POST['username']);
		$checkEmail = $this -> model -> checkEmail($_POST['email']);
		$checkRefer = $this -> model -> checkReferalCode($_POST['refer']);

		if ($checkEmail == 'true' OR $checkUsername == 'true' OR $checkRefer == 'false') {
			$search = array('-', '+');
			$this -> model -> registerUser(strtoupper($_POST['fname']), strtoupper($_POST['lname']), $_POST['email'], str_replace($search, '', $_POST['contactNo']), $_POST['username'], $_POST['password'], $_POST['refer'], $_POST['gender'], $_POST['local'], str_replace($search, '', $_POST['icno']), strtoupper($_POST['add1']), strtoupper($_POST['add2']), strtoupper($_POST['city']), $_POST['postcode'], strtoupper($_POST['state']), $_POST['country'], $_POST['bankaccount'], $_POST['accountno']);
			// $login = $this -> model -> login($_POST['username'], $_POST['password']);

			$login_id = $this -> model -> searchID($_POST['username'], $_POST['password']);

			//add into registerfee
			$this -> model -> addtofee($_SESSION['registerfee']['fromBank'], $_SESSION['registerfee']['toBank'], $_SESSION['registerfee']['payment'], $_SESSION['registerfee']['dateInSlip'], $_SESSION['registerfee']['timeInSlip'], $_SESSION['registerfee']['picture'], $login_id);

			//give commission
			// $this -> model -> giveCommissionto($_POST['refer']);

			//email plak
			//email
			$msg = 'Hi! Thank you for registering with Biopro Fuel Saver Agent program. We are in the middle of processing your account. Once we are done, we will notify you through email, thanks! Have a nice day.';
			$msg = wordwrap($msg, 70);

			$headers = "From: hello@biopropetrol.com";

			mail($_POST['email'], "BioPetrol", $msg, $headers);

			//redirect
			$this -> redirect('../home/registerPre?register=success');
		} else {
			echo "username or password already exist!";
		}

	}

	public function logout() {
		session_destroy();
		header("location:../");
	}

	/*
	 * checking purpose
	 */
	public function checkUsername() {
		echo $this -> model -> checkUsername($_POST['username']);
	}

	public function checkEmail() {
		echo $this -> model -> checkEmail($_POST['email']);
	}

	public function checkReferalCode() {
		echo $this -> model -> checkReferalCode($_POST['refer']);
	}

	public function buyPayment() {
		$search = array('-', '+');

		if ($_POST['fromBank'] == 'Other') {
			$fromBank = $_POST['fromBank'] . ':' . $_POST['other'];
		} else {
			$fromBank = $_POST['fromBank'];
		}

		if ($_FILES['imageupload']['error'] == 0) {
			$image_name = $this -> uploadFile('', 'views/upload/receipt/', $_FILES['imageupload']);
		}

		if ($_POST['package'] == '7.00') {
			$package = '1 Bottle - RM 7.00';
		} else if ($_POST['package'] == '28.00') {
			$package = '4 Bottle - RM 28.00';
		} else if ($_POST['package'] == '56.00') {
			$package = '8 Bottle - RM 56.00';
		} else if ($_POST['package'] == '14.00') {
			$package = '2 Bottle - RM 14.00';
		} else if ($_POST['package'] == '90.00') {
			$package = '15 Bottle - RM 90.00';
		}

		$receipt_number = $this -> model -> uploadPayment($fromBank, $_POST['toBank'], $_POST['dateInSlip'], $_POST['timeInSlip'], $_POST['payment'], $image_name, strtoupper($_POST['fullname']), str_replace($search, '', $_POST['phone']), $_POST['email'], strtoupper($_POST['add1']), strtoupper($_POST['add2']), $_POST['postcode'], strtoupper($_POST['city']), strtoupper($_POST['state']), $package, $_POST['shipfee']);

		//email
		$msg = 'Hi! Thank you for purchasing Biopro Fuel Saver. For your convenienve, here is the receipt link: http://biopropetrol.com/home/receipt/' . $receipt_number . ' .Thank you and have a nice day!';
		$msg = wordwrap($msg, 70);

		$headers .= "Reply-To: Biopropetrol Admin <hello@biopropetrol.com>\r\n";
		$headers .= "Return-Path: Biopropetrol Admin <hello@biopropetrol.com>\r\n";
		$headers .= "From: Biopropetrol Admin <hello@biopropetrol.com>\r\n";
		$headers .= "Organization: Biopetrol\r\n";
		$headers .= "Content-Type: text/plain\r\n";

		// $headers = "From: hello@biopropetrol.com";

		mail($_POST['email'], "BioPetrol", $msg, $headers);

		$this -> redirect('../home/receipt/' . $receipt_number . '?order=success');
	}

	/*
	 * email function
	 */
	public function contactRegister() {
		$this -> model -> contactRegister($_POST['name'], $_POST['email'], $_POST['contactNo']);

		// PREPARE THE BODY OF THE MESSAGE

		$message = '<html><body>';
		$message .= '<img src="http://biopro.wiredin.my/images/header-logo.svg" alt="BioPro" />';
		$message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
		$message .= "<tr style='background: #eee;'><td><strong>Name:</strong> </td><td>" . strip_tags($_POST['name']) . "</td></tr>";
		$message .= "<tr><td><strong>Email:</strong> </td><td>" . strip_tags($_POST['email']) . "</td></tr>";
		// $message .= "<tr><td><strong>Type of Change:</strong> </td><td>" . strip_tags($_POST['typeOfChange']) . "</td></tr>";
		// $message .= "<tr><td><strong>Urgency:</strong> </td><td>" . strip_tags($_POST['urgency']) . "</td></tr>";
		// $message .= "<tr><td><strong>URL To Change (main):</strong> </td><td>" . $_POST['URL-main'] . "</td></tr>";
		// $addURLS = $_POST['addURLS'];
		// if (($addURLS) != '') {
		// $message .= "<tr><td><strong>URL To Change (additional):</strong> </td><td>" . strip_tags($addURLS) . "</td></tr>";
		// }
		// $curText = htmlentities($_POST['curText']);
		// if (($curText) != '') {
		// $message .= "<tr><td><strong>CURRENT Content:</strong> </td><td>" . $curText . "</td></tr>";
		// }
		$message .= "<tr><td><strong>Contact Number:</strong> </td><td>" . strip_tags($_POST['contactNo']) . "</td></tr>";
		$message .= "</table>";
		$message .= "</body></html>";

		//  MAKE SURE THE "FROM" EMAIL ADDRESS DOESN'T HAVE ANY NASTY STUFF IN IT

		// $pattern = "/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/i";
		// if (preg_match($pattern, trim(strip_tags($_POST['req-email'])))) {
		// $cleanedFrom = trim(strip_tags($_POST['req-email']));
		// } else {
		// return "The email address you entered was invalid. Please try again!";
		// }

		//   CHANGE THE BELOW VARIABLES TO YOUR NEEDS

		$to = $_POST['email'];

		$subject = 'Website Change Reqest';

		// $headers = "From: " . $cleanedFrom . "\r\n";
		//$headers .= "Reply-To: " . strip_tags($_POST['req-email']) . "\r\n";
		$headers = "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

		if (mail($to, $subject, $message, $headers)) {
			//echo 'Your message has been sent.';
		} else {
			//echo 'There was a problem sending the email.';
		}

		$this -> redirect('../home/contact');

	}

	public function redirect($link) {
		echo '<script> window.location="' . $link . '"; </script> ';
	}

	public function uploadFile($type, $target, $file) {
		//$allowedExts = array("gif", "jpeg", "jpg", "png");
		$temp = explode(".", $file["name"]);
		$extension = end($temp);

		if ($file["error"] > 0) {
			echo "Return Code: " . $file["error"] . "
<br>
";
		} else {
			$name = time() . '.' . $extension;
			move_uploaded_file($file["tmp_name"], $target . $name);
			return $name;
		}
	}

}
?>