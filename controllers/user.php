<?php session_start();

class user extends Controller {

	public $linkshare = 'http://biopropetrol.com/home/registerPre?refer=';
	// public $linkshare = 'https://www.google.com/search?q=';

	function __construct() {
		parent::__construct();

		if (isset($_SESSION['username'])) {
			if ($_SESSION['rank'] != "User") {
				header("location:../home/logout");
			}
		} else {
			header("location:home");
		}
	}

	public function user() {
		$data['linkshare'] = $this -> linkshare;

		$data['profile'] = $this -> model -> myProfile();

		$data['firstCount'] = $this -> model -> totalRecruit($data['profile']['referral_code']);
		$data['firstTree'] = $this -> model -> recruit($data['profile']['referral_code']);

		// echo $data['firstCount'];
		$data['secondTree'] = array();
		$total = 0;
		foreach ($data['firstTree'] as $key) {
			$sum = $this -> model -> totalRecruit($key['referral_code']);
			$total = $total + $sum;
			$begin = $data['secondTree'];
			$new = $this -> model -> recruit($key['referral_code']);
			$data['secondTree'] = array_merge($begin, $new);
		}
		$data['secondCount'] = $total;

		$this -> view -> render($data, 'User/User_Base', 'Dashboard');
	}

	public function stock() {
		$data['pricing'] = $this -> model -> getPricing();
		$data['mystock'] = $this -> model -> getTotalStock();
		$this -> view -> render($data, 'User/User_Base', 'Stock');
	}

	public function stockBuy() {
		$data['pricing'] = $this -> model -> getPricing();
		$data['currentBioProStock'] = $this -> model -> getBioproTotalStock();
		$data['stockAvailable'] = $this -> model -> getBioproStockAvailable();
		$this -> view -> render($data, 'User/User_Base', 'StockBuy');
	}

	public function stockTransaction() {
		$data['pendingTransaction'] = $this -> model -> stockTransaction('Pending');
		$this -> view -> render($data, 'User/User_Base', 'StockTransaction');
	}

	public function recruit() {
		$data['profile'] = $this -> model -> myProfile();

		$data['firstCount'] = $this -> model -> totalRecruit($data['profile']['referral_code']);
		$data['firstTree'] = $this -> model -> recruit($data['profile']['referral_code']);

		// echo $data['firstCount'];
		$data['secondTree'] = array();
		$total = 0;
		foreach ($data['firstTree'] as $key) {
			$sum = $this -> model -> totalRecruit($key['referral_code']);
			$total = $total + $sum;
			$begin = $data['secondTree'];
			$new = $this -> model -> recruit($key['referral_code']);
			$data['secondTree'] = array_merge($begin, $new);
		}
		$data['secondCount'] = $total;

		$this -> view -> render($data, 'User/User_Base', 'Recruit');
	}

	public function commision() {
		$date = date('Y-m');
		$data['commissionList'] = $this -> model -> myCommission();
		$data['total'] = $this -> model -> myCommissionThisMonth($date);
		$this -> view -> render($data, 'User/User_Base', 'Commision');
	}

	public function commissionLog() {
		if (isset($_GET['month'])) {
			if ($_GET['month'] != NULL) {
				$date = '01-' . $_GET['month'];
				$month = explode('-', $_GET['month']);
				$data['date'] = date('F Y', strtotime($month[1] . '-' . $month[0]));
				$date = '01-' . $_GET['month'];
			} else {
				$date = date('Y-m');
				$data['date'] = date('F Y', strtotime(date('Y-m') . " -1 month"));
			}
		} else {
			$date = date('Y-m');
			$data['date'] = date('F Y', strtotime(date('Y-m') . " -1 month"));
		}

		$data['log'] = $this -> model -> getUserCommission($date);
		$data['total'] = $this -> model -> myCommissionThisMonth($date);

		$this -> view -> render($data, 'User/User_Base', 'Commision_byUser');
	}

	public function profile($page = NULL) {
		$data['linkshare'] = $this -> linkshare;

		$data['profile'] = $this -> model -> myProfile();

		if ($page == NULL) {
			$this -> view -> render($data, 'User/User_Base', 'Profile');
		} else if ($page == 'profile') {
			$this -> view -> render($data, 'User/User_Base', 'Profile');
		}else if ($page == 'password') {
			$this -> view -> render($data, 'User/User_Base', 'Profile');
		}else if ($page == 'profile') {
			$this -> view -> render($data, 'User/User_Base', 'Profile');
		}
	}

	/*******************************************************************************************
	 *
	 * Below is operation for user controller. Anything about page(aka view) please add above
	 *
	 *******************************************************************************************/

	public function updateStock() {
		$this -> model -> addNewStock($_POST['total']);
		$this -> redirect('../user/stock');
	}

	public function updateProfie() {
		$this -> model -> updateProfile($_POST['fname'], $_POST['lname'], $_POST['email'], $_POST['phone'], $_POST['gender'], $_POST['local'], $_POST['icno'], $_POST['add1'], $_POST['add2'], $_POST['city'], $_POST['postcode'], $_POST['state'], $_POST['country']);
		$this -> redirect('../user/profile?success=update');
	}

	public function updateAccount() {
		$this -> model -> updateAccount($_POST['bankaccount'], $_POST['accountno']);
		$this -> redirect('../user/profile?success=account');
	}

	public function updatePicture() {
		$name = $this -> uploadFile('', 'views/upload/', $_FILES['picture']);
		$this -> model -> updatePicture($name);
		$this -> redirect('../user/profile?success=picture');
	}

	public function generateProductKey() {
		$template = 'XX99-XX99-99XX-99XX-XXXX-99XX';
		$k = strlen($template);
		$sernum = '';
		for ($i = 0; $i < $k; $i++) {
			switch($template[$i]) {
				case 'X' :
					$sernum .= chr(rand(65, 90));
					break;
				case '9' :
					$sernum .= rand(0, 9);
					break;
				case '-' :
					$sernum .= '-';
					break;
			}
		}

		//check first in db.. if there is no any same than proceed if not than repeat again

		return $sernum;
	}

	public function uploadPayment() {
		if ($_POST['fromBank'] == 'Other') {
			$fromBank = $_POST['fromBank'] . ':' . $_POST['other'];
		} else {
			$fromBank = $_POST['fromBank'];
		}

		if ($_FILES['imageupload']['error'] == 0) {
			$image_name = $this -> uploadFile('', 'views/upload/receipt/', $_FILES['imageupload']);
		}

		$this -> model -> uploadPayment($fromBank, $_POST['toBank'], $_POST['dateInSlip'], $_POST['timeInSlip'], $_POST['payment'], $image_name, $_POST['amount']);

		$this -> redirect('../user/stockTransaction');
	}

	/*******************************************************************************************
	 *
	 * below is for other function use
	 *
	 *******************************************************************************************/
	public function redirect($link) {
		echo '
<script>
window.location="' . $link . '";
</script> ';
	}

	public function uploadFile($type, $target, $file) {
		//$allowedExts = array("gif", "jpeg", "jpg", "png");
		$temp = explode(".", $file["name"]);
		$extension = end($temp);

		if ($file["error"] > 0) {
			echo "Return Code: " . $file["error"] . "
<br>
";
		} else {
			$name = time() . '.' . $extension;
			move_uploaded_file($file["tmp_name"], $target . $name);
			return $name;
		}
	}

}
?>