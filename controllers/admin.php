<?php
session_start();

class admin extends Controller {

	function __construct() {
		parent::__construct();

		if (isset($_SESSION['username'])) {
			if ($_SESSION['rank'] != "Admin") {
				header("location:../home/logout");
			}
		} else {
			header("location:home");
		}
	}

	public function admin() {
		$data['lastMonthSell'] = $this -> model -> getLastMonthSell();
		$data['yesterdaySell'] = $this -> model -> getYesterdaySell();
		$data['currentStock'] = $this -> model -> getTotalStock();
		$data['stockPerDay'] = $this -> model -> getAverageSellPerDay();
		$this -> view -> render($data, 'Admin/Admin_Base', 'Dashboard');
	}

	public function stock() {
		if (isset($_GET['update'])) {
			if ($_GET['update'] == 0) {
				$data['updateMessage'] = $this -> successMsg('Stock had been updated ');
			} else {
				$data['updateMessage'] = $this -> errorsMsg('Cannot update the stock ');
			}
		} else {
			$data['updateMessage'] = '';
		}

		$data['currentStock'] = $this -> model -> getTotalStock();
		$data['available'] = $this -> model -> getAvailabilityStock();

		$this -> view -> render($data, 'Admin/Admin_Base', 'Stock');
	}

	public function user() {
		$data['totalAgent'] = $this -> model -> getTotalAgent();
		$data['agent'] = $this -> model -> getAllAgent();
		$this -> view -> render($data, 'Admin/Admin_Base', 'User');
	}

	public function manual() {
		$data['totalPending'] = $this -> model -> getTotalPendingTransaction();
		$data['alltransaction'] = $this -> model -> stockTransaction('Pending');
		$this -> view -> render($data, 'Admin/Admin_Base', 'Manual');
	}

	public function manualFront() {
		$data['totalPending'] = $this -> model -> getTotalPendingManualFront();
		$data['alltransaction'] = $this -> model -> getManualFront('Pending');
		$this -> view -> render($data, 'Admin/Admin_Base', 'ManualFront');
	}

	public function manualFrontHistory() {
		if (isset($_GET['status'])) {
			$data['status'] = $_GET['status'];
			$data['alltransaction'] = $this -> model -> getManualFront($data['status']);
		} else {
			$data['status'] = '';
			$data['alltransaction'] = $this -> model -> getManualFrontNot('Pending');
		}
		$data['alltransaction'] = $this -> model -> getManualFrontNot('Pending');
		$this -> view -> render($data, 'Admin/Admin_Base', 'ManualFrontHistory');
	}

	public function commission() {
		// $date = date('Y-m-d H:i:s');
		if (isset($_GET['month'])) {
			if ($_GET['month'] != NULL) {
				$date = '01-' . $_GET['month'];
				$month = explode('-', $_GET['month']);
				$data['date'] = date('F Y', strtotime($month[1] . '-' . $month[0]));
			} else {
				$date = date('Y-m') . " -1 month";
				$data['date'] = date('F Y', strtotime(date('Y-m') . " -1 month"));
			}
		} else {
			$date = date('Y-m') . " -1 month";
			$data['date'] = date('F Y', strtotime(date('Y-m') . " -1 month"));
		}

		$data['commissionList'] = $this -> model -> commissionListBy($date);
		// $data['commissionList'] = $this -> model -> commissionList();
		$data['commTotal'] = $this -> model -> commissionTotal($date);
		$this -> view -> render($data, 'Admin/Admin_Base', 'Commission');
	}

	public function commissionRate() {
		if (isset($_GET['update'])) {
			if ($_GET['update'] == 0) {
				$data['updateMessage'] = $this -> successMsg('Commission Rate had been updated ');
			} else {
				$data['updateMessage'] = $this -> errorsMsg('Cannot update the Commission Rate  ');
			}
		} else {
			$data['updateMessage'] = '';
		}

		$data['pricing'] = $this -> model -> getPricing();
		$this -> view -> render($data, 'Admin/Admin_Base', 'CommissionRate');
	}

	public function commissionHistory() {

		$this -> view -> render('', 'Admin/Admin_Base', 'CommissionHistory');
	}

	public function addNewAgent() {
		$this -> view -> render('', 'Admin/Admin_Base', 'AddNewAgent');
	}

	public function manualHistory() {
		if (isset($_GET['status'])) {
			$data['status'] = $_GET['status'];
			$data['alltransaction'] = $this -> model -> stockTransaction($data['status']);
		} else {
			$data['status'] = '';
			$data['alltransaction'] = $this -> model -> stockTransactionNot('Pending');
		}

		$this -> view -> render($data, 'Admin/Admin_Base', 'ManualHistory');
	}

	public function profile($login_id) {
		$data['agent'] = $this -> model -> getAgent($login_id);
		$this -> view -> render($data, 'Admin/Admin_Base', 'Profile');
	}

	public function profileCommission($id) {
		$data['id'] = $id;
		$data['agent'] = $this -> model -> getAgent($id);
		$data['commissionList'] = $this -> model -> myCommission($id);
		$this -> view -> render($data, 'Admin/Admin_Base', 'ProfileCommission');
	}

	public function commissionLog($id) {
		$date = $_GET['month'];
		$month = explode('-', $_GET['month']);
		$data['date'] = date('F Y', strtotime($month[1] . '-' . $month[0]));

		$date = '01-' . $_GET['month'];

		$data['log'] = $this -> model -> getUserCommission($id, $date);
		$data['total'] = $this -> model -> myCommissionThisMonth($id, $date);

		$data['id'] = $id;

		$this -> view -> render($data, 'Admin/Admin_Base', 'CommissionLog');
	}

	public function profileRecruit() {
		$this -> view -> render('', 'Admin/Admin_Base', 'ProfileRecruit');
	}

	public function profileStock() {
		$this -> view -> render('', 'Admin/Admin_Base', 'ProfileStock');
	}

	public function stockSearch() {
		$this -> view -> render('', 'Admin/Admin_Base', 'StockSearch');
	}

	public function registrationApproval() {
		$data['listRegister'] = $this -> model -> listRegisterApproval('Pending');
		$this -> view -> render($data, 'Admin/Admin_Base', 'RegistrationApproval');
	}

	public function registrationHist() {
		$this -> view -> render('', 'Admin/Admin_Base', 'RegistrationHist');
	}

	/*******************************************************************************************
	 *
	 * Below is operation for admin controller. Anything about page(aka view) please add above
	 *
	 *******************************************************************************************/

	public function updateProfile($id) {
		if ($_POST['bankaccount'] == 'Other') {
			$fromBank = $_POST['bankaccount'] . ':' . $_POST['other'];
		} else {
			$fromBank = $_POST['bankaccount'];
		}

		$this -> model -> updateProfile($id, $_POST['fname'], $_POST['lname'], $_POST['email'], $_POST['contactNo'], $_POST['gender'], $_POST['local'], $_POST['icno'], $_POST['add1'], $_POST['add2'], $_POST['city'], $_POST['postcode'], $_POST['state'], $_POST['country'], $fromBank, $_POST['accno']);

		$this -> redirect('../profile/' . $id . '?success=profile');
	}

	public function approveRegister($id) {
		//approve register
		$this -> model -> approveRegister($id);

		//get user profile
		$user = $this -> model -> getAgent($_GET['login_id']);
		//add to their father and father
		$this -> model -> addCommissionRegister($user);
		// $this -> model -> addCommission($user);

		//change login status
		$this -> model -> approveLogin($_GET['login_id']);

		//email
		$msg = 'Hi! Congratulations! We are pleased to inform you that your application of Biopro Fuel Saver Agent have been accepted! You can login here http://biopropetrol.com/home/login Thanks and have a nice day!';
		$msg = wordwrap($msg, 70);
		$headers = "From: hello@biopropetrol.com";

		mail($_GET['email'], "BioPetrol", $msg, $headers);

		$this -> redirect('../registrationApproval');
	}

	public function rejectRegister($id) {
		//reject register
		$this -> model -> rejectRegister($id);

		//remove in login
		//remove in profile
		$this -> model -> deleteUser($_GET['login_id']);

		//email
		$msg = 'Hi, We humbly apologised that your application to become Biopro Fuel Saver agent have been kindly rejected. There might be inconsistencies in payment or insufficent bank-in amount, please contact us for clarification. Contact us here: http://biopropetrol.com/home/contact Thanks, and have a nice day!';
		$msg = wordwrap($msg, 70);
		$headers = "From: hello@biopropetrol.com";

		mail($_GET['email'], "BioPetrol", $msg, $headers);

		$this -> redirect('../registrationApproval');
	}

	public function updateStock($operation) {
		//add or minus
		$errorMsg = $this -> model -> updateStock($_POST['newstock'], $operation);
		$this -> redirect('../stock?update=' . $errorMsg);

	}

	public function updateStatusStock() {
		$errorMsg = $this -> model -> updateAvailabilityStock();
		$this -> redirect('../admin/stock');
	}

	public function updateCommissionRate($lvl) {
		$errorMsg = $this -> model -> updateCommissionRate($_POST['amount'], $lvl);
		$this -> redirect('../commissionRate?update=' . $errorMsg);
	}

	public function registerUser() {
		$checkUsername = $this -> model -> checkUsername($_POST['username']);
		$checkEmail = $this -> model -> checkEmail($_POST['email']);

		if ($checkEmail == 'true' OR $checkUsername == 'true') {
			$search = array('-', '+');
			$this -> model -> registerUser(strtoupper($_POST['fname']), strtoupper($_POST['lname']), $_POST['email'], str_replace($search, '', $_POST['contactNo']), $_POST['username'], $_POST['password'], $_POST['gender'], $_POST['local'], str_replace($search, '', $_POST['icno']), strtoupper($_POST['add1']), strtoupper($_POST['add2']), strtoupper($_POST['city']), $_POST['postcode'], strtoupper($_POST['state']), $_POST['country']);
			// $login = $this -> model -> login($_POST['username'], $_POST['password']);
			$this -> redirect('../admin/user');
		} else {
			echo "already have username or password";
		}
	}

	public function deleteThisUser($login_id) {
		$this -> model -> deleteUser($login_id);
		// $this -> popupMsg('try');
		$this -> redirect('../user');

	}

	/*
	 * checking purpose
	 */
	public function checkUsername() {
		echo $this -> model -> checkUsername($_POST['username']);
	}

	public function checkEmail() {
		echo $this -> model -> checkEmail($_POST['email']);
	}

	public function updatePicture($login_id) {
		$name = $this -> uploadFile('', 'views/upload/', $_FILES['picture']);
		$this -> model -> updatePicture($name, $login_id);
		$this -> redirect('../profile/' . $login_id . '?success=picture');
	}

	public function acceptTransaction($id) {
		$this -> model -> acceptTransaction($id);
		$this -> redirect('../manual');
	}

	public function rejectTransaction($id) {
		$this -> model -> rejectTransaction($id);
		$this -> redirect('../manual');
	}

	public function acceptTransactionFront($id) {
		$this -> model -> acceptTransactionFront($id);
		$this -> redirect('../manualFront');
	}

	public function rejectTransactionFront($id) {
		$this -> model -> rejectTransactionFront($id);
		$this -> redirect('../manualFront');
	}

	/*******************************************************************************************
	 *
	 * bootstrap compatible
	 *
	 *******************************************************************************************/
	public function successMsg($message) {
		$msg = '
	 	<div class="alert alert-success alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">
				&times;
			</button>
			' . $message . '
		</div>
	 	';
		return $msg;
	}

	public function errorsMsg($message) {
		$msg = '
	 	<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">
				&times;
			</button>
			' . $message . '
		</div>
	 	';
		return $msg;
	}

	public function redirect($link) {
		echo '<script> window.location="' . $link . '"; </script> ';
	}

	// public function popupMsg($message) {
	// echo '<script> bootbox.alert("' . $message . '");</script> ';
	// }

	public function uploadFile($type, $target, $file) {
		//$allowedExts = array("gif", "jpeg", "jpg", "png");
		$temp = explode(".", $file["name"]);
		$extension = end($temp);

		if ($file["error"] > 0) {
			echo "Return Code: " . $file["error"] . "<br>";
		} else {
			$name = time() . '.' . $extension;
			move_uploaded_file($file["tmp_name"], $target . $name);
			return $name;
		}
	}

}
?>