<?php

class admin_model extends Model {

	public $errorMessage = 0;

	public $table_profile = 'profile';
	public $table_login = 'login';
	public $table_stock = 'stock';
	public $table_buy_history = 'buy_history';
	public $table_pricing = 'pricing';
	public $table_commission = 'commission';
	public $table_commission_log = 'commission_log';
	public $table_payment = 'manual_payment';
	public $table_buy_order = 'buy_order';
	public $table_registerfee = 'registerfee';

	public $table_biopro_stock = 'biopro_stock';

	function __construct() {
		parent::__construct();
	}

	public function registerUser($fname, $lname, $email, $contactNo, $username, $password, $gender, $local, $icno, $add1, $add2, $city, $postcode, $state, $country) {

		$words = explode(" ", $fname . ' ' . $lname);
		$acronym = "";
		foreach ($words as $w) {
			$acronym .= $w[0];
		}
		$generate_refer = $acronym . '_' . time();
		$rank = 'User';

		$refer = '';

		$sql = 'INSERT INTO ' . $this -> table_login . ' (username, password, rank) VALUES (:username, :password, :rank)';
		$params = array(':username' => $username, ':password' => md5($password), ':rank' => $rank);
		$this -> easydb -> stmt($sql, $params);

		$login_id = $this -> easydb -> lastinsertedid();

		$sql = 'INSERT INTO ' . $this -> table_profile . '(login_id, fname, lname, email, phone, referral_code, parent_code, gender, malaysian, icpass, address1, address2, city, postcode, state, country) VALUES (:login_id, :fname, :lname, :email, :phone, :referral_code, :parent_code, :gender, :local, :ic, :address1, :address2, :city, :postcode, :state, :country)';
		$params = array(':login_id' => $login_id, ':fname' => $fname, ':lname' => $lname, ':email' => $email, ':phone' => $contactNo, ':referral_code' => $generate_refer, ':parent_code' => $refer, ':gender' => $gender, ':local' => $local, ':ic' => $icno, ':address1' => $add1, ':address2' => $add2, ':city' => $city, ':postcode' => $postcode, ':state' => $state, ':country' => $country);
		$this -> easydb -> stmt($sql, $params);

		// echo $sql;
		// print_r($params);

		$sql = 'INSERT INTO ' . $this -> table_stock . ' (login_id, total) VALUES (:login_id, 0)';
		$params = array(':login_id' => $login_id);
		$this -> easydb -> stmt($sql, $params);

	}

	public function deleteUser($login_id) {
		$sql = 'DELETE FROM ' . $this -> table_profile . ' WHERE login_id=:login_id';
		$params = array(':login_id' => $login_id);
		$this -> easydb -> stmt($sql, $params);

		$sql = 'DELETE FROM ' . $this -> table_login . ' WHERE id=:login_id';
		$this -> easydb -> stmt($sql, $params);
	}

	public function updateStock($total, $operation) {
		$currentStock = $this -> getTotalStock();
		$error = 0;

		if ($operation == 'add') {
			$newStock = $currentStock + $total;
		} else if ($operation == 'minus') {
			if ($currentStock >= $total) {
				$newStock = $currentStock - $total;
			} else {
				$error = 1;
				// too small to minus
			}
		}

		if ($error == 0) {
			$sql = 'UPDATE ' . $this -> table_biopro_stock . ' SET total_stock = :new';
			$params = array(':new' => $newStock);
			$this -> easydb -> stmt($sql, $params);
		}

		return $error;
	}

	public function updateProfile($id, $fname, $lname, $email, $contactNo, $gender, $local, $icno, $add1, $add2, $city, $postcode, $state, $country, $fromBank, $accno) {
		$sql = 'UPDATE ' . $this -> table_profile . ' SET fname=:fname, lname=:lname, email=:email, phone=:phone, gender=:gender, malaysian=:local, icpass=:icno, address1=:add1, address2=:add2, city=:city, postcode=:postcode, state=:state, country=:country, account_bank=:accbank, account_number=:acc_no WHERE login_id = :id';
		$params = array(':id' => $id, ':fname' => $fname, ':lname' => $lname, ':email' => $email, ':phone' => $contactNo, ':gender' => $gender, ':local' => $local, ':icno' => $icno, ':add1' => $add1, ':add2' => $add2, ':city' => $city, ':postcode' => $postcode, ':state' => $state, ':country' => $country, ':accbank' => $fromBank, ':acc_no' => $accno);
		$data = $this -> easydb -> stmt($sql, $params);
	}

	public function updateAvailabilityStock() {
		$error = 0;

		$status = $this -> getAvailabilityStock();
		if ($status == 'TRUE') {
			$new = 'FALSE';
		} else if ($status == 'FALSE') {
			$new = 'TRUE';
		} else {
			$error = 1;
			//if there bug in future counter
		}

		if ($error == 0) {
			$sql = 'UPDATE ' . $this -> table_biopro_stock . ' SET disable_stock = :new';
			$params = array(':new' => $new);
			$this -> easydb -> stmt($sql, $params);
		}

		return $error;

	}

	public function updateCommissionRate($newCR, $lvl) {
		$error = 0;

		if ($lvl == 1) {
			$sql = 'UPDATE ' . $this -> table_pricing . ' SET comm_lvl2 = :new';
		} else if ($lvl == 2) {
			$sql = 'UPDATE ' . $this -> table_pricing . ' SET comm_lvl3 = :new';
		} else {
			$error = 1;
			//if there bug in future counter
		}

		if ($error == 0) {
			$params = array(':new' => $newCR);
			$this -> easydb -> stmt($sql, $params);
		}

		return $error;
	}

	//get
	public function getAvailabilityStock() {
		$sql = 'SELECT * FROM ' . $this -> table_biopro_stock . ' WHERE id = 1';
		$data = $this -> easydb -> stmt($sql);
		return $data[0]['disable_stock'];
	}

	public function getTotalStock() {
		$sql = 'SELECT * FROM ' . $this -> table_biopro_stock . ' WHERE id = 1';
		$data = $this -> easydb -> stmt($sql);
		return $data[0]['total_stock'];
	}

	public function getPricing() {
		$sql = 'SELECT * FROM ' . $this -> table_pricing . ' WHERE id = 1';
		$data = $this -> easydb -> stmt($sql);
		return $data[0];
	}

	public function getYesterdaySell() {
		$sql = 'SELECT SUM(total_stock_buy) counter FROM ' . $this -> table_buy_history . ' WHERE date_buy >= DATE_SUB(CURDATE(), INTERVAL 1 DAY) AND date_buy < CURDATE()';
		$data = $this -> easydb -> stmt($sql);
		return $data[0]['counter'];
	}

	public function getLastMonthSell() {
		$sql = 'SELECT SUM(total_stock_buy) counter FROM ' . $this -> table_buy_history . ' WHERE YEAR(date_buy) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH) AND MONTH(date_buy) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH)';
		$data = $this -> easydb -> stmt($sql);
		return $data[0]['counter'];
	}

	public function getAverageSellPerDay() {
		$sql = 'SELECT (SUM(total_stock_buy)/COUNT(DISTINCT(DATE( date_buy )))) as average FROM ' . $this -> table_buy_history;
		$data = $this -> easydb -> stmt($sql);
		return $data[0]['average'];
	}

	public function stockTransaction($status) {
		$sql = 'SELECT p.*, pr.fname, pr.lname, pr.email, pr.phone, pr.account_bank, pr.account_number FROM ' . $this -> table_payment . ' p,' . $this -> table_profile . ' pr WHERE p.status LIKE :status AND p.login_id=pr.login_id ';
		$params = array(':status' => $status);
		$data = $this -> easydb -> stmt($sql, $params);
		return $data;
	}

	public function stockTransactionNot($status) {
		$sql = 'SELECT p.*, pr.fname, pr.lname, pr.email, pr.phone, pr.account_bank, pr.account_number FROM ' . $this -> table_payment . ' p,' . $this -> table_profile . ' pr WHERE p.status NOT LIKE :status AND p.login_id=pr.login_id ';
		$params = array(':status' => $status);
		$data = $this -> easydb -> stmt($sql, $params);
		return $data;
	}

	/*
	 * Checking username or email
	 */

	public function checkUsername($username) {
		$result = 'true';

		$sql = 'SELECT id FROM ' . $this -> table_login . ' WHERE username=:username';

		$params = array(':username' => $username);
		$data = $this -> easydb -> stmt($sql, $params);

		if ($data != NULL) {
			$result = 'false';
		}
		return $result;
	}

	public function checkEmail($email) {
		$result = 'true';

		$sql = 'SELECT id FROM ' . $this -> table_profile . ' WHERE email=:email';

		$params = array(':email' => $email);
		$data = $this -> easydb -> stmt($sql, $params);

		if ($data != NULL) {
			$result = 'false';
		}
		return $result;
	}

	public function getTotalAgent() {
		$status = 'Pending';
		$sql = 'SELECT COUNT(p.id) as counter FROM ' . $this -> table_profile . ' p, ' . $this -> table_login . ' l WHERE l.id=p.login_id AND l.rank NOT LIKE :status';
		$params = array(':status' => $status);
		$data = $this -> easydb -> stmt($sql, $params);
		return $data[0]['counter'];
	}

	public function getAllAgent() {
		$status = 'Pending';
		$sql = 'SELECT p.* FROM ' . $this -> table_profile . ' p, ' . $this -> table_login . ' l WHERE l.id=p.login_id AND l.rank NOT LIKE :status';
		$params = array(':status' => $status);
		$data = $this -> easydb -> stmt($sql, $params);
		return $data;
	}

	public function getAgent($login_id) {
		$sql = 'SELECT * FROM ' . $this -> table_profile . ' p, ' . $this -> table_login . ' l WHERE p.login_id = l.id AND p.login_id = :login_id';
		$params = array(':login_id' => $login_id);
		$data = $this -> easydb -> stmt($sql, $params);
		return $data[0];
	}

	public function getTotalPendingTransaction() {
		$status = 'Pending';
		$sql = 'SELECT COUNT(id) as total FROM ' . $this -> table_payment . ' WHERE status LIKE :status';
		$params = array(':status' => $status);
		$data = $this -> easydb -> stmt($sql, $params);
		return $data[0]['total'];
	}

	public function getTotalPendingManualFront() {
		$status = 'Pending';
		$sql = 'SELECT COUNT(id) as total FROM ' . $this -> table_buy_order . ' WHERE status LIKE :status';
		$params = array(':status' => $status);
		$data = $this -> easydb -> stmt($sql, $params);
		return $data[0]['total'];
	}

	public function getManualFront($status) {
		$sql = 'SELECT * FROM ' . $this -> table_buy_order . ' WHERE status LIKE :status';
		$params = array(':status' => $status);
		$data = $this -> easydb -> stmt($sql, $params);
		return $data;
	}

	public function getManualFrontNot($status) {
		$sql = 'SELECT * FROM ' . $this -> table_buy_order . ' WHERE status NOT LIKE :status';
		$params = array(':status' => $status);
		$data = $this -> easydb -> stmt($sql, $params);
		return $data;
	}

	public function updatePicture($name, $login_id) {
		$sql = 'UPDATE ' . $this -> table_profile . ' SET picture_name=:picture_name WHERE login_id = :id';
		$params = array(':id' => $login_id, ':picture_name' => $name);
		$data = $this -> easydb -> stmt($sql, $params);
	}

	public function listRegisterApproval($status) {
		$sql = 'SELECT p.*, rf.* FROM ' . $this -> table_registerfee . ' rf,' . $this -> table_profile . ' p WHERE p.login_id=rf.login_id AND rf.status=:status';
		$params = array(':status' => $status);
		$data = $this -> easydb -> stmt($sql, $params);
		// print_r($data);
		return $data;
	}

	public function rejectTransaction($id) {
		$sql = 'UPDATE ' . $this -> table_payment . ' SET status=:status WHERE id=:id';
		$params = array(':id' => $id, ':status' => 'Reject');
		$this -> easydb -> stmt($sql, $params);

		//get total stock
		$sql = 'SELECT stock_quantity FROM ' . $this -> table_payment . ' WHERE id=:id ';
		$params = array(':id' => $id);
		$data = $this -> easydb -> stmt($sql, $params);

		$stock_from_payment = $data[0]['stock_quantity'];

		//update stock in database
		$bioprostock = $this -> getTotalStock();

		$new_bioprostock = $bioprostock + $stock_from_payment;

		// echo $new_bioprostock . ' = ' . $bioprostock . ' + ' . $stock_from_payment;

		$sql = 'UPDATE ' . $this -> table_biopro_stock . ' SET total_stock=:stock WHERE id = 1;';
		$params = array(':stock' => $new_bioprostock);
		$this -> easydb -> stmt($sql, $params);

	}

	public function acceptTransaction($id) {
		$sql = 'UPDATE ' . $this -> table_payment . ' SET status=:status WHERE id=:id';
		$params = array(':id' => $id, ':status' => 'Accept');
		$this -> easydb -> stmt($sql, $params);

		//get payment info
		$sql = 'SELECT * FROM ' . $this -> table_payment . ' WHERE id=:id ';
		$params = array(':id' => $id);
		$data = $this -> easydb -> stmt($sql, $params);

		//update kat history plak
		$sql = 'INSERT INTO ' . $this -> table_buy_history . ' (login_id, total_stock_buy, total_price) VALUES (:login_id, :total_stock, :total_price)';
		$params = array(':login_id' => $data[0]['login_id'], ':total_stock' => $data[0]['stock_quantity'], ':total_price' => $data[0]['amount_payment']);
		$this -> easydb -> stmt($sql, $params);

		//check sekiranya commision dia sudah dikira atau belum untuk bulan ni
		$result = FALSE;
		$date = date('Y-m-d H:i:s');
		$sql = 'SELECT id,total_commission FROM ' . $this -> table_commission . ' WHERE MONTH(date_getCommission) = MONTH(:date) AND YEAR(date_getCommission) = YEAR(:date) AND login_id = :login_id';
		$params = array(':login_id' => $data[0]['login_id'], ':date' => $date);
		$isHave = $this -> easydb -> stmt($sql, $params);

		if ($isHave != NULL) {
			$result = TRUE;
			$this -> testMessage('Already have commission');
		} else {
			$this -> testMessage('Do not have commission yet.');
		}

		$total_commision = 0;
		$getPricing = $this -> getPricing();
		$commLvl2 = $getPricing['comm_lvl2'];
		$commLvl3 = $getPricing['comm_lvl3'];

		//masuk commision kat diri sendiri
		if ($result == FALSE) {
			$total_commision = $commLvl3;
			$sql = 'INSERT INTO ' . $this -> table_commission . ' (login_id, total_commission) VALUES (:login_id, :totalCommission)';
			$params = array(':login_id' => $data[0]['login_id'], ':totalCommission' => $total_commision);
			$this -> testMessage('Insert new commission');
		} else {
			$total_commision = $isHave[0]['total_commission'] + $commLvl3;
			$sql = 'UPDATE ' . $this -> table_commission . ' SET total_commission = :totalCommission WHERE MONTH(date_getCommission) = MONTH(:date) AND YEAR(date_getCommission) = YEAR(:date) AND login_id = :login_id';
			$params = array(':login_id' => $data[0]['login_id'], ':totalCommission' => $total_commision, ':date' => $date);
			$this -> testMessage('Update last commission');
		}

		$this -> easydb -> stmt($sql, $params);

		//huruf pelik : ∂

		//cek dia ada parent x?
		$haveParents = FALSE;
		$sql = 'SELECT parent_code FROM ' . $this -> table_profile . ' WHERE login_id=:login_id ';
		$params = array(':login_id' => $data[0]['login_id']);
		$data2 = $this -> easydb -> stmt($sql, $params);

		if ($data2[0]['parent_code'] != NULL) {
			//masukkan commission kat parent dia
			$this -> testMessage('Have parent');

			//dapatkan login_id parent
			$sql = 'SELECT login_id FROM ' . $this -> table_profile . ' WHERE referral_code LIKE :code';
			$params = array(':code' => $data2[0]['parent_code']);
			$parent = $this -> easydb -> stmt($sql, $params);

			if ($parent != NULL) {
				$this -> testMessage('Parent still alive');

				//check sekiranya commision dia sudah dikira atau belum untuk bulan ni
				$result = FALSE;
				$date = date('Y-m-d H:i:s');
				$sql = 'SELECT id,total_commission FROM ' . $this -> table_commission . ' WHERE MONTH(date_getCommission) = MONTH(:date) AND YEAR(date_getCommission) = YEAR(:date) AND login_id = :login_id';
				$params = array(':login_id' => $parent[0]['login_id'], ':date' => $date);
				$isHave = $this -> easydb -> stmt($sql, $params);

				if ($isHave != NULL) {
					$result = TRUE;
					$this -> testMessage('Parent already have commission for this month');
				} else {
					$this -> testMessage('Parent dont have any commision for this month');
				}

				$total_commision = 0;

				if ($result == FALSE) {
					$total_commision = $commLvl2;
					$sql = 'INSERT INTO ' . $this -> table_commission . ' (login_id, total_commission) VALUES (:login_id, :totalCommission)';
					$params = array(':login_id' => $parent[0]['login_id'], ':totalCommission' => $total_commision);
					$this -> testMessage('Insert new commission for parent');
				} else {
					$total_commision = $isHave[0]['total_commission'] + $commLvl2;
					$sql = 'UPDATE ' . $this -> table_commission . ' SET total_commission = :totalCommission WHERE MONTH(date_getCommission) = MONTH(:date) AND YEAR(date_getCommission) = YEAR(:date) AND login_id = :login_id';
					$params = array(':login_id' => $parent[0]['login_id'], ':totalCommission' => $total_commision, ':date' => $date);
					$this -> testMessage('Update last commission');
				}

				$this -> easydb -> stmt($sql, $params);
			}

		}//finish for parent commission
		else {
			$this -> testMessage('Do not have any parent');
		}

	}

	public function rejectTransactionFront($id) {
		$sql = 'UPDATE ' . $this -> table_buy_order . ' SET status=:status WHERE id=:id';
		$params = array(':id' => $id, ':status' => 'Reject');
		$this -> easydb -> stmt($sql, $params);
	}

	public function acceptTransactionFront($id) {
		$sql = 'UPDATE ' . $this -> table_buy_order . ' SET status=:status WHERE id=:id';
		$params = array(':id' => $id, ':status' => 'Accept');
		$this -> easydb -> stmt($sql, $params);
	}

	public function approveRegister($id) {
		$sql = 'UPDATE ' . $this -> table_registerfee . ' SET status=:status WHERE id=:id';
		$params = array(':id' => $id, ':status' => 'Accept');
		$this -> easydb -> stmt($sql, $params);
	}

	public function rejectRegister($id) {
		$sql = 'UPDATE ' . $this -> table_registerfee . ' SET status=:status WHERE id=:id';
		$params = array(':id' => $id, ':status' => 'Reject');
		$this -> easydb -> stmt($sql, $params);
	}

	public function approveLogin($id) {
		$sql = 'UPDATE ' . $this -> table_login . ' SET rank=:status WHERE id=:id';
		$params = array(':id' => $id, ':status' => 'User');
		$this -> easydb -> stmt($sql, $params);
	}

	public function commissionList() {
		$date = date('Y-m-d H:i:s', strtotime(date('Y-m') . " -1 month"));
		$sql = 'SELECT p.*, c.total_commission FROM ' . $this -> table_profile . ' p, ' . $this -> table_commission . ' c WHERE MONTH(c.date_getCommission) = MONTH(:date) AND YEAR(c.date_getCommission) = YEAR(:date) AND p.login_id=c.login_id';
		$params = array(':date' => $date);
		$data = $this -> easydb -> stmt($sql, $params);
		return $data;
	}

	public function commissionListBy($date) {
		$date = date('Y-m-d H:i:s', strtotime($date));
		$sql = 'SELECT p.*, c.total_commission FROM ' . $this -> table_profile . ' p, ' . $this -> table_commission . ' c WHERE MONTH(c.date_getCommission) = MONTH(:date) AND YEAR(c.date_getCommission) = YEAR(:date) AND p.login_id=c.login_id';
		$params = array(':date' => $date);
		$data = $this -> easydb -> stmt($sql, $params);
		return $data;
	}

	public function commissionTotal($date) {
		$date = date('Y-m-d H:i:s', strtotime($date));
		$sql = 'SELECT SUM(total_commission) as totalComm FROM ' . $this -> table_commission . ' WHERE MONTH(date_getCommission) = MONTH(:date) AND YEAR(date_getCommission) = YEAR(:date)';
		$params = array(':date' => $date);
		$data = $this -> easydb -> stmt($sql, $params);
		return $data[0]['totalComm'];
	}

	// public function commissionTotal() {
	// $date = date('Y-m-d H:i:s', strtotime(date('Y-m') . " -1 month"));
	// $sql = 'SELECT SUM(total_commission) as totalComm FROM ' . $this -> table_commission . ' WHERE MONTH(date_getCommission) = MONTH(:date) AND YEAR(date_getCommission) = YEAR(:date)';
	// $params = array(':date' => $date);
	// $data = $this -> easydb -> stmt($sql, $params);
	// return $data[0]['totalComm'];
	// }

	public function addCommissionRegister($user) {
		$pricing = $this -> getPricing();
		$case = 'Commission from Register.';
		$parent_user = $this -> searchRefer($user['parent_code']);

		$isHave = $this -> checkIfHaveCommissionThisMonth($parent_user['login_id']);
		if ($isHave == FALSE) {
			$this -> addCommissionToThisMonth($parent_user['login_id'], $pricing['registercommission']);
		} else {
			$this -> updateCommissionThisMonth($parent_user['login_id'], $pricing['registercommission']);
		}

		$this -> addCommisionLogToLogin_id($case, $pricing['registercommission'], $parent_user['login_id'], $user['login_id']);
	}

	public function addCommission($user) {
		$pricing = $this -> getPricing();
		$case = 'Commission from Buying.';

		// $user = $this -> getAgent($user['login_id']);
		$parent_user = $this -> searchRefer($user['parent_code']);
		//lvl 2
		$isHave = $this -> checkIfHaveCommissionThisMonth($parent_user['login_id']);
		if ($isHave == FALSE) {
			$this -> $this -> addCommissionToThisMonth($parent_user['login_id'], $pricing['comm_lvl2']);
		} else {
			$this -> updateCommissionThisMonth($parent_user['login_id'], $pricing['comm_lvl2']);
		}
		$this -> addCommisionLogToLogin_id($case, $pricing['comm_lvl2'], $parent_user['login_id'], $user['login_id']);

		$this -> testMessage('user_id :' . $user['login_id']);
		$this -> testMessage('parent_id :' . $parent_user['login_id']);
		$this -> testMessage($parent_user['parent_code']);

		if ($parent_user['parent_code'] != NULL) {
			$this -> testMessage('Have parent');
			// $parent_parent_user = $this -> getAgent($parent_user['login_id']);
			$parent_parent_user = $this -> searchRefer($parent_user['parent_code']);
			//lvl 1
			$isHave = $this -> checkIfHaveCommissionThisMonth($parent_parent_user['login_id']);
			if ($isHave == FALSE) {
				$this -> addCommissionToThisMonth($parent_parent_user['login_id'], $pricing['comm_lvl3']);
			} else {
				$this -> updateCommissionThisMonth($parent_parent_user['login_id'], $pricing['comm_lvl3']);
			}
			$this -> addCommisionLogToLogin_id($case, $pricing['comm_lvl3'], $parent_parent_user['login_id'], $user['login_id']);
		} else {
			$this -> testMessage('Do not have parent');
		}
	}

	public function searchRefer($refer) {
		$sql = 'SELECT * FROM ' . $this -> table_profile . ' WHERE referral_code LIKE :refer';
		$params = array(':refer' => $refer);
		$data = $this -> easydb -> stmt($sql, $params);
		return $data[0];
	}

	public function addCommisionLogToLogin_id($case, $total, $to, $from) {
		$sql = 'INSERT INTO ' . $this -> table_commission_log . ' (detail, total, to_login_id, from_login_id) VALUES (:case, :total, :to, :from)';
		$params = array(':case' => $case, ':total' => $total, ':to' => $to, ':from' => $from);
		$this -> easydb -> stmt($sql, $params);

		$this -> testMessage($params);
		$this -> testMessage($sql);
	}

	public function checkIfHaveCommissionThisMonth($login_id) {
		//check sekiranya commision dia sudah dikira atau belum untuk bulan ni
		$result = FALSE;
		$date = date('Y-m-d H:i:s');
		$sql = 'SELECT id,total_commission FROM ' . $this -> table_commission . ' WHERE MONTH(date_getCommission) = MONTH(:date) AND YEAR(date_getCommission) = YEAR(:date) AND login_id = :login_id';
		$params = array(':login_id' => $login_id, ':date' => $date);
		$isHave = $this -> easydb -> stmt($sql, $params);
		if ($isHave != NULL) {
			$result = TRUE;
			$this -> testMessage('Already have commission');
		} else {
			$this -> testMessage('Do not have commission yet.');
		}

		return $result;
	}

	public function addCommissionToThisMonth($login_id, $total_commission) {
		$sql = 'INSERT INTO ' . $this -> table_commission . ' (login_id, total_commission) VALUES (:login_id, :totalCommission)';
		$params = array(':login_id' => $login_id, ':totalCommission' => $total_commission);
		$this -> testMessage('Insert new commission for parent');

		$this -> easydb -> stmt($sql, $params);

	}

	public function updateCommissionThisMonth($login_id, $total_commission) {
		$date = date('Y-m-d H:i:s');
		$sql = 'SELECT id,total_commission FROM ' . $this -> table_commission . ' WHERE MONTH(date_getCommission) = MONTH(:date) AND YEAR(date_getCommission) = YEAR(:date) AND login_id = :login_id';
		$params = array(':login_id' => $login_id, ':date' => $date);
		$user_commission = $this -> easydb -> stmt($sql, $params);

		$new_total_commision = $user_commission[0]['total_commission'] + $total_commission;

		$date = date('Y-m-d H:i:s');
		$sql = 'UPDATE ' . $this -> table_commission . ' SET total_commission = :totalCommission WHERE MONTH(date_getCommission) = MONTH(:date) AND YEAR(date_getCommission) = YEAR(:date) AND login_id = :login_id';
		$params = array(':login_id' => $login_id, ':totalCommission' => $new_total_commision, ':date' => $date);
		$this -> testMessage('Update last commission');

		$this -> easydb -> stmt($sql, $params);
	}

	public function myCommission($login_id) {
		$sql = 'SELECT * FROM ' . $this -> table_commission . ' WHERE login_id=:login_id';
		$params = array(':login_id' => $login_id);
		$data = $this -> easydb -> stmt($sql, $params);
		return $data;
	}

	public function myCommissionThisMonth($login_id, $date) {
		$date = date('Y-m-d H:i:s', strtotime($date));

		$sql = 'SELECT * FROM ' . $this -> table_commission . ' WHERE MONTH(date_getCommission) = MONTH(:date) AND YEAR(date_getCommission) = YEAR(:date) AND login_id=:login_id';
		$params = array(':login_id' => $login_id, ':date' => $date);
		$data = $this -> easydb -> stmt($sql, $params);

		$this -> testMessage($data);

		return $data[0]['total_commission'];
	}

	public function getUserCommission($login_id, $date) {

		$date = date('Y-m-d H:i:s', strtotime($date));
		$sql = 'SELECT l.*,p.fname,p.lname FROM ' . $this -> table_commission_log . ' l,' . $this -> table_profile . ' p WHERE  MONTH(date_log) = MONTH(:date) AND YEAR(date_log) = YEAR(:date) AND to_login_id = :login_id AND from_login_id=p.login_id';
		$params = array(':login_id' => $login_id, ':date' => $date);
		$data = $this -> easydb -> stmt($sql, $params);

		$this -> testMessage($sql);
		$this -> testMessage($params);
		$this -> testMessage($data);

		return $data;

	}

	// public function checkThisMonthCommisionForThisId() {
	// $result = FALSE;
	// $date = date('Y-m-d H:i:s');
	// $sql = 'SELECT id FROM ' . $this -> table_commission . ' WHERE MONTH(date_getCommission) = MONTH(:date) AND YEAR(date) = YEAR(:date) AND login_id = :login_id';
	// $params = array(':login_id' => $_SESSION['login_id'], ':date' => $date);
	// $data = $this -> easydb -> stmt($sql, $params);
	//
	// if ($data != NULL) {
	// $result = TRUE;
	// }
	//
	// return $result;
	// }

	public function testMessage($msg) {
		if ($this -> errorMessage == 1) {
			if (is_array($msg)) {
				echo '<br />msg : ';
				echo "<pre>";
				print_r($msg);
				echo "</pre>";
			} else {
				echo '<br />msg : ' . $msg;
			}

		}
	}

}
?>