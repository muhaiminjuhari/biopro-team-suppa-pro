<?php

class user_model extends Model {

	public $table_profile = 'profile';
	public $table_stock = 'stock';
	public $table_buy_history = 'buy_history';
	public $table_pricing = 'pricing';
	public $table_commission = 'commission';
	public $table_commission_log = 'commission_log';
	public $table_payment = 'manual_payment';

	public $table_biopro_stock = 'biopro_stock';

	function __construct() {
		parent::__construct();
	}

	public function myProfile() {
		$sql = 'SELECT * FROM ' . $this -> table_profile . ' WHERE login_id = :id';
		$params = array(':id' => $_SESSION['login_id']);
		$data = $this -> easydb -> stmt($sql, $params);
		return $data[0];
	}

	public function updateProfile($fname, $lname, $email, $phone, $gender, $local, $icno, $add1, $add2, $city, $postcode, $state, $country) {
		$sql = 'UPDATE ' . $this -> table_profile . ' SET fname=:fname, lname=:lname, email=:email, phone=:phone, gender=:gender, malaysian=:local, icpass=:icno, address1=:add1, address2=:add2, city=:city, postcode=:postcode, state=:state, country=:country WHERE login_id = :id';
		$params = array(':id' => $_SESSION['login_id'], ':fname' => $fname, ':lname' => $lname, ':email' => $email, ':phone' => $phone, ':gender' => $gender, ':local' => $local, ':icno' => $icno, ':add1' => $add1, ':add2' => $add2, ':city' => $city, ':postcode' => $postcode, ':state' => $state, ':country' => $country);
		$data = $this -> easydb -> stmt($sql, $params);
	}

	public function updateAccount($acc_bank, $acc_no) {
		$sql = 'UPDATE ' . $this -> table_profile . ' SET account_bank=:acc_bank, account_number=:acc_no WHERE login_id = :id';
		$params = array(':id' => $_SESSION['login_id'], ':acc_bank' => $acc_bank, ':acc_no' => $acc_no);
		$data = $this -> easydb -> stmt($sql, $params);
	}

	public function updatePicture($name) {
		$sql = 'UPDATE ' . $this -> table_profile . ' SET picture_name=:picture_name WHERE login_id = :id';
		$params = array(':id' => $_SESSION['login_id'], ':picture_name' => $name);
		$data = $this -> easydb -> stmt($sql, $params);
	}

	public function totalRecruit($referral_code) {
		$sql = 'SELECT COUNT(id) as count FROM ' . $this -> table_profile . ' WHERE parent_code = :refer';
		$params = array(':refer' => $referral_code);
		$data = $this -> easydb -> stmt($sql, $params);
		return $data[0]['count'];
	}

	public function recruit($referral_code) {
		$sql = 'SELECT * FROM ' . $this -> table_profile . ' WHERE parent_code = :refer';
		$params = array(':refer' => $referral_code);
		$data = $this -> easydb -> stmt($sql, $params);
		return $data;
	}

	public function recruitCode($referral_code) {
		$sql = 'SELECT referral_code FROM ' . $this -> table_profile . ' WHERE parent_code = :refer';
		$params = array(':refer' => $referral_code);
		$data = $this -> easydb -> stmt($sql, $params);
		return $data;
	}

	public function findParent($login_id) {
		$sql = 'SELECT parent_code FROM ' . $this -> table_profile . ' WHERE login_id = :id';
		$params = array(':login_id' => $login_id);
		$parent = $this -> easydb -> stmt($sql, $params);

		$sql = 'SELECT * FROM ' . $this -> table_profile . ' WHERE referral_code = :code';
		$params = array(':code' => $parent[0]['parent_code']);
		$data = $this -> easydb -> stmt($sql, $params);

		return $data;
	}

	public function addNewStock($total) {
		$mystock = $this -> getTotalStock();
		$pricing = $this -> getPricing();

		$newtotal = $total + $mystock;
		$price = $pricing['perstock'] * $total;

		$sql = 'UPDATE ' . $this -> table_stock . ' SET total=:total WHERE login_id = :id';
		$params = array(':id' => $_SESSION['login_id'], ':total' => $newtotal);
		$this -> easydb -> stmt($sql, $params);

		$sql = 'INSERT INTO ' . $this -> table_buy_history . ' (login_id, total_stock_buy, total_price) VALUES (:id, :total, :price)';
		$params = array(':id' => $_SESSION['login_id'], ':total' => $total, ':price' => $price);
		$this -> easydb -> stmt($sql, $params);

		//checking commission

		//if not hv this month commission yet, add one

		//else just update the old one

	}

	public function stockTransaction($status) {
		$sql = 'SELECT * FROM ' . $this -> table_payment . ' WHERE status LIKE :status AND login_id = :login_id';
		$params = array(':status' => $status, ':login_id' => $_SESSION['login_id']);
		$data = $this -> easydb -> stmt($sql, $params);
		return $data;
	}

	public function getTotalStock() {
		$sql = 'SELECT * FROM ' . $this -> table_stock . ' WHERE login_id = :id';
		$params = array(':id' => $_SESSION['login_id']);
		$data = $this -> easydb -> stmt($sql, $params);

		$total = 0;

		if (isset($data[0])) {
			$total = $data[0]['total'];
		}

		return $total;
	}

	public function getPricing() {
		$sql = 'SELECT * FROM ' . $this -> table_pricing . ' WHERE id = 1';
		$data = $this -> easydb -> stmt($sql);
		return $data[0];
	}

	public function getBioproTotalStock() {
		$sql = 'SELECT * FROM ' . $this -> table_biopro_stock . ' WHERE id = 1';
		$data = $this -> easydb -> stmt($sql);
		return $data[0]['total_stock'];
	}

	public function getBioproStockAvailable() {
		$sql = 'SELECT * FROM ' . $this -> table_biopro_stock . ' WHERE id = 1';
		$data = $this -> easydb -> stmt($sql);
		return $data[0]['disable_stock'];
	}

	public function uploadPayment($fromBank, $toBank, $dateInSlip, $timeInSlip, $payment, $image_name, $total2) {
		$newtime = strtotime($dateInSlip);
		$newformat = date('Y-m-d', $newtime);

		// echo $fromBank . ',' . $toBank . ',' . $dateInSlip . ',' . $timeInSlip . ',' . $payment . ',' . $image_name . ',' . $total2;
		$sql = 'INSERT INTO ' . $this -> table_payment . ' (login_id, payment_from, payment_to, date_payment, time_payment, amount_payment, pic_slip, stock_quantity) VALUES (:login_id, :paymentfrom, :paymentto, :date, :time, :amount, :slip, :stock)';
		$params = array(':login_id' => $_SESSION['login_id'], ':paymentfrom' => $fromBank, ':paymentto' => $toBank, ':date' => $newformat, ':time' => $timeInSlip, ':amount' => $payment, ':slip' => $image_name, ':stock' => $total2);

		$this -> easydb -> stmt($sql, $params);

		//float the stock, minus it with total
		$bioprostock = $this -> getBioproTotalStock();

		$new_bioprostock = $bioprostock - $total2;

		$sql = 'UPDATE ' . $this -> table_biopro_stock . ' SET total_stock=:stock WHERE id = 1;';
		$params = array(':stock' => $new_bioprostock);
		$this -> easydb -> stmt($sql, $params);
	}

	public function myCommission() {
		$sql = 'SELECT * FROM ' . $this -> table_commission . ' WHERE login_id=:login_id';
		$params = array(':login_id' => $_SESSION['login_id']);
		$data = $this -> easydb -> stmt($sql, $params);
		return $data;
	}

	public function getUserCommission($date) {

		$date = date('Y-m-d H:i:s', strtotime($date));
		$sql = 'SELECT l.*,p.fname,p.lname FROM ' . $this -> table_commission_log . ' l,' . $this -> table_profile . ' p WHERE  MONTH(date_log) = MONTH(:date) AND YEAR(date_log) = YEAR(:date) AND to_login_id = :login_id AND from_login_id=p.login_id';
		$params = array(':login_id' => $_SESSION['login_id'], ':date' => $date);
		$data = $this -> easydb -> stmt($sql, $params);

		return $data;

	}

	public function myCommissionThisMonth($date) {
		$date = date('Y-m-d H:i:s', strtotime($date));

		$sql = 'SELECT * FROM ' . $this -> table_commission . ' WHERE MONTH(date_getCommission) = MONTH(:date) AND YEAR(date_getCommission) = YEAR(:date) AND login_id=:login_id';
		$params = array(':login_id' => $_SESSION['login_id'], ':date' => $date);
		$data = $this -> easydb -> stmt($sql, $params);


		return $data[0]['total_commission'];
	}

	/******************************
	 * Checking and add commission
	 ******************************/
	public function checkMonthlyCom() {

	}

	public function addMonthlyCom() {

	}

	public function updateMonthlyCom() {

	}

	public function getMonthlyCom() {

	}

}
?>