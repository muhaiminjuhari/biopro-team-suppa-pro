<?php

class home_model extends Model {

	public $table_contact = 'contact';
	public $table_login = 'login';
	public $table_profile = 'profile';
	public $table_stock = 'stock';
	public $table_buy_order = 'buy_order';
	public $table_registerfee = 'registerfee';
	public $table_pricing = 'pricing';

	function __construct() {
		parent::__construct();
	}

	public function contactRegister($name, $email, $contactNo) {
		$sql = 'INSERT INTO ' . $this -> table_contact . ' (name, email, contact_number) VALUES (:name, :email, :contact)';
		$params = array(':name' => $name, ':email' => $email, ':contact' => $contactNo);
		$this -> easydb -> stmt($sql, $params);
	}

	public function registerUser($fname, $lname, $email, $contactNo, $username, $password, $refer, $gender, $local, $icno, $add1, $add2, $city, $postcode, $state, $country, $acc_bank, $acc_no) {

		$words = explode(" ", $fname . ' ' . $lname);
		$acronym = "";
		foreach ($words as $w) {
			$acronym .= $w[0];
		}
		$generate_refer = $acronym . '_' . time();
		$rank = 'Pending';

		$sql = 'INSERT INTO ' . $this -> table_login . ' (username, password, rank) VALUES (:username, :password, :rank)';
		$params = array(':username' => $username, ':password' => md5($password), ':rank' => $rank);
		$this -> easydb -> stmt($sql, $params);

		$login_id = $this -> easydb -> lastinsertedid();
		// $login_id = $this -> searchID($username, $password);

		$sql = 'INSERT INTO ' . $this -> table_profile . ' (login_id, fname, lname, email, phone, referral_code, parent_code, gender, malaysian, icpass, address1, address2, city, postcode, state, country, account_bank, account_number) VALUES (:login_id, :fname, :lname, :email, :phone, :referral_code, :parent_code, :gender, :local, :ic, :address1, :address2, :city, :postcode, :state, :country, :acc_bank, :acc_no)';
		$params = array(':login_id' => $login_id, ':fname' => $fname, ':lname' => $lname, ':email' => $email, ':phone' => $contactNo, ':referral_code' => $generate_refer, ':parent_code' => $refer, ':gender' => $gender, ':local' => $local, ':ic' => $icno, ':address1' => $add1, ':address2' => $add2, ':city' => $city, ':postcode' => $postcode, ':state' => $state, ':country' => $country, ':acc_bank' => $acc_bank, ':acc_no' => $acc_no);
		$this -> easydb -> stmt($sql, $params);

		// echo $sql;
		// print_r($params);

		$sql = 'INSERT INTO ' . $this -> table_stock . ' (login_id, total) VALUES (:login_id, 0)';
		$params = array(':login_id' => $login_id);
		$this -> easydb -> stmt($sql, $params);

	}

	public function addtofee($fromBank, $toBank, $payment, $dateInSlip, $timeInSlip, $picture, $login_id) {
		$newtime = strtotime($dateInSlip);
		$newformat = date('Y-m-d', $newtime);

		$sql = 'INSERT INTO ' . $this -> table_registerfee . ' (login_id, fromBank, toBank, payment, dateInSlip, timeInSlip, imageName) VALUES (:login_id, :fromBank, :toBank, :payment, :date, :time, :image)';
		$params = array(':login_id' => $login_id, ':fromBank' => $fromBank, ':toBank' => $toBank, ':payment' => $payment, ':date' => $newformat, ':time' => $timeInSlip, ':image' => $picture);
		$this -> easydb -> stmt($sql, $params);

	}

	public function searchID($username, $password) {
		$sql = 'SELECT id FROM ' . $this -> table_login . ' WHERE username=:username AND password=:password';

		$params = array(':username' => $username, ':password' => md5($password));
		$data = $this -> easydb -> stmt($sql, $params);
		return $data[0]['id'];
	}

	/*
	 * login and logout
	 */

	public function login($username, $password) {

		$result = FALSE;

		$sql = 'SELECT id, rank FROM ' . $this -> table_login . ' WHERE username=:username AND password=:password';

		$params = array(':username' => $username, ':password' => md5($password));
		$data = $this -> easydb -> stmt($sql, $params);

		if ($data != NULL) {
			$result = TRUE;
			$_SESSION['login_id'] = $data[0]['id'];
			$_SESSION['username'] = $username;
			$_SESSION['rank'] = $data[0]['rank'];
		}
		return $result;
	}

	/*
	 * Checking username or email
	 */

	public function checkUsername($username) {
		$result = 'true';

		$sql = 'SELECT id FROM ' . $this -> table_login . ' WHERE username=:username';

		$params = array(':username' => $username);
		$data = $this -> easydb -> stmt($sql, $params);

		if ($data != NULL) {
			$result = 'false';
		}
		return $result;
	}

	public function checkEmail($email) {
		$result = 'true';

		$sql = 'SELECT id FROM ' . $this -> table_profile . ' WHERE email=:email';

		$params = array(':email' => $email);
		$data = $this -> easydb -> stmt($sql, $params);

		if ($data != NULL) {
			$result = 'false';
		}
		return $result;
	}

	public function checkReferalCode($refer) {
		$result = 'false';

		$sql = 'SELECT id FROM ' . $this -> table_profile . ' WHERE referral_code=:refer';

		$params = array(':refer' => $refer);
		$data = $this -> easydb -> stmt($sql, $params);

		if ($data != NULL) {
			$result = 'true';
		}
		return $result;
	}

	public function receiptPayment($receipt_no) {
		$sql = 'SELECT * FROM ' . $this -> table_buy_order . ' WHERE receipt_number = :rn';
		$params = array(':rn' => $receipt_no);
		$data = $this -> easydb -> stmt($sql, $params);
		return $data[0];
	}

	public function uploadPayment($fromBank, $toBank, $dateInSlip, $timeInSlip, $payment, $image_name, $fullname, $phone, $email, $add1, $add2, $postcode, $city, $state, $package, $shipfee) {
		$receipt_number = 'BP' . time();

		$newtime = strtotime($dateInSlip);
		$newformat = date('Y-m-d', $newtime);

		// echo $fromBank, $toBank, $dateInSlip, $timeInSlip, $payment, $image_name, $fullname, $phone, $email, $add1, $add2, $postcode, $city, $state, $package, $shipfee;
		$sql = 'INSERT INTO ' . $this -> table_buy_order . ' (fromBank, toBank, payment, dateInSlip, timeInSlip, imageName, fullname, phone, email, address1, address2, postcode, city, state, package, shipFee, receipt_number) VALUES (:fromBank, :toBank, :payment, :dateInSlip, :timeInSlip, :imageName, :fullname, :phone, :email, :address1, :address2, :postcode, :city, :state, :package, :shipFee, :receipt_number)';
		$params = array(':fromBank' => $fromBank, ':toBank' => $toBank, ':payment' => $payment, ':dateInSlip' => $newformat, ':timeInSlip' => $timeInSlip, ':imageName' => $image_name, ':fullname' => $fullname, ':phone' => $phone, ':email' => $email, ':address1' => $add1, ':address2' => $add2, ':postcode' => $postcode, ':city' => $city, ':state' => $state, ':package' => $package, ':shipFee' => $shipfee, ':receipt_number' => $receipt_number);
		$this -> easydb -> stmt($sql, $params);

		return $receipt_number;

	}

	public function getPricing() {
		$sql = 'SELECT * FROM ' . $this -> table_pricing . ' WHERE id = 1';
		$data = $this -> easydb -> stmt($sql);
		return $data[0];
	}

	public function referProfile($refer) {
		$sql = 'SELECT fname, lname FROM ' . $this -> table_profile . ' WHERE referral_code=:refer';
		$params = array(':refer' => $refer);
		$data = $this -> easydb -> stmt($sql, $params);
		if(isset($data[0])){
		return $data[0];
		}else{
			return 0;
		}
	}

}
?>