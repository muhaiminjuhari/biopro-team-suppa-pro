<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Biopro - Sign Up!</title>
    
    <?php include 'maincss.php' ?>
   

</head>

<body>

    <div id="wrapper">
        
        <?php include 'navbar.php'?>
        
         <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="login-panel panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Register</h3>
                        </div>
                        <div class="panel-body">
                            <form role="form">
                                <fieldset>
                                    <div class="form-group">
                                        Signing up only take a minute! 
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="First Name" name="" type="" autofocus>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Last Name" name="" type="" >
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="E-mail" name="" type="email">
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Contact Number" name="" type="">
                                    </div>
                                    <div class="form-group">
                                        Login Information
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Username" name="" type="" >
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Password" name="password" type="password" value="">
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input name="remember" type="checkbox" value="Remember Me">I agree with the <a href=""  data-toggle="modal" data-target="#tnc-modal">Terms & Conditions</a>
                                        </label>
                                    </div>
                                    <a href="javascript:;" class="btn btn-lg btn-success btn-block">Sign Up</a>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- Modal -->
        <div class="modal fade" id="tnc-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Terms & Conditions</h4>
                    </div>
                    <div class="modal-body">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        
    </div>
    <!-- /#wrapper -->

   <?php include 'mainjs.php'?>
    
</body>

</html>
