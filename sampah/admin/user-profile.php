<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Biopro User</title>
    
    <?php include 'maincss.php' ?>
   

</head>

<body>

    <div id="wrapper">
        
        <?php include 'navbar.php'?>
        <?php include 'navbar-side-user.php'?>
        

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Profile</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="row">
                <div class="col-lg-3">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Profile Picture</h3>
                        </div>
                        <div class="panel-body">
                           <img src="assets/images/profile-default.jpg" class="img-responsive profile-img" />
                           <input type="file" />
                            <br>
                           <a href="" class="btn btn-primary btn-outline btn-block">Change Image</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Personal Information</h3>
                        </div>
                        <div class="panel-body">
                            <form role="form">
                                <fieldset>   
                                    <div class="form-group">
                                        <input class="form-control" placeholder="First Name" name="" type="" autofocus>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Last Name" name="" type="" >
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="E-mail" name="" type="email">
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Contact Number" name="" type="">
                                    </div>
                                    <a href="javascript:;" class="btn btn-success btn-outline">Update</a>
                                </fieldset>
                            </form>
                        </div>
                    </div>    
                </div>
                <!-- /.row -->          
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Recruit/Referral</h3>
                        </div>
                        <div class="panel-body">
                          <div class="jumbotron">
                            <h1>Expand your network!</h1>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt est vitae ultrices accumsan. Aliquam ornare lacus adipiscing, posuere lectus et, fringilla augue. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt est vitae ultrices accumsan. Aliquam ornare lacus adipiscing.</p>
                            <a class="btn btn-social btn-facebook"><i class="fa fa-facebook"></i> Facebook</a>
                            <a class="btn btn-social btn-twitter"><i class="fa fa-twitter"></i> Twitter</a>
                            <a class="btn btn-social btn-google-plus"><i class="fa fa-google-plus"></i> Google+</a>
                            <br><br>
                            <p>If you want to share it manually, you can use the link below:</p>  
                              
                             <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Show referral link</a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <a href="">www.google.com</a>
                                    </div>
                                </div>
                            </div>
                            
                    </div>
                        </div>
                    </div>
                </div>
            </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

   <?php include 'mainjs.php'?>
    
</body>

</html>
