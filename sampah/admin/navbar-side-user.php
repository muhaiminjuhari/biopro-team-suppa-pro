<!-- Navbar Side (user) code -->
<nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="side-menu">
                   
                   <!--search (could be handy later) -->
                   <!-- <li class="sidebar-search">
                        <div class="input-group custom-search-form">
                            <input type="text" class="form-control" placeholder="Search...">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                         /input-group 
                    </li>-->
                    <li>
                        <a href="index.html"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href=""><i class="fa fa-check"></i> Manage/Buy Stock</a>
                    </li>
                    <li>
                        <a href=""><i class="fa fa-check"></i> Recruit/Referral</a>
                    </li>
                    <li>
                        <a href=""><i class="fa fa-check"></i> Commision</a>
                    </li>
                    <li>
                        <a href=""><i class="fa fa-check"></i> Profile</a>
                    </li>
<!--nav link second level -->                    
<!--
                    <li>
                        <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Charts<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="flot.html">Flot Charts</a>
                            </li>
                            <li>
                                <a href="morris.html">Morris.js Charts</a>
                            </li>
                        </ul>
                         /.nav-second-level 
                    </li>
-->
                </ul>
                <!-- /#side-menu -->
            </div>
            <!-- /.sidebar-collapse -->
        </nav>
        <!-- /.navbar-static-side -->