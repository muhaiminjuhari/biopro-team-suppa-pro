<?php

class home_model extends Model {

	public $table_contact = 'contact';

	function __construct() {
		parent::__construct();
	}

	public function contactRegister($name, $email, $contactNo) {
		$sql = 'INSERT INTO ' . $this -> table_contact . ' (name, email, contact_number) VALUES (:name, :email, :contact)';
		$params = array(':name' => $name, ':email' => $email, ':contact' => $contactNo);
		$this -> easydb -> stmt($sql, $params);
	}

}
?>