<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" lang="en-US" prefix="og: http://ogp.me/ns#">
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" lang="en-US" prefix="og: http://ogp.me/ns#">
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html lang="en-US">
	<!--<![endif]-->
	<head>
		<?=$fixheader; ?>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width">
		<title>BIOPRO - Jimat lebih minyak!.</title>
		<link rel="shortcut icon" href="http://www.thecarbontree.com/wp-content/themes/carbontree/favicon.ico" />

		<link rel='stylesheet' id='contact-form-7-css'  href='assets/css/styles.css' type='text/css' media='all' />
		<link rel='stylesheet' id='theme_fonts-css'  href='assets/fonts/fonts.css' type='text/css' media='all' />
		<link rel='stylesheet' id='theme_stylesheet-css'  href='assets/css/style.css' type='text/css' media='all' />
		<script type='text/javascript' src='assets/js/jquery/jquery.js'></script>
		<script type='text/javascript' src='assets/js/jquery/jquery-migrate.min.js'></script>

		<body class="home page page-id-62 page-template page-template-index-php">

			<div class="site-container">

				<header class="m-site-head" role="banner">

					<div class="container">

						<span class="logo"> <a href="../home/"><img class="top-logo" src="assets/images/bioprologo-mini.png" alt="Biopro - Save More Fuel" data-svg-replacement="assets/images/header-logo.png"/></a> </span>

						<nav class="m-site-nav l-right">
							<ul class="m-inline-list">
								<li id="menu-item-33" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-33 list-item">
									<a href="../home#biopro">Apakah itu Biopro?</a>
								</li>
								<li id="menu-item-32" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-32 list-item">
									<a href="../home#inventor" >Inventor</a>
								</li>
								<li id="menu-item-27" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-27 list-item">
									<a href="../home#sertai">Sertai Kami</a>
								</li>
								<li id="menu-item-26" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-26 list-item">
									<a href="../home#statistik">Statistik Penjimatan</a>
								</li>
								<li id="menu-item-30" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-26 list-item">
									<a href="../home#mengenai">Mengenai Kami</a>
								</li>
								<li id="menu-item-29" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-26 list-item">
									<a href="../home/contact">Hubungi Kami</a>
								</li>
							</ul>
						</nav>

						<!-- Mobile responsive navigation menu -->
						<label class="mobile-nav-label" for="mobile-nav-checkbox">&#9776;</label>
						<input class="mobile-nav-checkbox" id="mobile-nav-checkbox" type="checkbox"/>

						<div class="mobile-nav">
							<ul class="t-grid-4 no-padding no-style p-grid-12">
								<li id="menu-item-249" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-249 list-item">
									<a href="" >Link 1</a>
								</li>
								<li id="menu-item-248" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-248 list-item">
									<a href="" >Link 2</a>
								</li>
							</ul>
							<ul class="t-grid-5 no-padding no-style p-grid-12">
								<!--<p class="t-grid-4">What is Carbon Capture?</p>-->
								<div class="t-grid-8">
									<li id="menu-item-59" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-59 list-item">
										<a href="" >About Us</a>
									</li>
									<li id="menu-item-55" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-55 list-item">
										<a href="" >Link 2</a>
									</li>
									<li id="menu-item-53" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-53 list-item">
										<a href="" >Link 3</a>
									</li>
								</div>
							</ul>
							<ul class="t-grid-3 no-padding no-style p-grid-12">
								<li id="menu-item-457" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-457 list-item">
									<a href="">Case Study</a>
								</li>
								<li id="menu-item-57" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-57 list-item">
									<a href="" >About Us</a>
								</li>
								<li id="menu-item-56" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-56 list-item">
									<a href="" >Contact Us</a>
								</li>
							</ul>
						</div>

					</div>

				</header>
				<!---------- BANNER ---------->
				<section class="m-hero m-banner" style="background-image:url('http://www.thecarbontree.com/wp-content/uploads/2013/12/TrunkNEW.jpg');">

					<div class="container">
						<div class="caption">
							<h3 class="h1 heading h1-1">Hubungi Kami</h3>
						</div>
					</div>

				</section>
				<!---------- CONTACT US PAGE ---------->
				<div class="container">

					<div class="form-container padding-4">

						<!---------- HEADING ---------->
						<h3 class="l-centered">Berminat untuk menjadi pengedar rasmi Biopro Petrol?</h3>
						<p>
							Atau hanya ingin mengetahui plan perniagaan, sila tinggalkan butiran anda dibawah. Kami akan hubungi anda untuk membantu!
						</p>
						<!---------- CONTACT FORM ---------->
						<div class="l-centered">
							<div class="wpcf7" id="wpcf7-f43-p18-o1">
								<form action="../home/contactRegister" method="post" class="wpcf7-form">

									<p>
										<span class="wpcf7-form-control-wrap name">
											<input type="text" name="name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-field" required="required" placeholder="Your Name" />
										</span>
									</p>
									<p>
										<span class="wpcf7-form-control-wrap email">
											<input type="email" name="email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email form-field" placeholder="Your Email" required="required" />
										</span>
									</p>
									<p>
										<span class="wpcf7-form-control-wrap email">
											<input type="text" name="contactNo" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email form-field"  placeholder="Contact Number" required="required" />
										</span>
									</p>

									<p>
										<input type="submit" value="Hantar" class="wpcf7-form-control wpcf7-submit m-btn form-button" />
									</p>
									<!-- <div class="wpcf7-response-output wpcf7-display-none"></div> -->
								</form>
							</div>
						</div>

						<!---------- EMAIL ---------->
						<p>
							<span class="small-text">Email&#8212;</span>
							<br>
							hello@biopropetrol.com
						</p>

						<!---------- TELEPHONE ---------->
						<p>
							<span class="small-text">Telephone&#8212;</span>
							<br>
							019 290 4438 (9 AM - 6 PM, Monday - Friday)
						</p>
					</div>
				</div>

				<footer class="m-site-footer">

					<div class="container footer-nav">

						<div class="l-grid-2 l-gutter-expand t-grid-4 p-grid-12">
							<ul class="m-stacked-list">
								<li id="menu-item-50" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-50 list-item">
									<a href="../home/">Home</a>
								</li>
								<li id="menu-item-52" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-52 list-item">
									<a href="../home#biopro">Biopro</a>
								</li>
								<li id="menu-item-51" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-51 list-item">
									<a href="../home#statistik">Statistik</a>
								</li>
							</ul>
						</div>

						<div class="l-grid-3 l-gutter-expand t-grid-5 p-grid-12">
							<ul class="l-grid-7 l-gutter-expand-left t-grid-8 t-gutter-expand-left p-grid-12 m-stacked-list">
								<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-59 list-item">
									<a href="../home#inventor">Inventor</a>
								</li>
								<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-55 list-item">
									<a href="../home#sertai">Sertai Kami</a>
								</li>
							</ul>
						</div>

						<div class="l-grid-2 l-gutter-expand t-grid-3 p-grid-12">
							<ul class="m-stacked-list">
								<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-57 list-item">
									<a href="../home#mengenai">Mengenai Kami</a>
								</li>
								<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-56 list-item">
									<a href="../home/contact">Hubungi Kami</a>
								</li>
							</ul>
						</div>

					</div>

					<div class="container copyright-credits">

						<p class="logo l-right">
							<a href="#"><img class="svg-replace" src="assets/images/footer-logo.png" alt="" data-svg-replacement=""/></a>
						</p>
						<p class="l-left">
							<span class="copyright">&copy; 2013</span>
							<!--			<span class="credits">Site by <a href="" target="_blank">Wired In</a></span>-->
						</p>
					</div>
				</footer>
			</div>

			<script type='text/javascript' src='js/jquery.form.min.js'></script>

			<!-- <script type='text/javascript'>
			/* <![CDATA[ */
			var _wpcf7 = {"loaderUrl":"http:\/\/www.thecarbontree.com\/wp-content\/plugins\/contact-form-7\/images\/ajax-loader.gif","sending":"Sending ..."};
			/* ]]> */
			</script>

			<script type='text/javascript' src='js/scripts2.js'></script>
			<script type='text/javascript' src='js/script.js'></script>
			<script type='text/javascript' src='js/modernizr.min.js'></script>
			<script type='text/javascript' src='js/jquery.flexslider-2.2.0.min.js'></script>
			<script type='text/javascript' src='js/waypoints.min.js'></script>
			<script type='text/javascript' src='js/jquery.event.move.js'></script>
			<script type='text/javascript' src='js/jquery.twentytwenty.js'></script>
			<script type='text/javascript' src='js/jquery.magnific-popup.min.js'></script> -->
		</body>
</html>