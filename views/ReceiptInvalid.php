<!DOCTYPE html>
<html>

	<head>
		<?=$fixheader; ?>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<title>Biopro Fuel Saver</title>
        <link rel="shortcut icon" href="assets/images/biopro-box.png" />

		<!--main css load code -->

		<!-- Core CSS - Include with every page -->
		<link href="assets2/css/bootstrap.css" rel="stylesheet">
		<link href="assets2/font-awesome/css/font-awesome.css" rel="stylesheet">

		<!-- Page-Level Plugin CSS - Dashboard -->
		<link href="assets2/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">
		<link href="assets2/css/plugins/timeline/timeline.css" rel="stylesheet">
		<link href="assets2/css/plugins/social-buttons/social-buttons.css" rel="stylesheet">

		<!-- SB Admin CSS - Include with every page -->
		<link href="assets2/css/sb-admin.css" rel="stylesheet">
		<link href="assets2/css/main.css" rel="stylesheet">
		<link href="assets2/css/nazar.css" rel="stylesheet">

	</head>

	<body style="background-color:white;">
		<div id="wrapper">
			<!-- Navbar Top code -->
			<nav class="navbar navbar-default navbar-static-top navbar-green" role="navigation" style="margin-bottom: 0">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="../">Biopro - Penjimatan Terbaik!</a>
				</div>
				<!-- /.navbar-header -->
			</nav>
			<!-- /.navbar-static-top -->
            <div class="container">
                <br/><br/>
                <div class="well col-xs-10 col-sm-10 col-md-6 col-xs-offset-1 col-sm-offset-1 col-md-offset-3">
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <address>
                                <strong>Biopro Petrol Global Marketing Sdn. Bhd</strong>
                                <br>
                                No 1 Jalan 2,
                                <br>
                                Kawasan Perindustrian Lekas 18.
                                <br>
                                Semenyih 43500, Selangor
                                <br>
                                <abbr title="Phone">P:</abbr> 019 290 4438 <br/>(9AM - 6PM Monday - Friday)
                            </address>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 text-right">
                            <p>
                                <em>Date: -, -</em>
                            </p>
                            <p>
                                <em>Receipt #: -</em>
                            </p>
                        </div>
                    </div>
                    <hr/>
                    <div class="row">
                        <div class="text-center">
                            <h1>Receipt Invalid!</h1>
                            <p>Please contact us for more clarification.</p>
                        </div>
                        </span>
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Product</th>
                                    <th>#</th>
                                    <th class="text-center">Price</th>
                                    <th class="text-center">Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="col-md-9"><em></em></h4></td>
                                    <td class="col-md-1" style="text-align: center">  </td>
                                    <td class="col-md-1 text-center"></td>
                                    <td class="col-md-1 text-center"></td>
                                </tr>
                                <tr>
                                    <td>   </td>
                                    <td>   </td>
                                    <td class="text-right">
                                    <p>
                                        <strong>Subtotal: </strong>
                                    </p>
                                    </td>
                                    <td class="text-center">
                                    <p>
                                        <strong></strong>
                                    </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>   </td>
                                    <td>   </td>
                                    <td class="text-right"><h4><strong>Total: </strong></h4></td>
                                    <td class="text-center text-danger"><h4><strong></strong></h4></td>
                                </tr>
                            </tbody>
                        </table>
                        <button type="button" class="btn btn-success btn-lg btn-block">
                            Back  <span class="glyphicon glyphicon-chevron-right"></span>
                        </button></td>
                    </div>
                </div>
		</div>
		<!-- /#wrapper -->

		<!-- Core Scripts - Include with every page -->
		<script src="assets2/js/jquery-1.10.2.js"></script>
		<script src="assets2/js/bootstrap.min.js"></script>
		<script src="assets2/js/plugins/metisMenu/jquery.metisMenu.js"></script>

		<!-- Page-Level Plugin Scripts - Dashboard -->
		<script src="assets2/js/plugins/morris/raphael-2.1.0.min.js"></script>
		<script src="assets2/js/plugins/morris/morris.js"></script>

		<!-- SB Admin Scripts - Include with every page -->
		<script src="assets2/js/sb-admin.js"></script>

		<!-- Page-Level Demo Scripts - Dashboard - Use for reference -->
		<script src="assets2/js/demo/dashboard-demo.js"></script>
		       
		<!--Validation-->
		<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.js"></script>
		
        <!--  Holder.js    -->
		<script src="assets2/js/holder.js"></script>
		
		
        
	</body>

</html>
