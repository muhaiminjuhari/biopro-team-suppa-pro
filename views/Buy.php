<!DOCTYPE html>
<html>

	<head>
		<?=$fixheader; ?>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<title>Biopro Fuel Saver</title>
        <link rel="shortcut icon" href="assets/images/biopro-box.png" />

		<!--main css load code -->

		<!-- Core CSS - Include with every page -->
		<link href="assets2/css/bootstrap.css" rel="stylesheet">
		<link href="assets2/font-awesome/css/font-awesome.css" rel="stylesheet">

		<!-- Page-Level Plugin CSS - Dashboard -->
		<link href="assets2/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">
		<link href="assets2/css/plugins/timeline/timeline.css" rel="stylesheet">
		<link href="assets2/css/plugins/social-buttons/social-buttons.css" rel="stylesheet">

		<!-- SB Admin CSS - Include with every page -->
		<link href="assets2/css/sb-admin.css" rel="stylesheet">
		<link href="assets2/css/main.css" rel="stylesheet">
		<link href="assets2/css/nazar.css" rel="stylesheet">

	</head>

	<body style="background-color:white;">
		<div id="wrapper">
			<!-- Navbar Top code -->
			<nav class="navbar navbar-default navbar-static-top navbar-green" role="navigation" style="margin-bottom: 0">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="../">Biopro - Penjimatan Terbaik!</a>
				</div>
				<!-- /.navbar-header -->
			</nav>
			<!-- /.navbar-static-top -->
            <div class="container">
                <div class="row">
                    <br/>
                    <div class="col-lg-12">
                        <div class="jumbotron background-box">
                            <h1 style="font-size:4em;"><b>Select the best product for you!</b></h1>
                            <h2><small>To get the most performance, tell us what kind of car that you drive?</small></h2>
                        </div>
                    </div>
                </div>
                
                <!-- <div class="row">
                    <div class="col-lg-12">
                        <div class="alert alert-info" align="center">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <span class="text-center">Thank You for Buying!</span>
                        </div>
                    </div>
                </div> -->
                
                
                <div class="row">
                    <div class="col-lg-12">
                        <div class="alert alert-success" align="center">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <span class="text-center">Credit/Debit card payment coming soon!</span>
                        </div>
                    </div>
                </div>
                
                <div class="row form-group product-chooser">
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="product-chooser-item">
                            <img src="assets/images/small-car.jpg" class="img-rounded col-xs-4 col-sm-4 col-md-12 col-lg-12" alt="Mobile and Desktop">
                            <div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">
                                <span class="title"><h2>Small Car</h2></span>
                                <span class="description">Kancil, Viva, Ford Fiesta, Kia Picanto, Mirage etc.</span>  
                                    <p><b>A small car with 20 Litre full tank needs 4 bottles of Biopro every month</b></p>                   
                                <br/><br/>
                                <a href="../home/buyOrder?order=4" class="btn btn-primary btn-block">Order 4 Bottles (RM28.00)<i class="fa fa-arrow-right"></i></a>
                                <a href="javascript:;" class="btn btn-default btn-block" disabled>Credit/Debit Card</a>
                             </div>
                            <div class="clear"></div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="product-chooser-item">
                            <img src="assets/images/sedan-car.jpg" class="img-rounded col-xs-4 col-sm-4 col-md-12 col-lg-12" alt="Desktop">
                            <div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">
                                <span class="title"><h2>Sedan</h2></span>
                                <span class="description">Proton Saga, Persona, Vios, Honda City, Almeera. Nissan Vannete.</span>
                                <p><b>A sedan with 40 Litre full tank needs 8 bottles of Biopro every month</b></p>
                                <br/>
                                <a href="../home/buyOrder?order=8" class="btn btn-primary btn-block">Order 8 Bottles (RM56.00) <i class="fa fa-arrow-right"></i></a> 
                                <a href="javascript:;" class="btn btn-default btn-block" disabled>Credit/Debit Card</a>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="product-chooser-item">
                            <img src="assets/images/motor.jpg" class="img-rounded col-xs-4 col-sm-4 col-md-12 col-lg-12" alt="Mobile">
                            <div class="col-xs-8 col-sm-8 col-md-12 col-lg-12">
                                <span class="title"><h2>Motorcycle</h2></span>
                                <span class="description">Motorcycle example here <br/></span>
                                <p><b>1 bottle of Biopro Fuel Saver can fill up to 6 full tank of 3 litre motorcycle per month</b></p>
                                <br/><br/>
                                <a href="../home/buyOrder?order=1" class="btn btn-primary btn-block">Order 1 Bottle (RM7.00) <i class="fa fa-arrow-right"></i></a> 
                                <a href="javascript:;" class="btn btn-default btn-block" disabled>Credit/Debit Card</a>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
                <hr/>
                <div class="container">
                    <div class="row">
                        <br/>
                        <h1 class="text-center">Check out our package below!</h1>
                        <p class="text-center">Find the best package for you.</p>
                        <br/>
                        <div class="list-group">
                          <div class="list-group-item active">
                                <div class="media col-md-3">
                                    <figure class="pull-left">
                                        <img class="media-object img-rounded img-responsive"  src="assets/images/biopro-bottle.png" alt="placehold.it/350x250" style="height:200px; width:200px;">
                                    </figure>
                                </div>
                                <div class="col-md-6">
                                    <h2 class="list-group-item-heading"> Trial Package </h2>
                                    <p class="list-group-item-text">
                                        2 x 200ml Biopro Fuel Saver for Petrol <br/>
                                        Price RM7 x 2 = RM14<br/>
                                        Mailing cost RM8 Peninsular Msia, RM12 Sabah/Sarawak<br/>
                                    </p>
                                </div>
                                <div class="col-md-3 text-center">
                                    <h4>Payment Option</h4>
                                    <a href="../home/buyOrder?order=2" class="btn btn-default btn-primary" style="width:220px; margin-bottom:10px;"> Bank-in </a><br/>
                                    <a href="javascript:;" class="btn btn-default" style="width:220px;" disabled> Credit/Debit Card </a> 
                                </div>
                          </div>
                          <div class="list-group-item active">
                                <div class="media col-md-3">
                                    <figure class="pull-left">
                                        <img class="media-object img-rounded img-responsive"  src="assets/images/biopro-box.png" alt="placehold.it/350x250" style="height:200px; width:200px;">
                                    </figure>
                                </div>
                                <div class="col-md-6">
                                    <h2 class="list-group-item-heading"> Silver Package </h2>
                                    <p class="list-group-item-text">
                                        15 x 200ml Biopro Fuel Saver for Petrol <br/>
                                        Price RM90.00<br/>
                                        Mailing cost RM8 Peninsular Msia, RM12 Sabah/Sarawak<br/>
                                    </p>
                                </div>
                                <div class="col-md-3 text-center">
                                    <h4>Payment Option</h4>
                                    <a href="../home/buyOrder?order=15" class="btn btn-default btn-primary" style="width:220px; margin-bottom:10px;"> Bank-in </a><br/>
                                    <a href="javascript:;" class="btn btn-default" style="width:220px;" disabled> Credit/Debit Card </a> 
                                </div>
                          </div>
                          <div class="list-group-item active" style="height:350px;">
                                <div class="media col-md-3">
                                    <figure class="pull-left">
                                        <img class="media-object img-rounded img-responsive"  src="assets/images/biopro-box-premium.png" alt="placehold.it/350x250" style="height:200px; width:200px;">
                                    </figure>
                                </div>
                                <div class="col-md-6">
                                    <h2 class="list-group-item-heading"> Agent Package + Member </h2>
                                    <p class="list-group-item-text">
                                        50 x 200ml Biopro Fuel Saver for Petrol<br/>
                                        Price RM300<br/>
                                        1 Promotion Kit RM200 (one time only)<br/>
                                        Mailing cost RM20<br/>
                                        Total <b>RM520</b><br/>
                                        <br/>
                                        <b><span class="bg-primary" style="padding:5px;">Agent Affiliate Program</b></span>
                                        <br/>
                                        <br/>Promote your referral code to your friends and every time you recruit new member at Level 1, RM150 bonus will be paid to you.
                                        <br/>Every time your friends at Level 1 buy a box of Biopro Fuel Saver, RM40 repeat sales bonus will be paid to you.
                                        <br/>Every time your friends at Level 2 buy a box of Biopro Fuel Saver, RM20 repeat sales bonus will be paid to you.
                                    </p>
                                    
                                    
                                    
                                </div>
                                <div class="col-md-3 text-center">
                                    <h4>Payment Option</h4>
                                    <a href="../home/registerPre" class="btn btn-default btn-success" style="width:220px; margin-bottom:10px;"> Register </a>
                                    <!-- <a href="javascript:;" class="btn btn-default btn-primary" style="width:220px; margin-bottom:10px;"> Bank-in </a><br/>
                                    <a href="javascript:;" class="btn btn-default" style="width:220px;" disabled> Credit/Debit Card </a>  -->
                                </div>
                          </div>
                          
                        </div>
                    </div>
                </div>
            </div>
            
		</div>
		<!-- /#wrapper -->

		<!-- Core Scripts - Include with every page -->
		<script src="assets2/js/jquery-1.10.2.js"></script>
		<script src="assets2/js/bootstrap.min.js"></script>
		<script src="assets2/js/plugins/metisMenu/jquery.metisMenu.js"></script>

		<!-- Page-Level Plugin Scripts - Dashboard -->
		<script src="assets2/js/plugins/morris/raphael-2.1.0.min.js"></script>
		<script src="assets2/js/plugins/morris/morris.js"></script>

		<!-- SB Admin Scripts - Include with every page -->
		<script src="assets2/js/sb-admin.js"></script>

		<!-- Page-Level Demo Scripts - Dashboard - Use for reference -->
		<script src="assets2/js/demo/dashboard-demo.js"></script>s
		       
		<!--Validation-->
		<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.js"></script>
		
        <!--  Holder.js    -->
		<script src="assets2/js/holder.js"></script>
		
		
		
        
	</body>

</html>
