<!DOCTYPE html>
<html>

	<head>
		<?=$fixheader; ?>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<title>Biopro Fuel Saver - Sign Up!</title>
		<link rel="shortcut icon" href="assets/images/biopro-box.png" />

		<!--main css load code -->

		<!-- Core CSS - Include with every page -->
		<link href="assets2/css/bootstrap.css" rel="stylesheet">
		<link href="assets2/font-awesome/css/font-awesome.css" rel="stylesheet">

		<!-- Page-Level Plugin CSS - Dashboard -->
		<link href="assets2/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">
		<link href="assets2/css/plugins/timeline/timeline.css" rel="stylesheet">
		<link href="assets2/css/plugins/social-buttons/social-buttons.css" rel="stylesheet">

		<!-- SB Admin CSS - Include with every page -->
		<link href="assets2/css/sb-admin.css" rel="stylesheet">
		<link href="assets2/css/main.css" rel="stylesheet">

		<link href="assets2/css/nazar.css" rel="stylesheet">

	</head>

	<body class="background1">

		<div id="wrapper">

			<!-- Navbar Top code -->
			<nav class="navbar navbar-default navbar-static-top navbar-green" role="navigation" style="margin-bottom: 0">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="../">Biopro - Penjimatan Terbaik!</a>
				</div>
				<!-- /.navbar-header -->

			</nav>
			<!-- /.navbar-static-top -->

			<div class="container">
				<div class="row">
					<div class="col-lg-6 col-lg-offset-3" align="center">
						<div class="register-img"></div>
					</div>
				</div>
				<div class="row login-panel">
					<form id="submitForm" role="form" action="../home/registerUser" method="post">
						<div class="row">
							<div class="col-lg-6 col-lg-offset-3">
								<div class="panel panel-success">
									<div class="panel-heading">
										<h3 class="panel-title">Account Information</h3>
									</div>
									<div class="panel-body">
										<fieldset>
											<div class="form-group">

												<h3 style="padding:10px; color:black; border-bottom:5px solid #E7E7E7; background-color:#FFE200"> Please complete all fields, this will helps us to communicate with you and doing commission transactions. </h3>
												<div class="alert alert-success" style="padding-bottom:5px;">
													<h4>Pro tip: Make sure you have referral code with you!</h4>
												</div>

											</div>

											<div class="form-group">
												Login Information
											</div>
											<div class="form-group">
												<input id="username" class="form-control" placeholder="Username" minlength="7" name="username" type="text" required="required">
											</div>
											<div class="form-group">
												<input id="password" class="form-control" placeholder="Password" minlength="7" name="password" type="password" value="" required="required">
											</div>
											<div class="form-group">
												<input id="repassword" class="form-control" placeholder="Retype password" name="repassword" type="password" value="" required="required">
											</div>
										</fieldset>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-6 col-lg-offset-3">
								<div class="panel panel-success">
									<div class="panel-heading">
										<h3 class="panel-title">Personal Information</h3>
									</div>
									<div class="panel-body">

										<fieldset>
											<div class="form-group">

											</div>
											<div class="form-group">
												<input class="form-control" placeholder="First Name" name="fname" type="text" required="required" autofocus>
											</div>
											<div class="form-group">
												<input class="form-control" placeholder="Last Name" name="lname" type="text" required="required">
											</div>
											<div class="form-group">
												<input class="form-control" placeholder="E-mail" name="email" type="email" required="required">
											</div>
											<div class="form-group">
												<input class="form-control" placeholder="Contact Number" name="contactNo" type="text" required="required">
											</div>
											<div class="form-group">
												<label>Gender</label>
												<label class="radio-inline">
													<input type="radio" name="gender" id="optionsRadiosInline1" value="male" checked>
													Male </label>
												<label class="radio-inline">
													<input type="radio" name="gender" id="optionsRadiosInline2" value="female">
													Female </label>
											</div>
											<div class="form-group">
												<label>Are you Malaysian?</label>
												<label class="radio-inline">
													<input type="radio" name="local" id="optionsRadiosInline1" value="1" checked>
													Yes </label>
												<label class="radio-inline">
													<input type="radio" name="local" id="optionsRadiosInline2" value="0">
													No </label>
											</div>
											<div class="form-group">
												<label>IC Number/Passport Number</label>
												<input class="form-control" placeholder="" name="icno" type="text" required="required">
											</div>
											<div class="form-group">
												<label>Address</label>
												<input class="form-control" placeholder="Address 1" name="add1" type="text" required="required">
											</div>
											<div class="form-group">
												<input class="form-control" placeholder="Address 2" name="add2" type="text">
											</div>
											<div class="form-group">
												<input class="form-control" placeholder="City/Town" name="city" type="text" required="required">
											</div>
											<div class="form-group">
												<input class="form-control" placeholder="Postcode" name="postcode" type="text" required="required">
											</div>
											<div class="form-group">
												<input class="form-control" placeholder="State" name="state" type="text" required="required">
											</div>
											<div class="form-group">
												<select class="form-control" name="country" required="required">
													<option value="">Country</option>
													<option value="MALAYSIA">Malaysia</option>
													<option value="INDONESIA">Indonesia</option>
													<option value="THAILAND">Thailand</option>
													<option value="SINGAPORE">Singapore</option>
													<option value="BRUNEI">Brunei</option>
												</select>
											</div>
										</fieldset>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-6 col-lg-offset-3">
								<div class="panel panel-success">
									<div class="panel-heading">
										<h3 class="panel-title">Banking Information</h3>
									</div>
									<div class="panel-body">
										<div class="form-group">
											<label>Bank</label>
											<select class="form-control" name="bankaccount">
												<option value="Maybank">Maybank</option>
												<option value="CIMB">CIMB</option>
												<option value="Hong Leong">Hong Leong</option>
												<option value="Bank Islam">Bank Islam</option>
												<option value="Other">Other Bank</option>
											</select>
										</div>
										<div class="form-group">
											<label>Bank Account Number</label>
											<input class="form-control" type="text" placeholder="" name="accountno" required="required" />
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-6 col-lg-offset-3">
								<div class="panel panel-default">
									<div class="panel-body">
										<div class="form-group">
											<input id="refer" class="form-control" placeholder="Referral Code" name="refer" type="text" required="required" value="<?php
											if (isset($_GET['refer'])) {echo $_GET['refer'];
											}
 ?>">

											<!-- <small>Just leave it blank if you do not have any referral code</small> -->
										</div>
										<div class="checkbox">
											<label>
												<input id="remember" name="remember" type="checkbox" required="required">
												I agree with the <!-- <a href=""  data-toggle="modal" data-target="#tnc-modal"> -->Terms & Conditions<!-- </a> --> </label>
											<br />
											<label for="remember" class="error" style="font-weight: bold;"></label>
										</div>
										<button class="btn btn-success btn-block">
											Sign Up
										</button>
									</div>
								</div>
							</div>
						</div>

					</form>
				</div>
			</div>

			<!-- Modal -->
			<div class="modal fade" id="tnc-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
								&times;
							</button>
							<h4 class="modal-title" id="myModalLabel">Terms & Conditions</h4>
						</div>
						<div class="modal-body">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">
								Close
							</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->

		</div>
		<!-- /#wrapper -->

		<!-- Core Scripts - Include with every page -->
		<script src="assets2/js/jquery-1.10.2.js"></script>
		<script src="assets2/js/bootstrap.min.js"></script>
		<script src="assets2/js/plugins/metisMenu/jquery.metisMenu.js"></script>

		<!-- Page-Level Plugin Scripts - Dashboard -->
		<script src="assets2/js/plugins/morris/raphael-2.1.0.min.js"></script>
		<script src="assets2/js/plugins/morris/morris.js"></script>

		<!-- SB Admin Scripts - Include with every page -->
		<script src="assets2/js/sb-admin.js"></script>

		<!-- Page-Level Demo Scripts - Dashboard - Use for reference -->
		<script src="assets2/js/demo/dashboard-demo.js"></script>

		<!--Validation-->
		<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.js"></script>
		<script>
			$("#submitForm").validate({
				rules : {
					password : "required",
					repassword : {
						equalTo : "#password"
					},
					username : {
						remote : {
							url : "../home/checkUsername",
							type : "post"
						}
					},
					email : {
						remote : {
							url : "../home/checkEmail",
							type : "post"
						}
					},
					refer : {
						remote : {
							url : "../home/checkReferalCode",
							type : "post"
						}
					}
				},
				messages : {
					remember : {
						required : "Please accept term & condition."
					},
					username : {
						remote : "Username already in use!"
					},
					email : {
						remote : "Email already in use!"
					},
					refer : {
						remote : "Referal Code is not exist in database!"
					}
				}
			});
		</script>

	</body>

</html>
