<!DOCTYPE html>
<html>

	<head>
		<?=$fixheader; ?>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<title>Biopro - Login!</title>
        <link rel="shortcut icon" href="assets/images/biopro-box.png" />

		<!--main css load code -->

		<!-- Core CSS - Include with every page -->
		<link href="assets2/css/bootstrap.css" rel="stylesheet">
		<link href="assets2/font-awesome/css/font-awesome.css" rel="stylesheet">

		<!-- Page-Level Plugin CSS - Dashboard -->
		<link href="assets2/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">
		<link href="assets2/css/plugins/timeline/timeline.css" rel="stylesheet">
		<link href="assets2/css/plugins/social-buttons/social-buttons.css" rel="stylesheet">

		<!-- SB Admin CSS - Include with every page -->
		<link href="assets2/css/sb-admin.css" rel="stylesheet">
		<link href="assets2/css/main.css" rel="stylesheet">
		
		<link href="assets2/css/nazar.css" rel="stylesheet">

	</head>

	<body class="background1">

		<div id="wrapper">
           

			<!-- Navbar Top code -->
			<nav class="navbar navbar-default navbar-static-top navbar-green" role="navigation" style="margin-bottom: 0">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="../">Biopro - Penjimatan Terbaik!</a>
				</div>
				<!-- /.navbar-header -->

			</nav>
			<!-- /.navbar-static-top -->

			<div class="container">
                <br/><br/><br/>
                <div class="row" >
                    <div class="col-md-4 col-md-offset-4" style="margin-bottom:-250px;">
                        <div class="login-img"></div>
                    </div>
                </div>
				<div class="row" style="margin-top:-14px;">
					<div class="col-md-4 col-md-offset-4">
						<div class="login-panel panel panel-success">
							<div class="panel-heading">
								<h3 class="panel-title">Login</h3>
							</div>
							<div class="panel-body">
								<?php
								if (isset($_GET['display'])) {
									if ($_GET['display'] == 'error') {
										echo '
											<div class="alert alert-danger alert-dismissable">
											<button type="button" class="close" data-dismiss="alert" aria-hidden="true">
											&times;
											</button>
											<strong>Alert! </strong> Username or Password mismatch.
											</div>
										';
									}
								}
								?>

								<form id="submitForm" role="form" action="../home/loginCheck" method="post">
									<fieldset>
										<div class="form-group">
											<input class="form-control" placeholder="Username" name="username" type="text" required="required">
										</div>
										<div class="form-group">
											<input class="form-control" placeholder="Password" name="password" type="password" value="" required="required">
										</div>

										<button class="btn btn-success btn-block">
											Login
										</button>
									</fieldset>
								</form>
                                <hr/>
                                <p>New user? Register <a href="../home/registerPre">here</a></p>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- Modal -->
			<div class="modal fade" id="tnc-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
								&times;
							</button>
							<h4 class="modal-title" id="myModalLabel">Terms & Conditions</h4>
						</div>
						<div class="modal-body">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">
								Close
							</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->

		</div>
		<!-- /#wrapper -->

		<!-- Core Scripts - Include with every page -->
		<script src="assets2/js/jquery-1.10.2.js"></script>
		<script src="assets2/js/bootstrap.min.js"></script>
		<script src="assets2/js/plugins/metisMenu/jquery.metisMenu.js"></script>

		<!-- Page-Level Plugin Scripts - Dashboard -->
		<script src="assets2/js/plugins/morris/raphael-2.1.0.min.js"></script>
		<script src="assets2/js/plugins/morris/morris.js"></script>

		<!-- SB Admin Scripts - Include with every page -->
		<script src="assets2/js/sb-admin.js"></script>

		<!-- Page-Level Demo Scripts - Dashboard - Use for reference -->
		<script src="assets2/js/demo/dashboard-demo.js"></script>
		
		<!--Validation-->
		<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.js"></script>
		<script>
			$("#submitForm").validate({
				rules: {
					password: "required",
					repassword: {
						equalTo: "#password"
					}
				}
			});
		</script>
		
		<?php
		if(isset($_GET['register'])){
		?>
		<script type="text/javascript" src="assets2/js/bootbox.js"></script>
		<script>
			bootbox.alert("We are still processing your registration. We will notify you through email once we are done.");
		</script>
		<?php
		}
		?>

	</body>

</html>
