<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Profile <small><i class="fa fa-user"></i></small></h1>
	</div>
	<!-- /.col-lg-12 -->
</div>
<div class="row">
	<div class="col-lg-12">
		<a href="../user/commision/" class="btn btn-primary btn-outline"><i class="fa fa-user"></i> Profile</a> 
		<a href="../user/commision/" class="btn btn-primary btn-outline"><i class="fa fa-lock"></i> Password</a> 
		<a href="../user/commision/" class="btn btn-primary btn-outline"><i class="fa fa-university"></i> Banking Account</a> 
	</div>
</div>
<hr />
<div class="row">
	<div class="col-lg-3">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Profile Picture</h3>
			</div>
			<div class="panel-body">
				<?php
				if (isset($_GET['success'])) {
					if ($_GET['success'] == 'picture') {
						echo '
							<div class="alert alert-success alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">
							&times;
							</button>
							<strong>Success! </strong> Your picture updated.
							</div>
						';
					}
				}
				?>
				<form id="submitForm3" method="post" action="../user/updatePicture" enctype="multipart/form-data">
					<img src="upload/<?=$data['profile']['picture_name'] ?>" class="img-responsive" />
					<input type="file" name="picture" required="required"/>
					<br>
					<button class="btn btn-success btn-outline btn-block">Change Image <i class="fa fa-save"></i></button>
				</form>
			</div>
		</div>
		
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Banking Information</h3>
			</div>
			<div class="panel-body">
				<?php
				if (isset($_GET['success'])) {
					if ($_GET['success'] == 'account') {
						echo '
							<div class="alert alert-success alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">
							&times;
							</button>
							<strong>Success! </strong> Your picture updated.
							</div>
						';
					}
				}
				?>
				<form id="submitForm2" method="post" action="../user/updateAccount">
					<div class="form-group">
	                    <label>Bank</label>
	                    <select class="form-control" name="bankaccount">
	                        <option <?=($data['profile']['account_bank']=='Maybank')?'Selected':'';?>>Maybank</option>
	                        <option <?=($data['profile']['account_bank']=='CIMB')?'Selected':'';?>>CIMB</option>
	                        <option <?=($data['profile']['account_bank']=='Hong Leong')?'Selected':'';?>>Hong Leong</option>
	                        <option <?=($data['profile']['account_bank']=='BSN')?'Selected':'';?>>BSN</option>
	                        <option <?=($data['profile']['account_bank']=='Bank Rakyat')?'Selected':'';?>>Bank Rakyat</option>
	                    </select>
	                </div>
	                <div class="form-group">
	                    <label>Bank Account Number</label>
	                    <input class="form-control" type="text" placeholder="" name="accountno" required="required" value="<?=$data['profile']['account_number'] ?>"/>
	                </div>
					<button class="btn btn-success btn-outline btn-block">Update Banking Information <i class="fa fa-save"></i></button>
				</form>
			</div>
		</div>
		
		
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Banking Information</h3>
			</div>
			<div class="panel-body">
				<?php
				if (isset($_GET['success'])) {
					if ($_GET['success'] == 'account') {
						echo '
							<div class="alert alert-success alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">
							&times;
							</button>
							<strong>Success! </strong> Your picture updated.
							</div>
						';
					}
				}
				?>
				<form id="submitForm2" method="post" action="../user/updateAccount">
					<div class="form-group">
	                    <label>Bank</label>
	                    <select class="form-control" name="bankaccount">
	                        <option <?=($data['profile']['account_bank']=='Maybank')?'Selected':'';?>>Maybank</option>
	                        <option <?=($data['profile']['account_bank']=='CIMB')?'Selected':'';?>>CIMB</option>
	                        <option <?=($data['profile']['account_bank']=='Hong Leong')?'Selected':'';?>>Hong Leong</option>
	                        <option <?=($data['profile']['account_bank']=='BSN')?'Selected':'';?>>BSN</option>
	                        <option <?=($data['profile']['account_bank']=='Bank Rakyat')?'Selected':'';?>>Bank Rakyat</option>
	                    </select>
	                </div>
	                <div class="form-group">
	                    <label>Bank Account Number</label>
	                    <input class="form-control" type="text" placeholder="" name="accountno" required="required" value="<?=$data['profile']['account_number'] ?>"/>
	                </div>
					<button class="btn btn-success btn-outline btn-block">Update Banking Information <i class="fa fa-save"></i></button>
				</form>
			</div>
		</div>
		
	</div><!-- image update -->
	
	<!-- /.row -->
</div>