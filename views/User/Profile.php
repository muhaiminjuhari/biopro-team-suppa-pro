<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Profile <small><i class="fa fa-user"></i></small></h1>
	</div>
	<!-- /.col-lg-12 -->
</div>
<div class="row">
	<div class="col-lg-12">
		<a href="../user/commision/" class="btn btn-primary btn-outline"><i class="fa fa-user"></i> Profile</a> 
		<a href="../user/commision/" class="btn btn-primary btn-outline"><i class="fa fa-lock"></i> Password</a> 
		<a href="../user/commision/" class="btn btn-primary btn-outline"><i class="fa fa-university"></i> Banking Account</a> 
	</div>
</div>
<hr />
<div class="row">
	<div class="col-lg-3">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Profile Picture</h3>
			</div>
			<div class="panel-body">
				<?php
				if (isset($_GET['success'])) {
					if ($_GET['success'] == 'picture') {
						echo '
							<div class="alert alert-success alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">
							&times;
							</button>
							<strong>Success! </strong> Your picture updated.
							</div>
						';
					}
				}
				?>
				<form id="submitForm3" method="post" action="../user/updatePicture" enctype="multipart/form-data">
					<img src="upload/<?=$data['profile']['picture_name'] ?>" class="img-responsive" />
					<input type="file" name="picture" required="required"/>
					<br>
					<button class="btn btn-success btn-outline btn-block">Change Image <i class="fa fa-save"></i></button>
				</form>
			</div>
		</div>
		
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Banking Information</h3>
			</div>
			<div class="panel-body">
				<?php
				if (isset($_GET['success'])) {
					if ($_GET['success'] == 'account') {
						echo '
							<div class="alert alert-success alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">
							&times;
							</button>
							<strong>Success! </strong> Your picture updated.
							</div>
						';
					}
				}
				?>
				<form id="submitForm2" method="post" action="../user/updateAccount">
					<div class="form-group">
	                    <label>Bank</label>
	                    <select class="form-control" name="bankaccount">
	                        <option <?=($data['profile']['account_bank']=='Maybank')?'Selected':'';?>>Maybank</option>
	                        <option <?=($data['profile']['account_bank']=='CIMB')?'Selected':'';?>>CIMB</option>
	                        <option <?=($data['profile']['account_bank']=='Hong Leong')?'Selected':'';?>>Hong Leong</option>
	                        <option <?=($data['profile']['account_bank']=='BSN')?'Selected':'';?>>BSN</option>
	                        <option <?=($data['profile']['account_bank']=='Bank Rakyat')?'Selected':'';?>>Bank Rakyat</option>
	                    </select>
	                </div>
	                <div class="form-group">
	                    <label>Bank Account Number</label>
	                    <input class="form-control" type="text" placeholder="" name="accountno" required="required" value="<?=$data['profile']['account_number'] ?>"/>
	                </div>
					<button class="btn btn-success btn-outline btn-block">Update Banking Information <i class="fa fa-save"></i></button>
				</form>
			</div>
		</div>
		
		
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Banking Information</h3>
			</div>
			<div class="panel-body">
				<?php
				if (isset($_GET['success'])) {
					if ($_GET['success'] == 'account') {
						echo '
							<div class="alert alert-success alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">
							&times;
							</button>
							<strong>Success! </strong> Your picture updated.
							</div>
						';
					}
				}
				?>
				<form id="submitForm2" method="post" action="../user/updateAccount">
					<div class="form-group">
	                    <label>Bank</label>
	                    <select class="form-control" name="bankaccount">
	                        <option <?=($data['profile']['account_bank']=='Maybank')?'Selected':'';?>>Maybank</option>
	                        <option <?=($data['profile']['account_bank']=='CIMB')?'Selected':'';?>>CIMB</option>
	                        <option <?=($data['profile']['account_bank']=='Hong Leong')?'Selected':'';?>>Hong Leong</option>
	                        <option <?=($data['profile']['account_bank']=='BSN')?'Selected':'';?>>BSN</option>
	                        <option <?=($data['profile']['account_bank']=='Bank Rakyat')?'Selected':'';?>>Bank Rakyat</option>
	                    </select>
	                </div>
	                <div class="form-group">
	                    <label>Bank Account Number</label>
	                    <input class="form-control" type="text" placeholder="" name="accountno" required="required" value="<?=$data['profile']['account_number'] ?>"/>
	                </div>
					<button class="btn btn-success btn-outline btn-block">Update Banking Information <i class="fa fa-save"></i></button>
				</form>
			</div>
		</div>
		
	</div><!-- image update -->
		
	<div class="col-lg-7">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Personal Information</h3>
			</div>
			<div class="panel-body">
				<?php
				if (isset($_GET['success'])) {
					if ($_GET['success'] == 'update') {
						echo '
							<div class="alert alert-success alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">
							&times;
							</button>
							<strong>Success! </strong> Your profile updated.
							</div>
						';
					}
				}
				?>
				<form id="submitForm" role="form" method="post" action="../user/updateProfie">
					<fieldset>
						<div class="form-group">
							<input class="form-control" placeholder="First Name" name="fname" type="text" required="required" value="<?=$data['profile']['fname'] ?>" autofocus>
						</div>
						<div class="form-group">
							<input class="form-control" placeholder="Last Name" name="lname" type="text" required="required" value="<?=$data['profile']['lname'] ?>">
						</div>
						<div class="form-group">
							<input class="form-control" placeholder="E-mail" name="email" type="email" required="required" value="<?=$data['profile']['email'] ?>">
						</div>
						<div class="form-group">
							<input class="form-control" placeholder="Contact Number" name="phone" type="text" required="required" value="<?=$data['profile']['phone'] ?>">
						</div>
                        <div class="form-group">
                        <label>Gender</label>
                        <label class="radio-inline">
                            <input type="radio" name="gender" id="optionsRadiosInline1" value="male" <?=($data['profile']['gender']=='male')?'checked':'';?> >Male
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="gender" id="optionsRadiosInline2" value="female" <?=($data['profile']['gender']=='female')?'checked':'';?> >Female
                        </label>
                    </div>
                    <div class="form-group">
                        <label>Malaysian Citizenship</label>
                        <label class="radio-inline">
                            <input type="radio" name="local" id="optionsRadiosInline1" value="1" <?=($data['profile']['malaysian']=='1')?'checked':'';?> >Yes
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="local" id="optionsRadiosInline2" value="0" <?=($data['profile']['malaysian']=='0')?'checked':'';?> >No
                        </label>
                    </div>
                    <div class="form-group">
                        <label>IC Number/Passport Number</label>
                        <input class="form-control" placeholder="" name="icno" type="text" required="required" value="<?=$data['profile']['icpass']?>">
                    </div>
                    <div class="form-group">
                        <label>Address</label>
                        <input class="form-control" placeholder="Address 1" name="add1" type="text" required="required" value="<?=$data['profile']['address1']?>">    
                    </div>
                    <div class="form-group">
                        <input class="form-control" placeholder="Address 2" name="add2" type="text" value="<?=$data['profile']['address2']?>">
                    </div>
                    <div class="form-group">
                        <input class="form-control" placeholder="City/Town" name="city" type="text" required="required" value="<?=$data['profile']['city']?>">
                    </div>
                    <div class="form-group">
                        <input class="form-control" placeholder="Postcode" name="postcode" type="text" required="required" value="<?=$data['profile']['postcode']?>">
                    </div>
                    <div class="form-group">
                        <input class="form-control" placeholder="State" name="state" type="text" required="required" value="<?=$data['profile']['state']?>">
                    </div>
                    <div class="form-group">
                        <select class="form-control" name="country" required="required">
                            <option value="">Country</option>
                            <option value="MALAYSIA" <?=($data['profile']['country']=='MALAYSIA')?'selected':'';?> >Malaysia</option>
                            <option value="INDONESIA" <?=($data['profile']['country']=='INDONESIA')?'selected':'';?>>Indonesia</option>
                            <option value="THAILAND" <?=($data['profile']['country']=='THAILAND')?'selected':'';?>>Thailand</option>
                            <option value="SINGAPORE" <?=($data['profile']['country']=='SINGAPORE')?'selected':'';?>>Singapore</option>
                            <option value="BRUNEI" <?=($data['profile']['country']=='BRUNEI')?'selected':'';?>>Brunei</option>
                        </select>
                    </div>	
						<button class="btn btn-success btn-outline btn-block">Update Personal Information <i class="fa fa-save"></i></button>
					</fieldset>
				</form>
			</div>
		</div>
	</div><!--profile update -->
	
	
	
	<!-- /.row -->
</div>