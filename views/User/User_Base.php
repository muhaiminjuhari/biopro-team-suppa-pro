<!DOCTYPE html>
<html>

	<head>
		<?=$fixheader ?>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<title>Biopro Admin Dashboard 0.1</title>

		<!--main css load code -->

		<!-- Core CSS - Include with every page -->
		<link href="assets2/css/bootstrap.css" rel="stylesheet">
		<link href="assets2/font-awesome/css/font-awesome.css" rel="stylesheet">

		<!-- Page-Level Plugin CSS - Dashboard -->
		<link href="assets2/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">
		<link href="assets2/css/plugins/timeline/timeline.css" rel="stylesheet">
		<link href="assets2/css/plugins/social-buttons/social-buttons.css" rel="stylesheet">
		<link href="assets2/js/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">
		<link href="assets2/js/plugins/datetimepicker/jquery.datetimepicker.css" rel="stylesheet">
		<link href="assets2/js/plugins/zoom_image/magnific-popup.css" rel="stylesheet">

		<!-- SB Admin CSS - Include with every page -->
		<link href="assets2/css/sb-admin.css" rel="stylesheet">
		<link href="assets2/css/main.css" rel="stylesheet">
		<link href="assets2/css/nazar.css" rel="stylesheet">

	</head>

	<body>

		<div id="wrapper">

			<!-- Navbar Top code -->
			<nav class="navbar navbar-default navbar-static-top navbar-green" role="navigation" style="margin-bottom: 0">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="../admin">BIOPRO SYSTEM 0.1b</a>
				</div>
				<!-- /.navbar-header -->

				<ul class="nav navbar-top-links navbar-right">
					<!-- /.dropdown -->
					<li class="dropdown" >
						<a class="dropdown-toggle" style="color:white;"  data-toggle="dropdown" href="#"> <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i> </a>
						<ul class="dropdown-menu dropdown-user">
							<li>
								<a href="../user/profile"><i class="fa fa-user fa-fw"></i> User Profile</a>
							</li>
							<li>
								<!-- <a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a> -->
							</li>
							<li class="divider"></li>
							<li>
								<a href="../home/logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
							</li>
						</ul>
						<!-- /.dropdown-user -->
					</li>
					<!-- /.dropdown -->
				</ul>
				<!-- /.navbar-top-links -->

			</nav>
			<!-- /.navbar-static-top -->

			<!-- Navbar Side (user) code -->
			<nav class="navbar-default navbar-static-side" role="navigation">
				<div class="sidebar-collapse">
					<ul class="nav" id="side-menu">

						<!--search (could be handy later) -->
						<!-- <li class="sidebar-search">
						<div class="input-group custom-search-form">
						<input type="text" class="form-control" placeholder="Search...">
						<span class="input-group-btn">
						<button class="btn btn-default" type="button">
						<i class="fa fa-search"></i>
						</button>
						</span>
						</div>
						/input-group
						</li>-->
						<li>
							<a href="../user"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
						</li>
						<li>
							<!-- <a href="../user/stock"><i class="fa fa-dropbox"></i> Stock</a> -->
						</li>
						<li>
							<a href="../user/recruit"><i class="fa fa-group"></i> Recruit/Referral</a>
						</li>
						<li>
							<a href="../user/commision"><i class="fa fa-dollar"></i> Commision</a>
						</li>
						<li>
							<a href="../user/profile"><i class="fa fa-user"></i> Profile</a>
						</li>
						<!--nav link second level -->
						<!--
						<li>
						<a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Charts<span class="fa arrow"></span></a>
						<ul class="nav nav-second-level">
						<li>
						<a href="flot.html">Flot Charts</a>
						</li>
						<li>
						<a href="morris.html">Morris.js Charts</a>
						</li>
						</ul>
						/.nav-second-level
						</li>
						-->
					</ul>
					<!-- /#side-menu -->
				</div>
				<!-- /.sidebar-collapse -->
			</nav>
			<!-- /.navbar-static-side -->

			<div id="page-wrapper">
				<!-- Load Content Here -->
				<?php
				require $content1;
				?>
			</div>
			<!-- /#page-wrapper -->

		</div>
		<!-- /#wrapper -->

		<!-- Core Scripts - Include with every page -->
		<script src="assets2/js/jquery-1.10.2.js"></script>
		<script src="assets2/js/bootstrap.min.js"></script>
		<script src="assets2/js/plugins/metisMenu/jquery.metisMenu.js"></script>

		<!-- Page-Level Plugin Scripts - Dashboard -->
		<script src="assets2/js/plugins/morris/raphael-2.1.0.min.js"></script>
		<script src="assets2/js/plugins/morris/morris.js"></script>

		<!-- SB Admin Scripts - Include with every page -->
		<script src="assets2/js/sb-admin.js"></script>

		<!--Validation-->
		<script src="assets2/js/jquery.validate.js"></script>
		<script>
			$("#submitForm").validate();
			$("#submitForm2").validate({
				rules : {
					password : "required",
					repassword : {
						equalTo : "#password"
					}
				}
			});
			$("#submitForm3").validate();
		</script>

		<script type="text/javascript">
			function calculateTotal() {
				var totalAmt = document.addem.perbox.value;
				totalR = eval(totalAmt * document.addem.total.value);

				document.getElementById('update').innerHTML = totalR.toFixed(2);
				document.getElementById('update2').innerHTML = totalR.toFixed(2);

				document.ab.total2.value = document.addem.total.value;
				document.ab.payment.value = totalR.toFixed(2);
			}
		</script>

		<script>
			$("#imgInp").change(function() {
				readURL(this);
			});

			function readURL(input) {

				if (input.files && input.files[0]) {
					var reader = new FileReader();

					reader.onload = function(e) {
						$('#blah').attr('src', e.target.result);
					}

					reader.readAsDataURL(input.files[0]);
				}
			}
		</script>

		<!-- DataTables-->
		<script src="assets2/js/plugins/dataTables/jquery.dataTables.js"></script>
		<script src="assets2/js/plugins/dataTables/dataTables.bootstrap.js"></script>

		<!--  Holder.js    -->
		<script src="assets2/js/holder.js"></script>

		<!--  datetimepicker.js    -->
		<script src="assets2/js/plugins/datetimepicker/jquery.datetimepicker.js"></script>
		<script>
			jQuery('#datetimepicker').datetimepicker({
				timepicker : false,
				format : 'd.m.Y'
			});
			jQuery('#datetimepicker2').datetimepicker({
				datepicker : false,
				format : 'H:i'
			});
		</script>

		<!--  z.js    -->
		<script src="assets2/js/plugins/zoom_image/jquery.magnific-popup.js"></script>
		<script>
			$('.popup_image').magnificPopup({
				type : 'image',
				closeOnContentClick : true,
				closeBtnInside : false,
				fixedContentPos : true,
				mainClass : 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
				image : {
					verticalFit : true
				},
				zoom : {
					enabled : true,
					duration : 300 // don't foget to change the duration also in CSS
				}
				// other options
			});
		</script>

		<script>
			$(document).ready(function() {
				$('#dataTables-example').dataTable();
			});
		</script>

	</body>

</html>
