<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Commission <small><i class="fa fa-dollar"></i></small></h1>
	</div>
</div>
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Commission Info</h3>
			</div>
			<div class="panel-body">
				<div class="col-lg-3">
					<p>
						<span class="label label-success">Current Commission</span>
					</p>
					<h1>RM <?=number_format((float)$data['total'], 2, '.', ''); ?></h1>
				</div>
				<!-- <div class="col-lg-3">
					<p>
						<span class="label label-success">Next date comission paid</span>
					</p>
					<h1>15/5/2014</h1>
				</div> -->
			</div>
			<div class="panel-footer">
				<a href="../user/commissionLog" class="btn btn-primary btn-outline">View Commission Detalis <i class="fa fa-arrow-right"></i></a>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Commission History</h3>
			</div>
			<div class="panel-body">
				<table class="table table-striped table-bordered table-hover" id="dataTables-example">
					<thead>
						<tr>
							<th>Date</th>
							<th>Total Commission (RM)</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php
						foreach($data['commissionList']  as $com){
						?>
						<tr>
							<td><?= date('1 F Y', strtotime($com['date_getCommission'])); ?></td>
                            <td><?=number_format((float)$com['total_commission'], 2, '.', ''); ?></td>
							<td><a href="../user/commissionLog/<?= $com['id']; ?>?month=<?= date('m-Y', strtotime($com['date_getCommission'])); ?>" class="btn btn-success">View</a></td>
						</tr>
						<?php
						}
						?>
					</tbody>
				</table>
			</div>
			<!-- <div class="panel-footer">
				<a href="" class="btn btn-primary btn-outline">Print List <i class="fa fa-arrow-right"></i></a>
			</div> -->
		</div>
	</div>
</div>