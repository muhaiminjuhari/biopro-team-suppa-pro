<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Stock <small><i class="fa fa-dropbox"></i> List</small></h1>
	</div>
</div>
<div class="row">
	<div class="col-lg-12">
		<a href="../user/stock" class="btn btn-outline btn-success"> Stock List</a>
		<a href="../user/stockBuy" class="btn btn-outline btn-success"> Buy Stock</a>
		<a href="../user/stockTransaction" class="btn btn-outline btn-success"> Pending Transactions</a>
	</div>
</div>
<br/>
<div class="row">
	<div class="col-lg-4">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">My Ordered Stock</h3>
			</div>
			<div class="panel-body">
				<h1><?=$data['mystock'] ?>
				<small><i class="fa fa-dropbox"></i> boxes</small></h1>
				<p></p>
			</div>
		</div>
	</div>
	<div class="col-lg-4">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Stock Order from Agent</h3>
			</div>
			<div class="panel-body">
				<h1><?=$data['mystock'] ?>
				<small><i class="fa fa-dropbox"></i> boxes</small></h1>
				<p></p>
			</div>
		</div>
	</div>
	<div class="col-lg-4">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Total Stock</h3>
			</div>
			<div class="panel-body">
				<p>This is the total stock that you have to pickup</p>
				<h1><?=$data['mystock'] ?>
				<small><i class="fa fa-dropbox"></i> boxes</small></h1>
				<p></p>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-lg-12">
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-primary">
					<div class="panel-heading">
						Purchase History
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-12">
								<div class="well hidden">
									<div class="form-inline">
										<div class="form-group">
											<input class="form-control" placeholder="Enter Product ID.." />
											<a href="" class="btn btn-primary"> <i class="fa fa-search"> Search</i></a>
										</div>
										<div class="form-group pull-right">
											<a href="javascript:;" class="btn btn-primary btn-outline"> Print list <i class="fa fa-arrow-right"></i></a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12">
								<table class="table table-striped table-bordered table-hover" id="dataTables-example">
									<thead>
										<tr>
											<th>Timestamp Purchased</th>
											<th>Amount <i class="fa fa-dropbox"></i></th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>20:20 12/12/2014</td>
											<td> 20 </td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="panel-footer">
						<a href="" class="btn btn-primary btn-outline"> Print list <i class="fa fa-arrow-right"></i></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /.row -->

