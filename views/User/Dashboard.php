<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Hi, <small><?=$data['profile']['fname']. ' ' . $data['profile']['lname'] ?></small></h1>
	</div>
	<!-- /.col-lg-12 -->
</div>
<div class="row">
	<div class="col-lg-12">
		<div class="jumbotron">
			<h1>Expand your network</h1>
			<p>
                Share your referral code in social media, help friends and family become Biopro members and receive commission. Save and Earn now!
			</p>
<!--            <div class="alert alert-warning">We are now fixing these social share links, please bear with us for a while.</div>-->
			<a href="https://www.facebook.com/sharer/sharer.php?u=<?=$data['linkshare'].$data['profile']['referral_code'];?>" class="btn btn-social btn-facebook" onclick="javascript:window.open(this.href,
  '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><i class="fa fa-facebook"></i> Facebook</a>
			<a href="https://twitter.com/share?url=<?=$data['linkshare'].$data['profile']['referral_code'];?>" class="btn btn-social btn-twitter" onclick="javascript:window.open(this.href,
  '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><i class="fa fa-twitter"></i> Twitter</a>
			<a href="https://plus.google.com/share?url=<?=$data['linkshare'].$data['profile']['referral_code'];?>" class="btn btn-social btn-google-plus" onclick="javascript:window.open(this.href,
  '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><i class="fa fa-google-plus"></i> Google+</a>
			<br>
			<br>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">Referral Code </h4>
				</div>
				<div id="" class="panel-collapse">
					<div class="panel-body">
                        <h3>Your referral link</h3>
						
                        <span class="bg-success">
                            <p>
<!--                                    Sorry! We are now fixing the link, for now please use the Referral Code below.-->
                                <a href="<?=$data['linkshare'].$data['profile']['referral_code'];?>"><?=$data['linkshare'].$data['profile']['referral_code'];?></a>
				                
                                <br><span style="font-style:italic";><small>Copy this link and share it to prospective members.</small></span>
                            </p>
                        </span>
                        
                        <h3>Your Referral Code</h3>
						<p>
                            <?=$data['profile']['referral_code'];?>
                            <br/>
                            <span style="font-style:italic"><small>Alternatively, you can share your referral code too.</small></span>
                        </p>
                        
                        <div class="alert alert-success">
                            <p><b>Pro tip:</b> 
                                Referral code will be used by new member to join Biopro as an agent. If a member register with your 
                                Referral Code, he will be your recruit and you are eligible to earn commission based on repeat 
                                sales by your recruits.
                            </p>
                        </div>
					</div>
				</div>
               
			</div>
             <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Promotional Website</h3>
                    </div>
                    <div class="panel-body">
                        <p>Below is the link of Biopro Fuel Saver Promotional web page, some people will need a little more convincing before joining. Show the site to them, by sharing the link below!</p>
                        <br/><a href="http://biopropetrol.com">Biopro Promotional Website</a></p>
                        
                        <p>For membership registration, share the link below.</p>
                        <br/><a href="http://biopropetrol.com/home/registerPre">Biopro Membership Registration Page</a></p>
                    </div>
                </div>
		</div>
	</div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="col-lg-6">
            <div class="row">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Stok
                    </div>
                    <div class="panel-body">
                        <div class="well">
                            <p>Stok Terkini</p>
                            <h1>0 <small>Boxes <i class="fa fa-dropbox"></i></small></h1>
                        </div>
                        <div class="well">
                            <p>Pending transactions</p>
                            <h1>0 <small>Boxes <i class="fa fa-dropbox"></i></small></h1>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <a href="../user/stock" class="btn btn-primary btn-outline">Lebih lanjut <i class="fa fa-arrow-right"></i></a>
                    </div>
                </div>
            </div>
            <div class="row">           
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Rekrut
                    </div>
                    <div class="panel-body">
                        <h3>Total Rekrut : <?=$data['firstCount']+$data['secondCount'];?></h3>
                        <hr/>
                        <p>
                            Rekrut level 2 : <?=$data['firstCount'];?><br />
                            Rekrut level 3 : <?=$data['secondCount'];?>
                        </p>
                    </div>
                    <div class="panel-footer">
                       <a href="../user/recruit" class="btn btn-primary btn-outline">Lebih lanjut <i class="fa fa-arrow-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Komisyen
                </div>

                <div class="panel-body">
                    <div class="well">
                        <p>Bulan ini</p>
                        <hr/>
                        <h1><small>RM</small> 0.00</h1>
                    </div>
                    <div class="well">
                        <p>Bulan lalu</p>
                        <hr/>
                        <h1><small>RM</small> 0.00</h1>
                    </div>
                </div>
                <div class="panel-footer">
                    <a href="../user/commission" class="btn btn-primary btn-outline">Lebih lanjut <i class="fa fa-arrow-right"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.row -->