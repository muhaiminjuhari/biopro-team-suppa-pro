<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Commission</h1>
	</div>
</div>
<div class="row">
	<div class="col-lg-12">
		<a href="../user/commision/" class="btn btn-primary btn-outline">Commission</a>
		<hr />
	</div>
</div>
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Commission Info</h3>
			</div>
			<div class="panel-body">
				<div class="col-lg-3">
					<p>
						<span class="label label-success">Current Commission</span>
					</p>
					<h1>RM <?=number_format((float)$data['total'], 2, '.', ''); ?></h1>
				</div>
				<!-- <div class="col-lg-3">
					<p>
						<span class="label label-success">Next date comission paid</span>
					</p>
					<h1>15/5/2014</h1>
				</div> -->
			</div>
			<div class="panel-footer">
				<a href="../user/commissionLog" class="btn btn-primary btn-outline">View Commission Detalis <i class="fa fa-arrow-right"></i></a>
			</div>
		</div>
	</div>
</div>
<!-- <div class="row">
<div class="col-lg-12" align="center">
<div class="panel panel-success">
<div class="panel-heading">
<h3 class="panel-title">You</h3>
</div>
<div class="panel-body">
<h3>Username</h3>
</div>
</div>
<a href="" class="btn btn-default"> <i class="fa fa-arrow-down"></i></a>
</div>
</div>
<br/> -->
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-success">
			<div class="panel-heading">
				<h3 class="panel-title">Commission List</h3>
			</div>
			<div class="panel-body">
				<div class="">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th>Date</th>
                                <th>Detail</th>
                                <th>Total</th>
                                <th>From</th>
							</tr>
						</thead>
						<tbody>
							<?php
                        	foreach($data['log']as $log){
                        	?>
                            <tr>
                                <td><?= $log['date_log']; ?></td>
                                <td><?= $log['detail']; ?></td>
                                <td>RM <?=number_format((float)$log['total'], 2, '.', ''); ?></td>
                                <td><?=$log['fname'] . ' ' . $log['lname']; ?></td>
                            </tr>
                            <?php
							}
                            ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- <div class="row" align="center">
<a href="" class="btn btn-default"> <i class="fa fa-arrow-down"></i></a>
</div> -->
<!-- <br/>
<div class="row">
<div class="col-lg-12">
<div class="panel panel-success">
<div class="panel-heading">
<h3 class="panel-title">Level
<button class="btn btn-circle btn-outline btn-success">
3
</button></h3>
</div>
<div class="panel-body">
<div class="">
<table class="table table-bordered">
<thead>
<tr>
<th>Name</th>
<th>Commission</th>
<th>Date</th>
</tr>
</thead>
<tbody>
<tr>
<td>Shafwan</td>
<td>RM 30.00</td>
<td>12-12-2014</td>
</tr>
<tr>
<td>Nazar</td>
<td>RM 20.00</td>
<td>12-12-2014</td>
</tr>
</tbody>
</table>
</div>
</div>
</div>
</div>
</div>
<div class="row">
<div class="col-lg-12">
<hr/>
<h2 class="pull-right"><small>Total Commission:</small> RM <span>330.00</span></h2>
<hr/>
</div>
</div> -->