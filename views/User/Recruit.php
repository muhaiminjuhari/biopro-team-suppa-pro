<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Recruits <small><i class="fa fa-users"></i></small></h1>
	</div>
</div>

<div class="row">
	<div class="col-lg-12">
		<h3 class="page-header">1st Level Recruits [<?=$data['firstCount'];?>]</h3>
	</div>
</div>

<div class="row">
	<div class="col-lg-12">
		<table class="table table-striped table-bordered table-hover" id="dataTables-example">
			<thead>
				<tr>
					<th>Fullname</th>
					<th>Email</th>
					<th>Registration timestamp</th>
				</tr>
			</thead>
			<tbody>
				<?php
				foreach($data['firstTree'] as $first){
				?>
				<tr>
					<td><?=$first['fname'].' '.$first['lname']?></td>
					<td><?=$first['email'] ?></td>
					<td><?=$first['time_register'] ?></td>
				</tr>
				<?php
				}
				?>
			</tbody>
		</table>
	</div>
</div>

<div class="row">
	<div class="col-lg-12">
		<h3 class="page-header">2nd Level Recruits [<?=$data['secondCount'];?>]</h3>
	</div>
</div>

<div class="row">
	<div class="col-lg-12">
		<table class="table table-striped table-bordered table-hover" id="dataTables-example">
			<thead>
				<tr>
					<th>Fullname</th>
					<th>Email</th>
					<th>Registration timestamp</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<?php
					foreach($data['secondTree'] as $second){
					?>
					<tr>
						<td><?=$second['fname'].' '.$second['lname']?></td>
						<td><?=$second['email'] ?></td>
						<td><?=$second['time_register'] ?></td>
					</tr>
					<?php
					}
					?>
				</tr>
			</tbody>
		</table>
	</div>
</div>
