<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Stock <small><i class="fa fa-dropbox"></i> Pending Transactions</small></h1>
	</div>
</div>
<div class="row">
	<div class="col-lg-12">
		<a href="../user/stock" class="btn btn-outline btn-success"> Stock List</a>
		<a href="../user/stockBuy" class="btn btn-outline btn-success"> Buy Stock</a>
		<a href="../user/stockTransaction" class="btn btn-outline btn-success"> Pending Transactions</a>
	</div>
</div>
<br/>
<div class="row">
	<div class="col-lg-12">
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						Pending Transactions
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-12">
								<div class="well hidden">
									<div class="form-inline">
										<div class="form-group">
											<input class="form-control" placeholder="Enter Product ID.." />
											<a href="" class="btn btn-primary"> <i class="fa fa-search"> Search</i></a>
										</div>
										<div class="form-group pull-right">
											<a href="javascript:;" class="btn btn-primary btn-outline"> Print list <i class="fa fa-arrow-right"></i></a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12">
								<table class="table table-striped table-bordered table-hover" id="dataTables-example">
									<thead>
										<tr>
											<th>Timestamp Purchased</th>
											<th>Amount <i class="fa fa-dropbox"></i></th>
											<th>Total Bank in(RM)</th>
											<th>Receipt (click image to inlarge)</th>
										</tr>
									</thead>
									<tbody>
										<?php
										foreach($data['pendingTransaction'] as $key){
										?>
											<tr>
												<td><?= date('g:i a', strtotime($key['time_payment'])) ;?> / <?= date("d-M-Y", strtotime($key['date_payment'])) ;?></td>
												<td> <?=$key['stock_quantity']; ?> </td>
												<td><?=$key['amount_payment'] ?></td>
												<td>
													<!-- <img src="holder.js/200x120"> -->
													<a href="../views/upload/receipt/<?=$key['pic_slip'] ?>" class="popup_image"><img width="200px" height="120px" class="" src="../views/upload/receipt/<?=$key['pic_slip'] ?>" /></a>
												</td>
											</tr>
										<?php
										}
										?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="panel-footer">
						<a href="" class="btn btn-primary btn-outline"> Print list <i class="fa fa-arrow-right"></i></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

