<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Stock <small><i class="fa fa-dropbox"></i> Buy</small></h1>
	</div>
</div>
<div class="row">
	<div class="col-lg-12">
		<a href="../user/stock" class="btn btn-outline btn-success"> Stock List</a>
		<a href="../user/stockBuy" class="btn btn-outline btn-success"> Buy Stock</a>
		<a href="../user/stockTransaction" class="btn btn-outline btn-success"> Pending Transactions</a>
	</div>
</div>
<br/>
<?php
if($data['stockAvailable'] == 'TRUE'){
?>
<div class="row">
	<div class="col-lg-6">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">
				<button class="btn btn-outline btn-circle btn-success">
					1
				</button> Specify the quantity</h3>
			</div>
			<div class="panel-body">
				<form id="submitForm3" name="addem" class="form-inline">
					<fieldset>
						<div class="form-group">
							<p>
								Please specify the quantity of stock you want to buy below. This will help you to know the total of payment.
							</p>
							<div class="col-lg-6">
				                <input id="total" class="form-control" onkeyup="calculateTotal()" max="<?=$data['currentBioProStock']; ?>" placeholder="" name="total" type="number" min="1"  style="width:220px;" required="required" autofocus />
							</div>
							<div class="col-lg-6">
				            <!-- <button class="btn btn-primary">calculate</button> -->
							<input type="hidden" name="perbox" value="<?=$data['pricing']['perstock'] ?>" />
							<span style="font-size:20px;">x <i class="fa fa-dropbox"></i></span>
							<span style="font-size:20px;">= RM<span id="update"> <small>Total</small> </span></span>
							</div>
						</div>
						<br/>
						<br/>
						<div class="form-group hidden">
							<button class="btn btn-success btn-outline">
								Next <i class="fa fa-arrow-right"></i>
							</button>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
	<div class="col-lg-3">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">
                    <button class="btn btn-outline btn-circle btn-success"><i class="fa fa-dropbox"></i></button> BioPetrol Stock </h3>
			</div>
			<div class="panel-body">
				<p>
					This is the current stock from Biopetrol
				</p>
				<h1><?=$data['currentBioProStock']; ?>
				<small><i class="fa fa-dropbox"></i> boxes</small></h1>
			</div>
		</div>

	</div>
</div>
<div class="row">
	<div class="col-lg-9">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">
				<button class="btn btn-outline btn-circle btn-success">
					2
				</button> Bank-in the payment</h3>
			</div>
			<div class="panel-body">
				<p>
					Now you have to visit your preffered bank and bank in the specified amount RM <span id="update2"> <small>Total</small> </span>. <span class="text-info">REMEMBER to save the transaction slip (bank-in slip) or receipt</span>. For user who pay from online banking, you can use screen shot.
					Please bank in this bank account:
				</p>
				<div class="row">
					<div class="col-lg-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title">CIMB</h3>
							</div>
							<div class="panel-body">
								Account No: <b>8006947073</b>
								<br />
								<hr />
								Holder Name: <b>Biopro Petrol Global Marketing Sdn. Bhd</b>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<form id="submitForm" method="post" action="../user/uploadPayment" enctype="multipart/form-data" name="ab">
	<div class="row">
		<div class="col-lg-9">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">
					<button class="btn btn-outline btn-circle btn-success">
						3
					</button> Upload transaction slip or receipt</h3>
				</div>
				<div class="panel-body">
					<div class="form">
						<p>
							The last step is easy, you need to upload the transaction slip (bank-in slip) or receipt below.
							We will process your payment and notify you shortly.
						</p>
						<div class="form-group">
							<label>Select Payment made from</label>
							<select name="fromBank" class="form-control"  style="width:220px;">
								<option value="Maybank">Maybank</option>
								<option value="CIMB">CIMB</option>
								<option value="Hong Leong">Hong Leong</option>
								<option value="Bank Islam">Bank Islam</option>
								<option value="Other">Other Bank</option>
							</select>
						</div>
						<div class="form-group">
							<label>If made payment from 'Other Bank' please state here:</label>
							<input name="other" type="text" class="form-control" style="width:220px;" placeholder="Other Bank">
						</div>
						<div class="form-group">
							<label>Select Payment made to</label>
							<select name="toBank" class="form-control"  style="width:220px;">
								<!-- <option value="Maybank">Maybank - 716523812893793 - Biopro Petrol Global Marketing Sdn. Bhd</option> -->
								<option value="CIMB">CIMB - 8006947073 - Biopro Petrol Global Marketing Sdn. Bhd</option>
								<!-- <option value="Hong Leong">Hong Leong - 716523812893793 - Biopro Petrol Global Marketing Sdn. Bhd</option>
								<option value="Other">Bank Islam - 716523812893793 - Biopro Petrol Global Marketing Sdn. Bhd</option> -->
							</select>
						</div>
						<div class="form-group">
							<label>Verify amount of Payment</label>
							<div class="input-group" style="width:220px;">
								<span class="input-group-addon">RM</span>
								<input name="payment" type="text" class="form-control" placeholder="Amount of Payment">
							</div>
						</div>
						<div class="form-group">
							<label>Date of Payment</label>
							<!-- <br /> -->
							<!-- <small>This is important because sometimes we cannot see the picture clearly</small> -->
							<!-- <br /> -->
							<input id="datetimepicker" name="dateInSlip" type="text" class="form-control" style="width:220px;" placeholder="Date" required="required">
						</div>
						<div class="form-group">
							<label>Time of Payment</label>
							<input id="datetimepicker2" name="timeInSlip" type="text" class="form-control" style="width:220px;" placeholder="Time" required="required">
							<small>You can put minutes by changing manually 2 number at the end	, example: 14:12</small>
						</div>
						<div class="form-group">
							<label>Upload your receipt or transaction slip</label>
							<input id="imgInp" name="imageupload" type="file" required="required"/>
							<small>You also can upload print screen</small>
							<br />
							<img id="blah" src="holder.js/200x120" alt="Picture Preview" class="img-thumbnail"/>
						</div>
						<div class="form-group form-inline">
							<label>Specify the stock quantity</label>
							<p>
								Please enter the amount of stock you purchase.
							</p>
							<input id="total2" name="amount" type="number" placeholder="Enter amount.." min="1" max="<?=$data['currentBioProStock']; ?>" class="form-control" style="width:220px;" required="required"/>
							<span style="font-size:20px;"><i class="fa fa-dropbox"></i></span>
							<label for="total2" class="error"></label>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-9">
			<div class="panel panel-default">

				<div class="panel-body">
					<button class="btn btn-block btn-primary">
						<i class="fa fa-check"></i> Submit
					</button>
				</div>
			</div>
		</div>
	</div>
</form>
<!--form-->

<?php
}else{	
?>
<div class="row">
	<div class="col-lg-9">
		<div class="panel panel-default">

			<div class="panel-body">
				Sorry. Stock is not available.
			</div>
		</div>
	</div>
</div>
<?php
}
?>

