<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" lang="en-US" prefix="og: http://ogp.me/ns#">
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" lang="en-US" prefix="og: http://ogp.me/ns#">
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html lang="en-US">
	<!--<![endif]-->
	<head>
		<?=$fixheader; ?>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width">
		<title>BIOPRO FUEL SAVER - Jimat lebih minyak!.</title>
		<link rel="shortcut icon" href="assets/images/biopro-box.png" />

		<link rel='stylesheet' id='contact-form-7-css'  href='assets/css/styles.css' type='text/css' media='all' />
		<link rel='stylesheet' id='theme_fonts-css'  href='assets/fonts/fonts.css' type='text/css' media='all' />
		<link rel='stylesheet' id='theme_stylesheet-css'  href='assets/css/style.css' type='text/css' media='all' />
		<link rel='stylesheet' href='assets/css/label.min.css' type='assets/text/css' media='all' />

		<script type='text/javascript' src='assets/js/jquery/jquery.js'></script>
		<script type='text/javascript' src='assets/js/jquery/jquery-migrate.min.js'></script>

		<body class="home page page-id-62 page-template page-template-index-php">
            <a href="../home/buy"><div class="side-banner"></div></a>
			<div class="site-container">

				<header class="m-site-head" role="banner">

					<div class="container">

						<span class="logo"> <a href="../home/"><img class="top-logo" src="assets/images/bioprologo-mini.png" alt="Biopro - Save More Fuel" data-svg-replacement="assets/images/header-logo.png"/></a> </span>

						<nav class="m-site-nav l-right">
							<ul class="m-inline-list">
								<li id="menu-item-33" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-33 list-item">
									<a href="#biopro">Apakah itu Biopro?</a>
								</li>
<!--
								<li id="menu-item-32" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-32 list-item">
									<a href="#inventor" >Inventor</a>
								</li>
-->
								<li id="menu-item-27" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-27 list-item">
									<a href="../home/registerPre">Daftar Ahli</a>
								</li>
								<li id="menu-item-26" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-26 list-item">
									<a href="#statistik">Statistik</a>
								</li>
								<li id="menu-item-30" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-26 list-item">
									<a href="#mengenai">Mengenai Kami</a>
								</li>
								<li id="menu-item-29" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-26 list-item">
									<a href="../home/contact">Hubungi Kami</a>
								</li>
                                <li id="menu-item-29" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-26 list-item">
									<a href="../home/login">Login</a>
								</li>
							</ul>
						</nav>

						<!-- Mobile responsive navigation menu -->
						<label class="mobile-nav-label" for="mobile-nav-checkbox">&#9776;</label>
						<input class="mobile-nav-checkbox" id="mobile-nav-checkbox" type="checkbox"/>

						<div class="mobile-nav">
							<ul class="t-grid-4 no-padding no-style p-grid-12">
								<li id="menu-item-249" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-249 list-item">
									<a href="" >Link 1</a>
								</li>
								<li id="menu-item-248" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-248 list-item">
									<a href="" >Link 2</a>
								</li>
							</ul>
							<ul class="t-grid-5 no-padding no-style p-grid-12">
								<!--<p class="t-grid-4">What is Carbon Capture?</p>-->
								<div class="t-grid-8">
									<li id="menu-item-59" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-59 list-item">
										<a href="" >About Us</a>
									</li>
									<li id="menu-item-55" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-55 list-item">
										<a href="" >Link 2</a>
									</li>
									<li id="menu-item-53" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-53 list-item">
										<a href="" >Link 3</a>
									</li>
								</div>
							</ul>
							<ul class="t-grid-3 no-padding no-style p-grid-12">
								<li id="menu-item-457" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-457 list-item">
									<a href="">Case Study</a>
								</li>
								<li id="menu-item-57" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-57 list-item">
									<a href="" >About Us</a>
								</li>
								<li id="menu-item-56" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-56 list-item">
									<a href="" >Contact Us</a>
								</li>
							</ul>

							<!--
							<span class="social-links">

							<li class="m-social-link list-item">
							<a href="http://www.linkedin.com/company/the-carbon-tree" target="_blank">
							<i class="fa fa-linkedin">
							<img class="social-icon" src="http://www.thecarbontree.com/wp-content/themes/carbontree/public/images/linkedin-logo.png" />
							</i>
							</a>
							</li>
							<li class="m-social-link list-item">
							<a href="http://twitter.com/TheCarbonTree" target="_blank">
							<i class="fa fa-twitter">
							<img class="social-icon" src="http://www.thecarbontree.com/wp-content/themes/carbontree/public/images/twitter-logo.png" />
							</i>
							</a>
							</li>

							</span>
							-->

						</div>

					</div>

				</header>
				<section class="m-hero m-slider">
					<ul class="slides">
						<!---------- ADD CONTENTS TO THE SLIDER ---------->
						<li>
							<!---------- ADD IMAGE ---------->
							<div class="slide-image" style="background-image:url('assets/images/1.jpg');"></div>

							<div class="container">
								<div class="slide-caption">

									<!---------- ADD OPTIONAL HEADING ---------->
									<h4 class="h1 heading">Biopro Fuel Saver: Pilihan bijak penjimatan petrol</h4>

									<!---------- ADD OPTIONAL SUB-HEADING ---------->
									<div class="m-text-surround">
										<p>
											Memberikan lebih kilometer. Meningkatkan prestasi kenderaan. Penggunaan nisbah yang kecil. Diakui diseluruh dunia!
										</p>
									</div>

									<!---------- ADD OPTIONAL LINK ---------->
									<a class="m-btn" href="#biopro">Ketahui lebih lanjut</a>
								</div>
							</div>
						</li>
						<li>
							<!---------- ADD IMAGE ---------->
							<div class="slide-image" style="background-image:url('assets/images/2.jpg');"></div>

							<div class="container">
								<div class="slide-caption">

									<!---------- ADD OPTIONAL HEADING ---------->
									<h4 class="h1 heading">Menurunkan kadar penggunaan petrol sehingga 30%</h4>

									<!---------- ADD OPTIONAL SUB-HEADING ---------->
									<div class="m-text-surround">
										<p>
											Juga sehingga 16% penjimatan perbelanjaan petrol!
										</p>
									</div>

									<!---------- ADD OPTIONAL LINK ---------->
									<a class="m-btn" href="#statistik">Ketahui lebih lanjut</a>

								</div>
							</div>
						</li>
						<li>
							<!---------- ADD IMAGE ---------->
							<div class="slide-image" style="background-image:url('assets/images/3.jpg');"></div>

							<div class="container">
								<div class="slide-caption">

									<!---------- ADD OPTIONAL HEADING ---------->
									<h4 class="h1 heading">Pengedar dan Ejen Diperlukan!</h4>

									<!---------- ADD OPTIONAL SUB-HEADING ---------->
									<div class="m-text-surround">
										<p>
											Berminat memulakan perniagaan sendiri? Kami mencari pengedar dan ejen jualan diseluruh negara. Nikmati sehingga 55% keuntungan jualan!
										</p>
									</div>

									<!---------- ADD OPTIONAL LINK ---------->
									<a class="m-btn" href="#sertai">Ketahui lebih lanjut</a>
                                    <a class="m-btn" href="../home/registerPre">Daftar sekerang</a>
								</div>
							</div>
						</li>
					</ul>
					<!---------- SLIDER CONTROLS ---------->
					<div class="m-slider-controls-container">
						<div class="container">
							<div class="m-slider-controls"></div>
						</div>
					</div>

				</section>

				<section id="" style="display:none;"> 
					<div class=" m-site-section white-bg">

						<div class="container">

							<div class="box heading l-grid-4 t-grid-6">
								<h4 class="h1 m-text-background"> Biopro Fuel Saver </h4>
							</div>

							<div class="box image l-grid-4 t-grid-6">
								<img class="" src="assets/images/biopro-img2.png" alt="" style="width:320px; margin-left:-40px;" />
							</div>

							<div class="box cta l-grid-4 t-grid-12 third-box">
								<p>
									Biopro Fuel Saver (BFS) ialah bahan
									penambahbaikan baharu untuk meningkatkan prestasi
									kereta. Bahan penambahbaikan ini terbukti
									meningkatkan jangka hayat enjin dan memberi
									kelebihan seperti berikut

								</p>
								<ul>
									<li>
										Penjimatan bahan api yang lebih baik
									</li>
									<li>
										Meningkatkan prestasi kenderaan
									</li>
									<li>
										Sesuai untuk semua kenderaan petrol
									</li>
									<li>
										Hanya sebahagian kecil diperlukan [nisbah 1:100]
									</li>
									<li>
										Mesra alam
								</ul>
								<!-- <a href="" class="m-btn"> Save More Fuel! </a> -->
							</div>

						</div>
					</div>
				</section>

				<section style="background: url('assets/images/unsplashNEW.jpg') no-repeat center/cover;" id="biopro">
					<div class=" m-site-section green-bg-transparent" style="padding-top:0px; padding-bottom:0px; margin-bottom:-5px;">

						<div class="container">
							<div class="l-grid-4 t-grid-6 fill-up">
								<img class="" src="../views/assets2/assets/images/produk1.jpg" data-svg-replacement="" alt="" />
							</div>

							<div class="box cta l-grid-6 t-grid-6">
                                <br/>
								<h4 class="h1 m-text-background">BIOPRO FUEL SAVER</h4>
								<p>
									Biopro Fuel Saver (BFS) ialah bahan
									penambahbaikan baharu untuk meningkatkan prestasi
									kereta. Bahan penambahbaikan ini terbukti
									meningkatkan jangka hayat enjin dan memberi
									kelebihan seperti berikut
								</p>
                                <ul>
									<li>
										Penjimatan bahan api yang lebih baik
									</li>
									<li>
										Meningkatkan prestasi kenderaan
									</li>
									<li>
										Sesuai untuk semua kenderaan petrol
									</li>
									<li>
										Hanya sebahagian kecil diperlukan [nisbah 1:100]
									</li>
									<li>
										Mesra alam
                                    </li>
								</ul>
							
							</div>
                            <div class=" l-grid-4 t-grid-6">
                                
                            </div>
						</div>
					</div>
				</section>

				<section>
					<div class=" m-site-section white-bg">

						<div class="container">

							<div class="box heading l-grid-4 t-grid-6">
								<h4 class="h1 m-text-background"> 1:100 Ratio </h4>
								<h4 class="m-text-background">Mekanisma Campuran</h4>
							</div>

							<div class="box image l-grid-4 t-grid-6">
								<img class="" src="assets/images/mekanisma.png" alt="" />
							</div>

							<div class="box cta l-grid-4 t-grid-12 third-box">
								<p>
									Formula baru campuran Biopro Fuel Saver menghasilkan
									kualiti bahan api yang baharu. Campuran bahan api baharu ini
									membuatkan petrol menghasilkan kurang bau, meningkatkan
									ketumpatan bahan api dan ash point bahan api. Bahan api kurang
									bau dapat mengurangkan masalah pencemaran udara manakala
									bahan api dengan ketumpatan tinggi dan ash point tinggi akan
									memberikan lebih lama masa untuk bahan api terbakar sepenuhnya.
									Oleh itu, prestasi bahan api akan meningkat dengan menghasilkan
									lebihan kilometer sebanyak <strong>30%.</strong>
									<b></b>
								</p>
								<!-- <a href="" class="m-btn"> We are looking for you! </a> -->
							</div>

						</div>
					</div>
				</section>

				<section id="sertai">
					<div class=" m-site-section green-bg">

						<div class="container">

							<div class="box image l-grid-4 t-grid-6">
								<img class="svg-replace" src="assets/images/Homepage_3-.svg" alt="" />
							</div>

							<div class="box cta l-grid-4 t-grid-6">
								<p>
									Biopro Fuel Saver diiktiraf di seluruh dunia.
									Kriteria ini membantu pengedar dan ejen
									untuk mempromosikan produk kami dan
									dengan ini meningkatkan penjualan. Sistem
									rangkaian jualan ialah sistem yang senang dan
									berkesan yang boleh membantu pengedar
									dan ejen untuk mendapatkan keuntungan
									dan bonus jualan.

								</p>
								<p>
									<span class="m-text-background">JANJI KAMI</span>
									<br>
									Mudah sahaja! Kenapa tidak memulakan
									perniagaan anda sendiri dengan modal minima
									PENGENALAN PRODUK dan banyak peluang menarik menanti anda!
								</p>
								<a href="../home/contact" class="m-btn"> Hubungi Kami </a>
                                <a href="../home/registerPre" class="m-btn"> Daftar sekarang </a>
							</div>

							<div class="box heading l-grid-4 t-grid-12 third-box">
								<h4 class="h1 m-text-background"> Sertai Kami! </h4>
								<h4 class="m-text-background">Peluang Perniagaan</h4>
							</div>
						</div>
					</div>
				</section>
                
				<section>
					<div class=" m-site-section white-bg">

						<div class="container">

							<div class="box heading l-grid-6 t-grid-6">
								<h4 class="h1 m-text-background"> Pengedar </h4>
								<p>
									<br>
									Syarat untuk menjadi pengedar adalah membuat pembelian pertama seperti berikut:
								</p>
								<p>
									20 Kit Promosi : RM3600.00
									<br>
									20 Kotak BFS (50 botol/kotak) : RM5400.00
									<br>
									Jumlah modal pertama : RM9000.00
									<br>
									Kelebihan Pengedar:
									<br>
									<br>
									1) Untung Runcit 16-55%.
									<br>
									2) Komisen jualan dalam rangkaian.
									<br>
									3) Upah “dropship”.
									<br>
									4) Promosi dibantu oleh syarikat di peringkat
									nasional.
									<br>
                                    5)Setiap agen akan membeli daripada distributor kawasan terdekat.
                                    <br>
                                    6)Hasil keuntungan maksima pusingan pertama RM3550.00
								</p>
							</div>

							<div class="box heading l-grid-6 t-grid-6">
								<h4 class="h1 m-text-background"> Ejen </h4>
								<p>
									<br>
									Syarat untuk menjadi pengedar adalah membuat pembelian pertama seperti berikut:
								</p>
								<p>
                                    Beli satu kotak Biopro Fuel Saver (200ml x 50 botol) + 
                                    Yuran keahlian (kit promosi + sistem afilliate) bernilai <b>RM500.</b>
								</p>
                                <p>
                                    Rekrut ahli baru Biopro Fuel Saver, dengan kit promosi website. 
                                    Anda akan dapat komisen <b>RM150</b> untuk setiap ahli mendaftar dengan Referral Code anda.
                                </p>
                                <p>
                                    Setiap pembelian berulang rekrut level 1, anda akan memperoleh <b>RM40.</b>
                                </p>
                                <p>
                                    Untuk rekrut level 2 pula, setiap pembelian berulang anda akan memperoleh <b>RM20.</b>
                                </p>
							</div>
						</div>
					</div>
				</section>
                <section>
                    <div class=" m-site-section grey-bg" style="padding-bottom:10px;">
						<div class="container">
							<div class="box heading l-grid-4 t-grid-6">
								<h4 class="h1 m-text-background"> Pengedar Biopro Fuel Saver </h4>
							</div>
                            <div class="box l-grid-8">
                                <div class="l-grid-4">
                                    <img class="" src="assets/images/skudai-map.jpg" alt="" style="" />
                                    <h2 style="margin-bottom:0px; font-size:2em;">EN AZLI</h2>
                                    <h3 style="margin-bottom:0px;">Skudai, Johor</h3>
                                    <p style="font-style:italic;">+6019 772 2361</p>
                                </div>
                                <div class="l-grid-4">
                                    <img class="" src="assets/images/gelangpatah-map.jpg" alt="" style="" />
                                    <h2 style="margin-bottom:0px; font-size:2em;">EN ARIZAL</h2>
                                    <h3 style="margin-bottom:0px;">Gelang Patah, Johor</h3>
                                    <p style="font-style:italic;">+6012 795 5941</p>
                                </div>
                                <div class="l-grid-4 cta">
                                    <img class="" src="assets/images/kotatinggi-map.jpg" alt="" style="" />
                                    <h2 style="margin-bottom:0px; font-size:2em;">PN SITI </h2>
                                    <h3 style="margin-bottom:0px;">Kota Tinggi, Johor</h3>
                                    <p style="font-style:italic;">+6019 908 2352</p>
                                </div>
                            </div>
						</div>
					</div> 
                </section>
				<section id="statistik">
					<div class=" m-site-section green-bg">

						<div class="container">

							<div class="box image l-grid-4 t-grid-6">
								<img class="" src="assets/images/peugot.png"  alt="" />
								<h3>
								<br>
								Peugeot 408 Turbo 1500cc (2013)</h3>
								<p>
									<br>

								</p>
							</div>

							<div class="box cta l-grid-4 t-grid-6">
								<img class="svg-replace" src="assets/images/persona.png" alt="" />

								<h3 style="text-align: center;">
								<br>
								Persona 1.6 Auto (2011)</h3>

							</div>

							<div class="box heading l-grid-4 t-grid-12 third-box">
								<h4 class="h1 m-text-background"> Statistik Penjimatan </h4>
								<h4 class="m-text-background">Dapatkan brosur produk Biopro Fuel Saver untuk lebih maklumat.</h4>
								<a href="assets/files/brochure.png" class="m-btn" target="_blank"> Muat Turun </a>
							</div>
						</div>
					</div>
				</section>
                
                <!-- Testimoni section  -->
                <section class="m-slider2" style="margin-top:-21px;">
                    <ul class="slides" style="background-color:#4c4c4c;">
						<!---------- ADD CONTENTS TO THE SLIDER ---------->
						<li>
							<!---------- ADD IMAGE ---------->
							<div class="slide-image2" style="background-image:url('assets/images/testimoni/testimoni1.jpg');"></div>

							<div class="container">
								<div class="slide-caption2">

									<!---------- ADD OPTIONAL HEADING ---------->
									<h4 class="h1 heading">NISSAN VANNETE</h4>

									<!---------- ADD OPTIONAL SUB-HEADING ---------->
									<div class="m-text-surround">
										<p>
											Extra milleage 28% dengan 400ml Biopro Fuel Saver + 40 liter ron95.
										</p>
									</div>
								</div>
							</div>
						</li>
						<li>
							<!---------- ADD IMAGE ---------->
							<div class="slide-image2" style="background-image:url('assets/images/testimoni/testimoni2.jpg');"></div>

							<div class="container">
								<div class="slide-caption2">

									<!---------- ADD OPTIONAL HEADING ---------->
									<h4 class="h1 heading">TOYOTA VIOS</h4>

									<!---------- ADD OPTIONAL SUB-HEADING ---------->
									<div class="m-text-surround">
										<p>
											Extra milleage 30% selepas menggunakan 400ml Biopro Fuel Saver dengan 40 liter Ron95.
										</p>
									</div>
								</div>
							</div>
						</li>
						<li>
							<!---------- ADD IMAGE ---------->
							<div class="slide-image2" style="background-image:url('assets/images/testimoni/testimoni3.jpg');"></div>

							<div class="container">
								<div class="slide-caption2">

									<!---------- ADD OPTIONAL HEADING ---------->
									<h4 class="h1 heading">NAZA BLADE 650CC</h4>

									<!---------- ADD OPTIONAL SUB-HEADING ---------->
									<div class="m-text-surround">
										<p>
											Extra milleage 60% bila guna Biopro Fuel Saver.
										</p>
									</div>
								</div>
							</div>
						</li>
					</ul>
					<!---------- SLIDER CONTROLS ---------->
					<div class="m-slider-controls-container2">
						<div class="container">
							<div class="m-slider-controls2"></div>
						</div>
					</div>
                </section>

				<section class="m-info-section m-image-gallery white-bg">
					<div class="container">

						<h4 class="h1 m-text-background">Kelebihan Biopro Fuel Petrol Saver</h4>

						<div class="l-grid-3 t-grid-6 p-grid-12">

							<figure>
								<img class="svg-replace" src="assets/images/icons-01.png" data-svg-replacement="" alt="" />
								<figcaption>
									<p class="l-centered">
										Sesuai dengan semua kenderaan petrol standard.
									</p>
								</figcaption>
							</figure>

						</div>

						<div class="l-grid-3 t-grid-6 p-grid-12">
							<figure>
								<img class="svg-replace" src="assets/images/icons-02.png" data-svg-replacement="h" alt="" />
								<figcaption>
									<p class="l-centered">
										Penjimatan petrol.
									</p>
								</figcaption>
							</figure>

						</div>

						<div class="l-grid-3 t-grid-6 p-grid-12">

							<figure>
								<img class="svg-replace" src="assets/images/icons-03.png" data-svg-replacement="" alt="" />
								<figcaption>
									<p class="l-centered">
										Meningkatkan prestasi kenderaan.
									</p>
								</figcaption>
							</figure>

						</div>

						<div class="l-grid-3 t-grid-6 p-grid-12">

							<figure>
								<img class="svg-replace" src="assets/images/icons-04.png" data-svg-replacement="" alt="" />
								<figcaption>
									<p class="l-centered">
										Penggunaan nisbah yang rendah.
									</p>
								</figcaption>
							</figure>

						</div>

					</div>
				</section>
				<section id="mengenai">
					<div class=" m-site-section grey-bg">

						<div class="container">

							<div class="box heading l-grid-2 t-grid-6">
								<h4 class="h1 m-text-background"> Mengenai Kami </h4>
							</div>

							<!-- <div class="box image l-grid-4 t-grid-6">
							<img class="" src="assets/images/Homepage_5-.svg" data-svg-replacement="" alt="" />
							</div> -->

							<div class="box cta l-grid-9 t-grid-12 third-box">
								<div class="l-grid-3"><img class="" src="assets/images/biopro-circle.png" alt="" style="" />
								</div>
								<div class="l-grid-5" style="margin-bottom:30px;">
									<p>
										Biopro Petrol Global Marketing Sdn. Bhd. adalah syarikat pengeluar dan 
                                        pengedar eksklusif Biopro Fuel Saver di Malaysia dan Indonesia. 
                                        Syarikat ini berpangkalan di Semenyih, Selangor.
									</p>
                                    <p>No 1 jalan 2, Kawasan Perindustrian Lekas 18, <br>Semenyih 43500, Selangor
                                    <br/><br/>019 290 4438 (9 pagi - 6 petang, Isnin - Jumaat)
                                    <br/>WhatsApp / Line 
                                    <br/><br/><a href="https://www.facebook.com/biopropetrol.gm?ref=hl">Like kami di Facebook!</a></p>
								</div>

                                <!--
								<div class="l-grid-2"><img class="" src="assets/images/gyrus-circle.png" alt="" style="" />
								</div>
                                -->
								
								<!-- <a href="" class="m-btn"> About Us </a> -->
							</div>

						</div>
					</div>
				</section>
				<section class="light-grey-bg" style="padding:50px">
					<div class="container">
						<div class="l-grid-12">
							<h1 class="h1 m-text-background" style="text-align: center;">Pengiktirafan Seluruh Dunia</h1>
						</div>

						<div class="m-cta-inline l-centered">

							<img class="awards" src="assets/images/logo/1.png" alt="" />
							<img class="awards" src="assets/images/logo/2.png" alt="" />
							<img class="awards" src="assets/images/logo/3.png" alt="" />
							<img class="" src="assets/images/logo/4.png" alt="" width="95px;" />
							<img class="awards" src="assets/images/logo/5.png" alt="" />
							<img class="awards" src="assets/images/logo/6.png" alt="" />
							<img class="awards" src="assets/images/logo/7.png" alt="" />
							<img class="awards" src="assets/images/logo/8.png" alt="" />
							<img class="awards" src="assets/images/logo/9.png" alt="" />
						</div>
					</div>
				</section>

				<footer class="m-site-footer">

					<div class="container footer-nav">

						<div class="l-grid-2 l-gutter-expand t-grid-4 p-grid-12">
							<ul class="m-stacked-list">
								<li id="menu-item-50" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-50 list-item">
									<a href="../home/">Home</a>
								</li>
								<li id="menu-item-52" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-52 list-item">
									<a href="#biopro">Biopro</a>
								</li>
								<li id="menu-item-51" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-51 list-item">
									<a href="#statistik">Statistik</a>
								</li>
							</ul>
						</div>

						<div class="l-grid-3 l-gutter-expand t-grid-5 p-grid-12">
							<ul class="l-grid-7 l-gutter-expand-left t-grid-8 t-gutter-expand-left p-grid-12 m-stacked-list">
<!--
								<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-59 list-item">
									<a href="#inventor">Inventor</a>
								</li>
-->
								<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-55 list-item">
									<a href="../home/register">Daftar Ahli</a>
								</li>
							</ul>
						</div>

						<div class="l-grid-2 l-gutter-expand t-grid-3 p-grid-12">
							<ul class="m-stacked-list">
								<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-57 list-item">
									<a href="#mengenai">Mengenai Kami</a>
								</li>
								<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-56 list-item">
									<a href="../home/contact">Hubungi Kami</a>
								</li>
							</ul>
						</div>

						<!--
						<div class="l-grid-3 t-grid-12 p-grid-12 l-right">
						<ul class="m-inline-list social-links">

						<li class="m-social-link list-item">
						<a href="http://www.linkedin.com/company/the-carbon-tree" target="_blank">
						<i class="fa fa-linkedin">
						<img class="social-icon" src="http://www.thecarbontree.com/wp-content/themes/carbontree/public/images/linkedin-logo.png" />
						</i>
						</a>
						</li>
						<li class="m-social-link list-item">
						<a href="http://twitter.com/TheCarbonTree" target="_blank">
						<i class="fa fa-twitter">
						<img class="social-icon" src="http://www.thecarbontree.com/wp-content/themes/carbontree/public/images/twitter-logo.png" />
						</i>
						</a>
						</li>

						</ul>
						</div>
						-->

					</div>

					<div class="container copyright-credits">

						<p class="logo l-right">
							<a href="#"><img class="svg-replace" src="assets/images/footer-logo.png" alt="" data-svg-replacement=""/></a>
						</p>
						<p class="l-left">
							<span class="copyright">&copy; 2014 Biopro Global Marketing Sdn Bhd</span>
							<!--			<span class="credits">Site by <a href="" target="_blank">Wired In</a></span>-->
						</p>
					</div>
				</footer>
			</div>

			<script type='text/javascript' src='assets/js/jquery.form.min.js'></script>
			<!--
			<script type='text/javascript'>
			/* <![CDATA[ */
			var _wpcf7 = {"loaderUrl":"http:\/\/www.thecarbontree.com\/wp-content\/plugins\/contact-form-7\/images\/ajax-loader.gif","sending":"Sending ..."};
			/* ]]> */
			</script>
			-->
			<script type='text/javascript' src='assets/js/scripts2.js'></script>
			<script type='text/javascript' src='assets/js/script.js'></script>
			<script type='text/javascript' src='assets/js/modernizr.min.js'></script>
			<script type='text/javascript' src='assets/js/jquery.flexslider-2.2.0.min.js'></script>
			<script type='text/javascript' src='assets/js/waypoints.min.js'></script>
			<script type='text/javascript' src='assets/js/jquery.event.move.js'></script>
			<script type='text/javascript' src='assets/js/jquery.twentytwenty.js'></script>
			<script type='text/javascript' src='assets/js/jquery.magnific-popup.min.js'></script>
			<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
			<script type="text/javascript" src="assets/js/plusanchor/jquery.easing.1.3.js"></script>
			<script type="text/javascript" src="assets/js/plusanchor/jquery.plusanchor.js"></script>

			<script>
$(document).ready(function(){
$('body').plusAnchor({
easing: 'easeInOutExpo',
speed:  1000
});
});
			</script>
			
		</body>
</html>