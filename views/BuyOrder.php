<!DOCTYPE html>
<html>

	<head>
		<?=$fixheader; ?>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<title>Biopro Fuel Saver - Sign Up!</title>
		<link rel="shortcut icon" href="assets/images/biopro-box.png" />

		<!--main css load code -->

		<!-- Core CSS - Include with every page -->
		<link href="assets2/css/bootstrap.css" rel="stylesheet">
		<link href="assets2/font-awesome/css/font-awesome.css" rel="stylesheet">

		<!-- Page-Level Plugin CSS - Dashboard -->
		<link href="assets2/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">
		<link href="assets2/css/plugins/timeline/timeline.css" rel="stylesheet">
		<link href="assets2/css/plugins/social-buttons/social-buttons.css" rel="stylesheet">
		<link href="assets2/js/plugins/datetimepicker/jquery.datetimepicker.css" rel="stylesheet">

		<!-- SB Admin CSS - Include with every page -->
		<link href="assets2/css/sb-admin.css" rel="stylesheet">
		<link href="assets2/css/main.css" rel="stylesheet">
		<link href="assets2/css/nazar.css" rel="stylesheet">

	</head>

	<body style="background-color:white;">
		<div id="wrapper">
			<!-- Navbar Top code -->
			<nav class="navbar navbar-default navbar-static-top navbar-green" role="navigation" style="margin-bottom: 0">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="../">Biopro - Penjimatan Terbaik!</a>
				</div>
				<!-- /.navbar-header -->
			</nav>
			<!-- /.navbar-static-top -->
			<div class="container">

				<br/>
				<a href="../home/buy" class="btn btn-primary"> <i class="fa fa-arrow-left"> Back</i></a>
				<br/>
				<br/>
				<div class="row">

					<div class="col-lg-7">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title">
								<button class="btn btn-outline btn-circle btn-success">
									1
								</button> Bank-in the payment</h3>
							</div>
							<div class="panel-body">
								<p>
									Now you have to visit your preffered bank and bank in the specified amount: RM <span id="update2"><?php
									if (isset($_GET['order'])) {
										$order = $_GET['order'];
										if ($order == 1) {
											echo '15.00';
										} else if ($order == 2) {
											echo '22.00';
										} else if ($order == 4) {
											echo '36.00';
										} else if ($order == 8) {
											echo '66.00';
										} else if ($order == 15) {
											echo '100.00';
										} else {
											echo '0.00';
										}
									} else {
										echo '0.00';
									}
								?>
									</span> + Shipping fee (RM <span id="shipfee2"><?php
									if (isset($_GET['order'])) {
										$order = $_GET['order'];
										if ($order == 1) {
											echo '8.00';
										} else if ($order == 2) {
											echo '8.00';
										} else if ($order == 4) {
											echo '8.00';
										} else if ($order == 8) {
											echo '10.00';
										} else if ($order == 15) {
											echo '10.00';
										} else {
											echo '0.00';
										}
									} else {
										echo '0.00';
									}
											?></span> for Peninsular Malaysia). <span class="text-info">REMEMBER to save the transaction slip (bank-in slip) or receipt</span>. For user who pay from online banking, you can use screen shot.
									Please bank in this bank account:
								</p>
								<div class="row">
									<div class="col-lg-6">
										<div class="panel panel-default">
											<div class="panel-heading">
												<h3 class="panel-title">CIMB</h3>
											</div>
											<div class="panel-body">
												Account No: <b>8006947073</b>
												<br />
												<hr />
												Holder Name: <b>Biopro Petrol Global Marketing Sdn. Bhd</b>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
				<form id="submitForm" method="post" action="../home/buyPayment" enctype="multipart/form-data" name="ab">
					<div class="row">
						<div class="col-lg-7">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">
									<button class="btn btn-outline btn-circle btn-success">
										2
									</button> Upload transaction slip or receipt</h3>
								</div>
								<div class="panel-body">
									<div class="form">
										<p>
											The last step is easy, you need to upload the transaction slip (bank-in slip) or receipt below.
											We will process your payment and notify you shortly.
										</p>
										<div class="form-group">
											<label>Select Payment made from</label>
											<select name="fromBank" class="form-control"  style="width:420px;">
												<option value="Maybank">Maybank</option>
												<option value="CIMB">CIMB</option>
												<option value="Hong Leong">Hong Leong</option>
												<option value="Bank Islam">Bank Islam</option>
												<option value="Other">Other Bank</option>
											</select>
										</div>
										<div class="form-group">
											<label>If made payment from 'Other Bank' please state here:</label>
											<input name="other" type="text" class="form-control" style="width:420px;" placeholder="Other Bank">
										</div>
										<div class="form-group">
											<label>Select Payment made to</label>
											<select name="toBank" class="form-control"  style="width:420px;">
												<option value="CIMB">CIMB - 8006947073 - Biopro Petrol Global Marketing Sdn. Bhd</option>
											</select>
										</div>
										<div class="form-group">
											<label>Verify amount of payment</label>
											<div class="input-group" style="width:420px;">
												<span class="input-group-addon">RM</span>
												<input name="payment" type="text" class="form-control" placeholder="Amount of Payment" required="required" value="">
											</div>
										</div>
										<div class="form-group">
											<label>Date of Payment</label>
											<!-- <br /> -->
											<!-- <small>This is important because sometimes we cannot see the picture clearly</small> -->
											<!-- <br /> -->
											<input id="datetimepicker" name="dateInSlip" type="text" class="form-control" style="width:420px;" placeholder="Date" required="required">
										</div>
										<div class="form-group">
											<label>Time of Payment</label>
											<input id="datetimepicker2" name="timeInSlip" type="text" class="form-control" style="width:420px;" placeholder="Time" required="required">
											<small>You can put minutes by changing manually 2 number at the end	, example: 14:12</small>
										</div>
										<div class="form-group">
											<label>Upload your receipt or transaction slip</label>
											<input id="imgInp" name="imageupload" type="file" required="required"/>
											<small>You also can upload print screen</small>
											<br />
											<img id="blah" src="holder.js/200x120" alt="Picture Preview" class="img-thumbnail"/>
										</div>
										<div class="form-group form-inline">
											<!-- <label>Specify the stock quantity</label>
											<p>
											Please enter the amount of stock you purchase.
											</p> -->
											<!-- <input id="total2" name="amount" type="number" placeholder="Enter amount.." min="1" max="<?=$data['currentBioProStock']; ?>" class="form-control" style="width:420px;" required="required"/>
											<span style="font-size:20px;"><i class="fa fa-dropbox"></i></span>
											<label for="total2" class="error"></label> -->
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-lg-7">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">
									<button class="btn btn-outline btn-circle btn-success">
										3
									</button> Fill Up Personal Information</h3>
								</div>
								<div class="panel-body">
									<div>

										<div class="form-group">
											<p>
												Please fill up all the fields, this will help us ship Biopro Fuel Saver to you.
											</p>
											<label>Name</label>
											<input class="form-control" style="width:420px;" placeholder="" name="fullname" required="required"/>
										</div>
										<div class="form-group">
											<label>Contact Number</label>
											<input class="form-control" style="width:420px;" placeholder="" name="phone" required="required"/>
										</div>
										<div class="form-group">
											<label>Email</label>
											<input class="form-control" style="width:420px;" placeholder="" name="email" required="required"/>
										</div>
										<div class="form-group address-field">
											<label>Address</label>
											<br/>
											<span class="label label-success">We will mail your order to this address.</span>
											<br/>
											<input class="form-control" style="width:420px;" placeholder="Address 1" name="add1" required="required"/>
											<input class="form-control" style="width:420px;" placeholder="Address 2" name="add2" />
											<input class="form-control" style="width:420px;" placeholder="Postcode" name="postcode" required="required"/>
											<input class="form-control" style="width:420px;" placeholder="City" name="city" required="required"/>
											<input class="form-control" style="width:420px;" placeholder="State" name="state" required="required"/>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-7">
							<div class="panel panel-default">

								<div class="panel-body">
									<button class="btn btn-block btn-primary">
										Submit <i class="fa fa-check"></i>
									</button>
								</div>
							</div>
						</div>
					</div>

					<div class="col-lg-4" style="position:fixed; right:100px; top:128px; z-index:101;">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title">
								<button class="btn btn-outline btn-circle btn-success">
									<i class="fa fa-dropbox"></i>
								</button> Order Information</h3>
							</div>
							<div class="panel-body">
								<div class="col-lg-12">
									<div class="form-group">
										<label>Quantity</label>
										<select id="selectBox" class="form-control" onchange="calculateNew()" name="package">
											<option id="b1" value="7.00" <?=($_GET['order']==1)?'Selected':''?>>1 Bottle Biopro Fuel Saver (RM 7.00)</option>
											<option id="b4" value="28.00" <?=($_GET['order']==4)?'Selected':''?>>4 Bottles Biopro Fuel Saver (RM 28.00)</option>
											<option id="b8" value="56.00" <?=($_GET['order']==8)?'Selected':''?>>8 Bottles Biopro Fuel Saver (RM 56.00)</option>
											<option id="b2" value="14.00" <?=($_GET['order']==2)?'Selected':''?>>Trial Package (2 Bottles RM 14.00)</option>
											<option id="b15" value="90.00" <?=($_GET['order'] == 15) ? 'Selected' : '' ?>>Silver Package (15 Bottles RM 90.00)</option>
											<!-- <option>Agent Package + Member (50 Bottles RM400)</option> -->
										</select>
										<br />
										<p>
											Shipping fee: <!-- RM8 Peninsular Malaysia, RM12 Sabah/Sarawak -->
											<div class="radio">
												<label>
													<input id="ship1" type="radio" name="shipfee"  value="Peninsular Malaysia RM8" checked="checked" required="required"/>
													RM <span id="shipfee"><?php
													if (isset($_GET['order'])) {
														$order = $_GET['order'];
														if ($order == 1) {
															echo '8.00';
														} else if ($order == 2) {
															echo '8.00';
														} else if ($order == 4) {
															echo '8.00';
														} else if ($order == 8) {
															echo '10.00';
														} else if ($order == 15) {
															echo '10.00';
														} else {
															echo '0.00';
														}
													} else {
														echo '0.00';
													}
											?></span> - Peninsular Malaysia </label><!-- onclick="ship('semenanjung')" -->
											</div>
											<!-- <div class="radio">
											<label>
											<input id="ship2" type="radio" name="shipfee" onclick="ship('sabah')" value="Sabah/Sarawak - RM12" required="required"/> RM<span>12</span> - Sabah/Sarawak
											</label>
											</div> -->
										</p>
										<label for="shipfee" class="error"></label>
									</div>
									<!--
									<div class="form-group">
									<label>Product Type</label>
									<select class="form-control">
									<option>Small car (1.0-1.3 Litre)</option>
									<option>Sedan (1.3-2.0 Litre)</option>
									<option>Motorcycle (1.0-1.0 Litre)</option>
									</select>
									</div>
									-->
									<div class="form-group">

										<h1><small>TOTAL:</small> RM <span id="update"> <?php
										if (isset($_GET['order'])) {
											$order = $_GET['order'];
											if ($order == 1) {
												echo '15.00';
											} else if ($order == 2) {
												echo '22.00';
											} else if ($order == 4) {
												echo '36.00';
											} else if ($order == 8) {
												echo '66.00';
											} else if ($order == 15) {
												echo '100.00';
											} else {
												echo '0.00';
											}
										} else {
											echo '0.00';
										}
											?> </span></h1>
									</div>
								</div>
							</div>
						</div>
					</div>

				</form>
				<!--form-->

			</div>

		</div>
		<!-- /#wrapper -->
		<script>
			var sabahfee = 0;
			var mlfee = 0;
		</script>

		<!-- Core Scripts - Include with every page -->
		<script src="assets2/js/jquery-1.10.2.js"></script>
		<script src="assets2/js/bootstrap.min.js"></script>
		<script src="assets2/js/plugins/metisMenu/jquery.metisMenu.js"></script>

		<!-- Page-Level Plugin Scripts - Dashboard -->
		<script src="assets2/js/plugins/morris/raphael-2.1.0.min.js"></script>
		<script src="assets2/js/plugins/morris/morris.js"></script>

		<!-- SB Admin Scripts - Include with every page -->
		<script src="assets2/js/sb-admin.js"></script>

		<!-- Page-Level Demo Scripts - Dashboard - Use for reference -->
		<script src="assets2/js/demo/dashboard-demo.js"></script>

		<!--Validation-->
		<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.js"></script>

		<!--  Holder.js    -->
		<script src="assets2/js/holder.js"></script>

		<script>
			function calculateNew() {
				var selectBox = document.getElementById("selectBox");
				var selectedValue = selectBox.options[selectBox.selectedIndex].value;
				// alert(selectedValue);

				// document.ab.payment.value = selectedValue;
				// $('#ship1').attr('checked', false);
				// $('#ship2').attr('checked', false);

				var shafwan = selectBox.options[selectBox.selectedIndex].id;

				if (shafwan == 'b1') {
					mlfee = 8;
				} else if (shafwan == 'b2') {
					mlfee = 8;
				} else if (shafwan == 'b4') {
					mlfee = 8;
				} else if (shafwan == 'b8') {
					mlfee = 10;
				} else if (shafwan == 'b15') {
					mlfee = 10;
				}

				selectedValue = eval(selectedValue) + mlfee;

				document.getElementById('shipfee').innerHTML = mlfee.toFixed(2);
				document.getElementById('shipfee2').innerHTML = mlfee.toFixed(2);

				document.getElementById('update').innerHTML = selectedValue.toFixed(2);
				document.getElementById('update2').innerHTML = selectedValue.toFixed(2);
			}
		</script>

		<!--Validation-->
		<script src="assets2/js/jquery.validate.js"></script>
		<script>
			$("#submitForm").validate();
		</script>

		<script>
			$("#imgInp").change(function() {
				readURL(this);
			});

			function readURL(input) {

				if (input.files && input.files[0]) {
					var reader = new FileReader();

					reader.onload = function(e) {
						$('#blah').attr('src', e.target.result);
					}

					reader.readAsDataURL(input.files[0]);
				}
			}
		</script>

		<!--  datetimepicker.js    -->
		<script src="assets2/js/plugins/datetimepicker/jquery.datetimepicker.js"></script>
		<script>
			jQuery('#datetimepicker').datetimepicker({
				timepicker : false,
				format : 'd.m.Y'
			});
			jQuery('#datetimepicker2').datetimepicker({
				datepicker : false,
				format : 'H:i'
			});
		</script>

		<script>
			function ship(location) {
				var selectBox = document.getElementById("selectBox");
				var selectedValue = selectBox.options[selectBox.selectedIndex].value;
				var totalIncShip;
				if (location == 'semenanjung') {
					totalIncShip = eval(selectedValue) + 8;
				} else if (location == 'sabah') {
					totalIncShip = eval(selectedValue) + 12;
				}
				document.getElementById('update').innerHTML = totalIncShip.toFixed(2);
			}
		</script>

	</body>

</html>
