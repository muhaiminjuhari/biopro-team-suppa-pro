new Morris.Line({
  // ID of the element in which to draw the chart.
  element: 'salesgraph',
  // Chart data records -- each entry in this array corresponds to a point on
  // the chart.
    
      data: [
        { day: '2014-01-01', value: 200 },
        { day: '2014-01-02', value: 101 },
        { day: '2014-01-03', value: 50 },
        { day: '2014-01-04', value: 80 },
        { day: '2014-01-05', value: 120 },
        { day: '2014-01-06', value: 130 },
        { day: '2014-01-07', value: 200 },
        { day: '2014-01-08', value: 190 },
        { day: '2014-01-09', value: 190 },
        { day: '2014-01-10', value: 280 },
        { day: '2014-01-11', value: 300 },
        { day: '2014-01-12', value: 110 },
        { day: '2014-01-13', value: 190 },
        { day: '2014-01-14', value: 150 },
        { day: '2014-01-15', value: 140 },
        { day: '2014-01-16', value: 110 }
      ],
    // The name of the data record attribute that contains x-values.
    xkey: 'day',
    // A list of names of data record attributes that contain y-values.
    ykeys: ['value'],
    // Labels for the ykeys -- will be displayed when you hover over the
    // chart.
    labels: ['RM'],
});