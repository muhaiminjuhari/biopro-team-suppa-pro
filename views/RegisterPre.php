<!DOCTYPE html>
<html lang="en">

	<head>
		<?=$fixheader; ?>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<title>Biopro Fuel Saver - Sign Up!</title>
		<link rel="shortcut icon" href="assets/images/biopro-box.png" />
		
		<!-- meta -->
		<meta name="description" content="Join us now, and enjoy exclusive benefits from Biopro Fuel Saver!">
		
		<!--fb-->
		<meta property="og:title" content="Biopro Fuel Saver - Sign Up!" />
		<meta property="og:type" content="article" />
		<meta property="og:url" content=" http://biopropetrol.com/" />
		<meta property="og:image" content="http://biopropetrol.com/views/assets/images/biopro-bottle.png" />
		<meta property="og:description" content="Join us now, and enjoy exclusive benefits from Biopro Fuel Saver!" />
		
		<!--twitter-->
		<meta name="twitter:card" content="summary">
		<meta name="twitter:title" content="Biopro Fuel Saver - Sign Up!">	
		<meta name="twitter:description" content="Join us now, and enjoy exclusive benefits from Biopro Fuel Saver!">
		<meta name="twitter:image" content="http://biopropetrol.com/views/assets/images/biopro-bottle.png">

		<!--main css load code -->

		<!-- Core CSS - Include with every page -->
		<link href="assets2/css/bootstrap.css" rel="stylesheet">
		<link href="assets2/font-awesome/css/font-awesome.css" rel="stylesheet">

		<!-- Page-Level Plugin CSS - Dashboard -->
		<link href="assets2/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">
		<link href="assets2/css/plugins/timeline/timeline.css" rel="stylesheet">
		<link href="assets2/css/plugins/social-buttons/social-buttons.css" rel="stylesheet">

		<link href="assets2/js/plugins/datetimepicker/jquery.datetimepicker.css" rel="stylesheet">

		<!-- SB Admin CSS - Include with every page -->
		<link href="assets2/css/sb-admin.css" rel="stylesheet">
		<link href="assets2/css/main.css" rel="stylesheet">

		<link href="assets2/css/nazar.css" rel="stylesheet">

	</head>

	<body class="background1">

		<div id="wrapper">

			<!-- Navbar Top code -->
			<nav class="navbar navbar-default navbar-static-top navbar-green" role="navigation" style="margin-bottom: 0">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="../">Biopro - Penjimatan Terbaik!</a>
				</div>
				<!-- /.navbar-header -->

			</nav>
			<!-- /.navbar-static-top -->

			<div class="container">
				<div class="row">
					<div class="col-lg-8 col-lg-offset-2" align="center">
						<div class="registerPre-img"></div>
					</div>
				</div>
				<div class="row">

					<div class="row">
						<div class="col-lg-8 col-lg-offset-2">
							<div class="panel panel-success">
								<div class="panel-heading">
									<h3 class="panel-title">Become Biopro Fuel Saver!</h3>
								</div>
								<div class="panel-body">
									<fieldset>
										<?php
										if(isset($_GET['refer'])){
											if($data['refer']!=NULL){
										?>
                                        <div class="form-group">
                                            <div class="col-lg-12">
                                                <p>Referral Code: <?=$_GET['refer'] ;?></p>
                                                <p>Upline Agent name: <?=$data['refer']['fname']. ' '. $data['refer']['lname'];?></p>
                                            </div>
                                        </div>
                                        <?php
                                        	}
										}
                                        ?>
										<div class="form-group">
											<div class="col-lg-12">
												<h2 class="bg-success" style="padding:10px; color:black; border-bottom:5px solid #E7E7E7; background-color:#FFE200">Join us now, and enjoy exclusive benefits from Biopro Fuel Saver!</h2>
											</div>
											<br/>
											<br/>
											<div class="col-lg-4">
												<img src="../views/assets/images/icon-01.png" class="img-responsive" />
												<br/>
												<p>
													1 Box of Biopro Fuel Saver (that's 50 bottles!)
												</p>
											</div>

											<div class="col-lg-4">
												<img src="../views/assets/images/icon-02.png" class="img-responsive" />
												<br/>
												<p>
													Access to affiliate Biopro System
												</p>
											</div>

											<div class="col-lg-4">
												<img src="../views/assets/images/icon-03.png" class="img-responsive" />
												<br/>
												<p>
													Exclusive Promotional Kit!
												</p>
											</div>

											<div class="col-lg-12">
												<hr style="border-bottom:3px solid #EEEEEE" />
												<h2><small>Total membership <span style="text-decoration:line-through">RM660</span></small><b>RM520!</b></h2>
												<p style="font-style:italic;">
													For a limited time only!
												</p>
											</div>
										</div>
									</fieldset>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">

				</div>
				<div class="row">
					<div class="row">
						<div class="col-lg-8 col-lg-offset-2">
							<div class="panel panel-success">
								<div class="panel-body">
									<fieldset>
										<div class="form-group">
											<p>
												Please bank-in the total membership amount (RM500) to:
											</p>
										</div>
										<div class="form-group">
											<div class="panel panel-default">
												<div class="panel-heading">
													<h3 class="panel-title">Bank</h3>
												</div>
												<div class="panel-body">
													CIMB Bank
													Account No: 8006947073
													<hr/>
													Holder Name: Biopro Petrol Global Marketing Sdn. Bhd
												</div>
											</div>
										</div>
										<div class="form-group">
											<p>
												<span class="alert alert-success"><b>Keep in mind!</b> Please keep the transaction receipt or print screen image of the online transaction.</span>
											</p>
										</div>
									</fieldset>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<form id="submitForm" method="post" action="../home/register<?php
										if(isset($_GET['refer'])){
											if($data['refer']!=NULL){
												echo '?refer='. $_GET['refer'];
											}
										}
										?>" enctype="multipart/form-data">
						<div class="row">
							<div class="col-lg-8 col-lg-offset-2">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h3 class="panel-title"> Upload transaction slip or receipt</h3>
									</div>
									<div class="panel-body">
										<div class="form">
											<p>
												The last step is easy, you need to upload the transaction slip (bank-in slip) or receipt below.
												We will process your payment and notify you shortly.
											</p>
											<div class="form-group">
												<label>Select Payment made from</label>
												<select name="fromBank" class="form-control"  style="width:420px;">
													<option value="Maybank">Maybank</option>
													<option value="CIMB">CIMB</option>
													<option value="Hong Leong">Hong Leong</option>
													<option value="Bank Islam">Bank Islam</option>
													<option value="Other">Other Bank</option>
												</select>
											</div>
											<div class="form-group">
												<label>If made payment from 'Other Bank' please state here:</label>
												<input name="other" type="text" class="form-control" style="width:420px;" placeholder="Other Bank">
											</div>
											<div class="form-group">
												<label>Select Payment made to</label>
												<select name="toBank" class="form-control"  style="width:420px;">
													<!-- <option value="Maybank">Maybank - 716523812893793 - Biopro Petrol Global Marketing Sdn. Bhd</option> -->
													<option value="CIMB">CIMB - 8006947073 - Biopro Petrol Global Marketing Sdn. Bhd</option>
													<!-- <option value="Hong Leong">Hong Leong - 716523812893793 - Biopro Petrol Global Marketing Sdn. Bhd</option>
													<option value="Other">Bank Islam - 716523812893793 - Biopro Petrol Global Marketing Sdn. Bhd</option> -->
												</select>
											</div>
											<div class="form-group">
												<label>Verify amount of Payment</label>
												<div class="input-group" style="width:420px;">
													<span class="input-group-addon">RM</span>
													<input name="payment" type="text" class="form-control" placeholder="520" value="520" required="required">
												</div>
												<label for="payment" class="error"></label>
											</div>
											<div class="form-group">
												<label>Date of Payment</label>
												<!-- <br /> -->
												<!-- <small>This is important because sometimes we cannot see the picture clearly</small> -->
												<!-- <br /> -->
												<input id="datetimepicker" name="dateInSlip" type="text" class="form-control" style="width:420px;" placeholder="Date" required="required">
											</div>
											<div class="form-group">
												<label>Time of Payment</label>
												<input id="datetimepicker2" name="timeInSlip" type="text" class="form-control" style="width:420px;" placeholder="Time" required="required">
												<small>You can put minutes by changing manually 2 number at the end	, example: 14:12</small>
											</div>
											<div class="form-group">
												<label>Upload your receipt or transaction slip</label>
												<input id="imgInp" name="imageupload" type="file" required="required"/>
												<small>You also can upload print screen</small>
												<br />
												<img id="blah" src="holder.js/200x120" alt="Picture Preview" class="img-thumbnail"/>
											</div>
                                            <div class="form-group">
                                                <div class="bg-success">
                                                    <h3 style="padding:10px; color:black; border-bottom:5px solid #E7E7E7; background-color:#FFE200">After completing this form, you will need to register your details, click proceed to continue.</h3>
                                                </div>
                                            </div>
										</div>
									</div>
									<div class="panel-footer">
										<button class="btn btn-success btn-block">
											Proceed <i class="fa fa-chevron-right"></i>
										</button>
									</div>
								</div>
							</div>
						</div>

					</form>

				</div>
			</div>

			<!-- Modal -->
			<div class="modal fade" id="tnc-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
								&times;
							</button>
							<h4 class="modal-title" id="myModalLabel">Terms & Conditions</h4>
						</div>
						<div class="modal-body">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">
								Close
							</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->

		</div>
		<!-- /#wrapper -->

		<!-- Core Scripts - Include with every page -->
		<script src="assets2/js/jquery-1.10.2.js"></script>
		<script src="assets2/js/bootstrap.min.js"></script>
		<script src="assets2/js/plugins/metisMenu/jquery.metisMenu.js"></script>

		<!-- Page-Level Plugin Scripts - Dashboard -->
		<script src="assets2/js/plugins/morris/raphael-2.1.0.min.js"></script>
		<script src="assets2/js/plugins/morris/morris.js"></script>

		<!-- SB Admin Scripts - Include with every page -->
		<script src="assets2/js/sb-admin.js"></script>

		<!-- Page-Level Demo Scripts - Dashboard - Use for reference -->
		<script src="assets2/js/demo/dashboard-demo.js"></script>

		<!-- Holder.js     -->
		<script src="assets2/js/holder.js"></script>

		<!--Validation-->
		<script src="assets2/js/jquery.validate.js"></script>
		<script>
			$("#submitForm").validate();
		</script>

		<script>
			$("#imgInp").change(function() {
				readURL(this);
			});

			function readURL(input) {

				if (input.files && input.files[0]) {
					var reader = new FileReader();

					reader.onload = function(e) {
						$('#blah').attr('src', e.target.result);
					}

					reader.readAsDataURL(input.files[0]);
				}
			}
		</script>

		<!--  datetimepicker.js    -->
		<script src="assets2/js/plugins/datetimepicker/jquery.datetimepicker.js"></script>
		<script>
			jQuery('#datetimepicker').datetimepicker({
				timepicker : false,
				format : 'd.m.Y'
			});
			jQuery('#datetimepicker2').datetimepicker({
				datepicker : false,
				format : 'H:i'
			});
		</script>

		<?php
		if(isset($_GET['register'])){
		?>
		<script type="text/javascript" src="assets2/js/bootbox.js"></script>
		<script>
			bootbox.alert("Thank you for registering with Biopro Fuel Saver. You are just one step from becoming Biopro Agent! Please wait for us to confirm your payment, we will notify you shortly. Have a nice day!");
		</script>
		<?php
		}
		?>

	</body>

</html>
