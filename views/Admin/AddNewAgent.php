<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">User Management<small> <i class="fa fa-group"></i></small></h1>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="row">
            <div class="col-lg-12">
                <a href="../admin/addNewAgent" class="btn btn-outline btn-success">Add New Agent</a>
                <a href="../admin/user" class="btn btn-outline btn-success">User List</a>
            </div>
        </div>
        <br>
        <form id="submitForm4" action="../admin/registerUser" method="post">
            <div class="row">
                <div class="col-lg-6">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">Registration Form</h3>
                        </div>
                        <div class="panel-body">
                            <fieldset>
                                <div class="form-group">
                                    <input id="username" class="form-control" placeholder="Username" minlength="7" name="username" type="text" required="required" autofocus>
                                </div>
                                <div class="form-group">
                                    <input id="password" class="form-control" placeholder="Password" minlength="7" name="password" type="password" value="" required="required">
                                </div>
                                <div class="form-group">
                                    <input id="repassword" class="form-control" placeholder="Retype password" name="repassword" type="password" value="" required="required">
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">Personal Information</h3>
                        </div>
                        <div class="panel-body">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="First Name" name="fname" type="text" required="required">
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Last Name" name="lname" type="text" required="required">
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="E-mail" name="email" type="email" required="required">
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Contact Number" name="contactNo" type="text" required="required">
                                </div>
                                <div class="form-group">
                                    <label>Gender</label>
                                    <label class="radio-inline">
                                        <input type="radio" name="gender" id="optionsRadiosInline1" value="male" checked>Male</label>
                                    <label class="radio-inline">
                                        <input type="radio" name="gender" id="optionsRadiosInline2" value="female">Female</label>
                                </div>
                                <div class="form-group">
                                    <label>Are you Malaysian?</label>
                                    <label class="radio-inline">
                                        <input type="radio" name="local" id="optionsRadiosInline1" value="1" checked>Yes</label>
                                    <label class="radio-inline">
                                        <input type="radio" name="local" id="optionsRadiosInline2" value="0">No</label>
                                </div>
                                <div class="form-group">
                                    <label>IC Number/Passport Number</label>
                                    <input class="form-control" placeholder="" name="icno" type="text" required="required">
                                </div>
                                <div class="form-group">
                                    <label>Address</label>
                                    <input class="form-control" placeholder="Address 1" name="add1" type="text" required="required">
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Address 2" name="add2" type="text">
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="City/Town" name="city" type="text" required="required">
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Postcode" name="postcode" type="text" required="required">
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="State" name="state" type="text" required="required">
                                </div>
                                <div class="form-group">
                                    <select class="form-control" name="country" required="required">
                                        <option value="">Country</option>
                                        <option value="MALAYSIA">Malaysia</option>
                                        <option value="INDONESIA">Indonesia</option>
                                        <option value="THAILAND">Thailand</option>
                                        <option value="SINGAPORE">Singapore</option>
                                        <option value="BRUNEI">Brunei</option>
                                    </select>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">Banking Information</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label>Bank</label>
                                <select class="form-control">
                                    <option>Maybank</option>
                                    <option>CIMB</option>
                                    <option>Hong Leong</option>
                                    <option>BSN</option>
                                    <option>Bank Rakyat</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Bank Account Number</label>
                                <input class="form-control" type="text" placeholder="" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <button class="btn btn-success btn-block">
                                Sign Up
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- /.row -->