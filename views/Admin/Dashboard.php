<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Welcome! <small>Administrator</small></h1>
	</div>
</div>
<div class="row">
	<div class="col-lg-12">
		<div class="well">
			<h1>Dashboard</h1>
			<p>
				This is your dashboard, here you can monitor the latest stats in Biopro System. 
			</p>
		</div>
	</div>
</div>

<!-- graph -->
<!-- <div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">Sales</div>
            <div class="panel-body">
                <div id="salesgraph"></div>
            </div>
        </div>
    </div>
</div> -->

<div class="row">
    <div class="col-lg-12">
        <div class="col-lg-6">
            <div class="row">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Stocks
                    </div>
                    <div class="panel-body">
                        <div class="well">
                            <p>Current Stock</p>
                            <h1><?=$data['currentStock']; ?> <small><i class="fa fa-dropbox"></i> Boxes</small></h1>
                        </div>
                        <div class="well">
                            <p>Average daily stock purchased </p>
                            <h1><?=round($data['stockPerDay']); ?> <small><i class="fa fa-dropbox"></i> Boxes</small></h1>
                        </div>
                        <div class="well">
                            <p>Yesterday stock sold </p>
                            <h1><?=($data['yesterdaySell']==NULL?'0':$data['yesterdaySell']); ?> <small><i class="fa fa-dropbox"></i> Boxes</small></h1>
                        </div>
                        <div class="well">
                            <p>Last month stock sold </p>
                            <h1><?=$data['lastMonthSell']; ?> <small><i class="fa fa-dropbox"></i> Boxes</small></h1>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <a href="../admin/stock" class="btn btn-primary btn-outline">Manage Stock <i class="fa fa-arrow-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Commission
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="well col-lg-6">
                                <p>This month</p>
                                <hr/>
                                <h1><small>RM</small> 40.00</h1>
                            </div>
                            <div class="col-lg-6">
                                <p><span class="label label-primary">Paid date: 15/3/2014</span></p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="well col-lg-6">
                                <p>Last month</p>
                                <hr/>
                                <h1><small>RM</small> 330.00</h1>
                            </div>
                            <div class="col-lg-6">
                                <p><span class="label label-primary">Paid date: 15/3/2014</span></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <a href="../admin/commission" class="btn btn-primary btn-outline">Manage Commission <i class="fa fa-arrow-right"></i></a>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    Agents
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="well col-lg-6">
                                <p>Agent registered this month</p>
                                <hr/>
                                <h1>100<small> Agents</small></h1>
                            </div>
                            <div class="col-lg-6">
                                
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="well col-lg-6">
                                <p>Agent registered last month</p>
                                <hr/>
                                <h1>90<small> Agents</small></h1>
                            </div>
                            <div class="col-lg-6">
                                <button class="btn btn-primary btn-circle"> 99+</button>
                                <p>Pending manual transaction requests</p>
                                <a href="../admin/manual" class="btn btn-warning btn-outline"> more details</a>                            
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <a href="../admin/user" class="btn btn-primary btn-outline">Manage User <i class="fa fa-arrow-right"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.row -->