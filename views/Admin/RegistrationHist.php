<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Registration Approval<small> <i class="fa fa-list-alt"></i> History</small></h1>
	</div>
	<!-- /.col-lg-12 -->
</div>

<div class="row">
	<div class="col-lg-12">
		<div class="row">
			<div class="col-lg-12">
				<a href="../admin/registrationApproval" class="btn btn-outline btn-success">Transaction List</a>
				<a href="../admin/registrationHist" class="btn btn-outline btn-success">History</a>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-primary">
					<div class="panel-heading">
						Registration History List
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-4 pull-right hide">
								<div class="well">
									<form class="form-inline" role="form">
										<div class="form-group">
											<select class="form-control" style="width:180px;">
												<?php
												include 'sample-filter.php';
                                                ?>
											</select>
										</div>
										<button type="submit" class="btn btn-primary btn-default">
											Filter
										</button>
									</form>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12">
								<table class="table table-striped table-bordered table-hover" id="dataTables-example">
									<thead>
										<tr>
											<th>
											<input type="checkbox" id="selecctall">
											</th>
											<th>Name</th>
											<th>Address/Contact</th>
											<th>Bank in(RM)</th>
											<th>Receipt</th>
											<th>Status</th>
										</tr>
									</thead>
									<tbody>
										
										<tr>
											<td>
											<input type="checkbox" class="checkbox1" name="check[]">
											</td>
											<td>Muhaimin Juhari</td>
											<td>
                                                <b>Address:</b><br/>
                                                45-2A Lorong Ara Kiri 1, Kompleks Lucky Garden, Bangsar 59100, KL
                                                <br/><b>Phone:</b> 0135440305
                                                <br/><b>Email:</b> muhaiminjuhari@wiredin.my
                                            </td>
								            <td>500</td>
                                            <td>
                                                <a href="" class="popup_image"><img width="200px" height="120px" class="" src="holder.js/200x200" /></a>
                                            </td>
											<td><span class="label label-success">Accepted</span><span class="label label-danger">Rejected</span></td>
										</tr>
										

									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="panel-footer">
						<a href="" class="btn btn-primary btn-outline"> Print list <i class="fa fa-arrow-right"></i></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /.row -->