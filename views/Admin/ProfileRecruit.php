<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Profile <small><i class="fa fa-user"></i> Recruits</small></h1>
	</div>
	<!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <a href="../admin/profile" class="btn btn-primary btn-outline">Edit</a>
        <a href="../admin/profileCommission" class="btn btn-primary btn-outline">Commission</a>
        <a href="../admin/profileRecruit" class="btn btn-primary btn-outline">Recruit List</a>
        <a href="../admin/profileStock" class="btn btn-primary btn-outline">Stock</a>
    </div>    
</div>
<br>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Recruit List
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-12">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Date Joined</th>
                                    <th>Level</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><a href="../admin/profile">Muhaimin Juhari</a></td>
                                    <td>6/3/2014</td>
                                    <td><button class="btn btn-circle btn-primary btn-outline">1</button></td>
                                </tr>
                                <tr>
                                    <td><a href="../admin/profile">Nur Fitriah</a></td>
                                    <td>12/2/2014</td>
                                    <td><button class="btn btn-circle btn-success btn-outline">2</button></td>
                                </tr>
                                <tr>
                                    <td><a href="../admin/profile">Nellisa Ezzaty</a></td>
                                    <td>1/1/2014</td>
                                    <td><button class="btn btn-circle btn-success btn-outline">2</button></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <a href="" class="btn btn-primary btn-outline"> Print list <i class="fa fa-arrow-right"></i></a>
            </div>
        </div>
    </div>
</div>
