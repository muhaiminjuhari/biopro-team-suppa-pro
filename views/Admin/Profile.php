<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Profile <small><i class="fa fa-user"></i></small></h1>
	</div>
	<!-- /.col-lg-12 -->
</div>
<div class="row">
	<div class="col-lg-12">
		<a href="../admin/profile/<?=$data['agent']['login_id']?>" class="btn btn-primary btn-outline">Edit</a>
		<a href="../admin/profileCommission/<?=$data['agent']['login_id']?>" class="btn btn-primary btn-outline">Commission</a>
		<!-- <a href="../admin/profileRecruit/<?=$data['agent']['login_id']?>" class="btn btn-primary btn-outline">Recruit</a>
		<a href="../admin/profileStock/<?=$data['agent']['login_id']?>" class="btn btn-primary btn-outline">Stock</a> -->
	</div>
</div>
<br>
<div class="row">
	<div class="col-lg-3">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Profile Picture</h3>
			</div>
			<div class="panel-body">
				<?php
				if (isset($_GET['success'])) {
					if ($_GET['success'] == 'picture') {
						echo '
<div class="alert alert-success alert-dismissable">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">
&times;
</button>
<strong>Success! </strong> Your picture updated.
</div>
';
					}
				}
				?>
				<form id="submitForm3" method="post" action="../admin/updatePicture/<?=$data['agent']['login_id']?>" enctype="multipart/form-data">
					<img src="upload/<?= ($data['agent']['picture_name'] != '') ? $data['agent']['picture_name'] : 'holder.js/140x140'; ?>" class="img-responsive profile-img" />
					<input type="file" name="picture" required="required"/>
					<br>
					<button class="btn btn-primary btn-outline btn-block">
						Change Image
					</button>
				</form>
			</div>
		</div>
	</div>
	<div class="col-lg-6">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Personal Information</h3>
			</div>
			<form id="submitForm" method="post" action="../admin/updateProfile/<?=$data['agent']['login_id']?>">
				<div class="panel-body">
					<?php
				if (isset($_GET['success'])) {
					if ($_GET['success'] == 'profile') {
						echo '
<div class="alert alert-success alert-dismissable">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">
&times;
</button>
<strong>Success! </strong> Your profile updated.
</div>
';
					}
				}
				?>
					<fieldset>
						<div class="form-group">
							<input class="form-control" placeholder="First Name" name="fname" type="text" required="required" value="<?=$data['agent']['fname']?>">
						</div>
						<div class="form-group">
							<input class="form-control" placeholder="Last Name" name="lname" type="text" required="required" value="<?=$data['agent']['lname']?>">
						</div>
						<div class="form-group">
							<input class="form-control" placeholder="E-mail" name="email" type="email" required="required" value="<?=$data['agent']['email']?>">
						</div>
						<div class="form-group">
							<input class="form-control" placeholder="Contact Number" name="contactNo" type="text" required="required" value="<?=$data['agent']['phone']?>">
						</div>
						<div class="form-group">
							<label>Gender</label>
							<label class="radio-inline">
								<input type="radio" name="gender" id="optionsRadiosInline1" value="male" <?= ($data['agent']['gender'] == 'male') ? 'checked' : ''; ?> required="required" >
								Male </label>
							<label class="radio-inline">
								<input type="radio" name="gender" id="optionsRadiosInline2" value="female" <?= ($data['agent']['gender'] == 'female') ? 'checked' : ''; ?> >
								Female </label>
						</div>
						<div class="form-group">
							<label>Malaysian Citizenship</label>
							<label class="radio-inline">
								<input type="radio" name="local" id="optionsRadiosInline1" value="1" <?= ($data['agent']['malaysian'] == '1') ? 'checked' : ''; ?>  required="required">
								Yes </label>
							<label class="radio-inline">
								<input type="radio" name="local" id="optionsRadiosInline2" value="0" <?= ($data['agent']['malaysian'] == '0') ? 'checked' : ''; ?> >
								No </label>
						</div>
						<div class="form-group">
							<label>IC Number/Passport Number</label>
							<input class="form-control" placeholder="" name="icno" type="text" required="required" value="<?=$data['agent']['icpass']?>">
						</div>
						<div class="form-group">
							<label>Address</label>
							<input class="form-control" placeholder="Address 1" name="add1" type="text" required="required" value="<?=$data['agent']['address1']?>">
						</div>
						<div class="form-group">
							<input class="form-control" placeholder="Address 2" name="add2" type="text" value="<?=$data['agent']['address2'] ?>">
						</div>
						<div class="form-group">
							<input class="form-control" placeholder="City/Town" name="city" type="text" required="required" value="<?=$data['agent']['city'] ?>">
						</div>
						<div class="form-group">
							<input class="form-control" placeholder="Postcode" name="postcode" type="text" required="required" value="<?=$data['agent']['postcode'] ?>">
						</div>
						<div class="form-group">
							<input class="form-control" placeholder="State" name="state" type="text" required="required" value="<?=$data['agent']['state'] ?>">
						</div>
						<div class="form-group">
							<select class="form-control" name="country" required="required">
								<option value="">Country</option>
								<option value="MALAYSIA" <?= ($data['agent']['country'] == 'MALAYSIA') ? 'selected' : ''; ?> >Malaysia</option>
								<option value="INDONESIA" <?= ($data['agent']['country'] == 'INDONESIA') ? 'selected' : ''; ?>>Indonesia</option>
								<option value="THAILAND" <?= ($data['agent']['country'] == 'THAILAND') ? 'selected' : ''; ?>>Thailand</option>
								<option value="SINGAPORE" <?= ($data['agent']['country'] == 'SINGAPORE') ? 'selected' : ''; ?>>Singapore</option>
								<option value="BRUNEI" <?= ($data['agent']['country'] == 'BRUNEI') ? 'selected' : ''; ?>>Brunei</option>
							</select>
						</div>

						<div class="form-group">
							<label>Account</label>
							<div class="form-group">
								<select class="form-control" name="bankaccount">
									<?php
									$key = explode(':', $data['agent']['account_bank']);
									?>
									<option <?= ($key[0] == 'Maybank') ? 'selected' : ''; ?> value="Maybank">Maybank</option>
									<option <?= ($key[0] == 'CIMB') ? 'selected' : ''; ?> value="CIMB">CIMB</option>
									<option <?= ($key[0] == 'Hong Leong') ? 'selected' : ''; ?>  value="Hong Leong">Hong Leong</option>
									<option <?= ($key[0] == 'Bank Islam') ? 'selected' : ''; ?> value="Bank Islam">Bank Islam</option>
									<option <?= ($key[0] == 'Other') ? 'selected' : ''; ?> value="Other">Other Bank</option>
								</select>
							</div>
							<div class="form-group">
								<label>If made payment from 'Other Bank' please state here:</label>
								<input name="other" type="text" class="form-control" style="width:420px;" placeholder="Other Bank" value="<?=(isset($key[1]))?$key[1]:''; ?>">
							</div>
							<input class="form-control" placeholder="Account Number" name="accno" type="text" required="required" value="<?=$data['agent']['account_number'] ?>">
						</div>

					</fieldset>
				</div>
				<div class="panel-footer">
					<button class="btn btn-success btn-outline">
						Update <i class="fa fa-save"></i>
					</button>
				</div>
			</form>
		</div>
	</div>
</div>
<div class="row">

</div>
