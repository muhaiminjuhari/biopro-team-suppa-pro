<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Manual Transaction<small> <i class="fa fa-list-alt"></i> Transaction List</small></h1>
	</div>
	<!-- /.col-lg-12 -->
</div>

<div class="row">
	<div class="col-lg-12">
		<div class="row">
			<div class="col-lg-12">
				<a href="../admin/manual" class="btn btn-outline btn-success">Transaction List</a>
				<a href="../admin/manualHistory" class="btn btn-outline btn-success">History</a>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-lg-3">
				<div class="panel panel-info">
					<div class="panel-heading">
						Total Pending Approval
					</div>
					<div class="panel-body">
						<h3><?=$data['totalPending']; ?> <small>transactions <i class="fa fa-list-alt"></i></small></h3>
					</div>
				</div>
			</div>
			<div class="col-lg-9">
				<div class="panel panel-info">
					<div class="panel-heading">
						Transaction Search
					</div>
					<div class="panel-body">
						<div class="form-inline">
							<div class="form-group">
								<p>
									Search for specific transaction, user etc.
								</p>
								<input class="form-control" type="search" placeholder="Keyword..." style="width:220px;" />
								<a href="" class="btn btn-primary btn-outline"> Search <i class="fa fa-search"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-primary">
					<div class="panel-heading">
						Pending Transactions List
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-8">
								<div class="well">
									<div class="form-inline">
										<div class="form-group">
											Actions for selected item:
										</div>
										<div class="form-group">
											<a href="javascript:;" class="btn btn-primary">Approve</a>
											<a href="javascript:;" class="btn btn-warning">Reject</a>
										</div>
										<div class="form-group pull-right">
											<a href="javascript:;" class="btn btn-primary btn-outline"> Print list <i class="fa fa-arrow-right"></i></a>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-4 pull-right">
								<div class="well">
									<form class="form-inline" role="form">
										<div class="form-group">
											<select class="form-control" style="width:180px;">
												<?php
												include 'sample-filter.php';
 ?>
											</select>
										</div>
										<button type="submit" class="btn btn-primary btn-default">
											Filter
										</button>
									</form>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12">
								<table class="table table-striped table-bordered table-hover" id="dataTables-example">
									<thead>
										<tr>
											<th>
											<input type="checkbox" id="selecctall">
											</th>
											<th>Name</th>
											<th>Timestamp</th>
											<th>Amount <i class="fa fa-dropbox"></i></th>
											<th>Total Bank in(RM)</th>
											<th>Receipt</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<?php
										foreach($data['alltransaction'] as $key){
										?>
										<tr>
											<td>
											<input type="checkbox" class="checkbox1" name="check[]">
											</td>
											<td><a href="../admin/profile/<?=$key['login_id'] ?>"><?=$key['fname'] . ' ' . $key['lname']; ?></a>
											<br>
											<p>
												Account Bank: <?=$key['account_bank']; ?><br>
                                                Account Number: <?=$key['account_number']; ?><br>
											</p>
											<p>
												Contact: <?=$key['phone'] ?><br>
                                                Email: <?=$key['email'] ?>
											</p></td>
											<td><?= date('g:i a', strtotime($key['time_payment'])); ?> / <?= date("d-M-Y", strtotime($key['date_payment'])); ?></td>
											<td> <?=$key['stock_quantity']; ?> </td>
												<td><?=$key['amount_payment'] ?></td>
												<td>
													<!-- <img src="holder.js/200x120"> -->
													<a href="../views/upload/receipt/<?=$key['pic_slip'] ?>" class="popup_image"><img width="200px" height="120px" class="" src="../views/upload/receipt/<?=$key['pic_slip'] ?>" /></a>
												</td>
											<td><a href="../admin/acceptTransaction/<?=$key['id'] ?>" class="btn btn-primary confirm">Approve</a> <a href="../admin/rejectTransaction/<?=$key['id'] ?>" class="btn btn-warning confirm">Reject</a></td>
										</tr>
										<?php
										}
										?>

									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="panel-footer">
						<a href="" class="btn btn-primary btn-outline"> Print list <i class="fa fa-arrow-right"></i></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /.row -->