<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Manual Transaction<small> <i class="fa fa-list-alt"></i> History</small></h1>
	</div>
	<!-- /.col-lg-12 -->
</div>

<div class="row">   
    <div class="col-lg-12">
        <div class="row">
            <div class="col-lg-12">
                <a href="../admin/manual" class="btn btn-outline btn-success">Transaction List</a>
                <a href="../admin/manualHistory" class="btn btn-outline btn-success">History</a>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        Transaction Search
                    </div>
                    <div class="panel-body">
                        <div class="form-inline">
                            <div class="form-group">
                                <p>Search for specific transaction, user etc.</p>
                                <input class="form-control" type="search" placeholder="Keyword..." style="width:220px;" />
                                <a href="" class="btn btn-primary btn-outline"> Search <i class="fa fa-search"></i></a>
                            </div>
                        </div>                       
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        Transactions List (History)
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-4 pull-right">
                                <div class="well">
                                    <form class="form-inline" role="form" method="get">
                                        <div class="form-group">     
                                        <select class="form-control" name="status" style="width:180px;">
                                            <option value="Accept" <?=($data['status']=='Approved')?'Selected':''; ?>>Approved</option>
                                            <option value="Reject" <?=($data['status']=='Rejected')?'Selected':''; ?>>Rejected</option>
                                        </select>
                                        </div>
                                        <button type="submit" class="btn btn-primary btn-default">Filter</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>
											<input type="checkbox" id="selecctall">
											</th>
                                            <th>Name</th>
                                            <th>Timestamp</th>
                                            <th>Status</th>
                                            <th>Receipt</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	<?php
										foreach($data['alltransaction'] as $key){
										?>
                                        <tr>
                                            <td>
											<input type="checkbox" class="checkbox1" name="check[]">
											</td>
                                            <td>
                                                <a href="../admin/profile/<?=$key['login_id'] ?>"><?=$key['fname'] . ' ' . $key['lname']; ?></a><br>
                                                <p>
                                                    Account Bank: <?=$key['account_bank']; ?><br>
                                                	Account Number: <?=$key['account_number']; ?><br>
                                                </p>
                                                <p>
                                                    Contact: <?=$key['phone'] ?><br>
                                                    Email: <?=$key['email'] ?>
                                                </p>
                                            </td>
                                            <td><?= date('g:i a', strtotime($key['time_payment'])); ?> / <?= date("d-M-Y", strtotime($key['date_payment'])); ?></td>
                                            <td>
                                            	<?php
                                            	if($key['status'] == 'Accept'){
                                            	?>
                                            	<span class="label label-success">Approved</span>
                                            	<?php
												}else{
												?>
                                            	<span class="label label-danger">Rejected</span>
                                            	<?php
												}
												?>
                                            </td>
                                            <td><a href="../views/upload/receipt/<?=$key['pic_slip'] ?>" class="popup_image"><img width="200px" height="120px" class="" src="../views/upload/receipt/<?=$key['pic_slip'] ?>" /></a></td>
                                            <td><a href="javascript:;" class="btn btn-danger"> Delete</a></td> 
                                        </tr>
                                        <?php
                                        }
                                        ?>
                                        <!-- <tr>
                                            <td><input type="checkbox"></td>
                                            <td>
                                                <a href="#">Nur Fitriah</a><br>
                                                <p>
                                                    CIMB<br>
                                                    100039278<br>
                                                </p>
                                                <p>
                                                    Contact: 0125391715<br>
                                                    Email: nurfitriah@unimap.edu.my
                                                </p>
                                            </td>
                                            <td>14:03 11/12/2014</td>
                                            <td><span class="label label-success">Approved</span></td>
                                            <td><img src="holder.js/210x120/sky"></td>
                                            <td><a href="" class="btn btn-danger"> Delete</a><a href="" class="btn btn-danger visible-sm"></a></td> 
                                        </tr>
                                        <tr>
                                            <td><input type="checkbox"></td>
                                            <td>
                                                <a href="javascript:;">Nellisa Ezzaty</a><br>
                                                <p>
                                                    Hong Leong<br>
                                                    124123123<br>
                                                </p>
                                                <p>
                                                    Contact: 0135440305<br>
                                                    Email: nellisa@gmail.com
                                                </p>
                                            </td>
                                            <td>09:03 2/6/2014</td>
                                            <td><span class="label label-danger">Rejected</span></td>
                                            <td><img src="holder.js/210x120/sky"></td>
                                            <td><a href="javascript:;" class="btn btn-danger"> Delete</a></td> 
                                        </tr> -->
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <a href="" class="btn btn-primary btn-outline"> Print list <i class="fa fa-arrow-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.row -->