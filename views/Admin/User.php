<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">User Management<small> <i class="fa fa-group"></i></small></h1>
	</div>
	<!-- /.col-lg-12 -->
</div>

<div class="row">   
    <div class="col-lg-12">
        <div class="row">
            <div class="col-lg-12">
                <a href="../admin/addNewAgent" class="btn btn-outline btn-success">Add New Agent</a>
                <a href="../admin/user" class="btn btn-outline btn-success">User List</a>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-lg-3">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        Total Registered Agents
                    </div>
                    <div class="panel-body">
                         <h3><?=$data['totalAgent']; ?> <small>agents <i class="fa fa-group"></i></small></h3>                          
                    </div>
                </div>
            </div>
            <div class="col-lg-9">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        User Search
                    </div>
                    <div class="panel-body">
                        <div class="form-inline">
                            <div class="form-group">
                                <p>Search for specific user, email, contact etc.</p>
                                <input class="form-control" type="search" placeholder="Keyword..." style="width:220px;" />
                                <a href="" class="btn btn-primary btn-outline"> Search <i class="fa fa-search"></i></a>
                            </div>
                        </div>                       
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        User List
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="well">
                                    <div class="form-inline">
                                        <div class="form-group">Actions for selected item: </div>
                                        <div class="form-group">      
                                            <a href="" class="btn btn-danger">Delete</a>
                                        </div>
                                        <div class="form-group pull-right">
                                             <a href="" class="btn btn-primary btn-outline"> Print list <i class="fa fa-arrow-right"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 pull-right">
                                <div class="well">
                                    <form class="form-inline" role="form">
                                        <div class="form-group">     
                                        <select class="form-control" style="width:180px;">
                                            <?php include 'sample-filter.php'; ?>
                                        </select>
                                        </div>
                                        <button type="submit" class="btn btn-primary btn-default">Filter</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th><input type="checkbox" id="selecctall"></th>
                                            <th>Name</th>
                                            <th>Referral Code</th>
                                            <th>Region/City</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	<?php
                                    	foreach($data['agent'] as $agent){
                                    	?>
                                        <tr>
                                            <td><input type="checkbox" class="checkbox1" name="check[]"></td>
                                            <td>
                                                <a href="../admin/profile/<?=$agent['login_id']; ?>"><?=$agent['fname']; ?></a><br>
                                                <p>
                                                    Account Bank: <?=$agent['account_bank']; ?><br>
                                                    Account Number: <?=$agent['account_number']; ?><br>
                                                </p>
                                                <p>
                                                    Contact: <?=$agent['phone']; ?><br>
                                                    Email: <?=$agent['email']; ?>
                                                </p>
                                            </td>
                                            <td>
                                            	<?=$agent['referral_code']; ?>
                                            </td>
                                            <td>
                                            	<p>
                                            		<?=$agent['address1'] . '<br />'; ?>
                                            		<?=$agent['address2'] . '<br />'; ?>
                                            		<?=$agent['postcode'] . ' '; ?>
                                            		<?=$agent['city'] . '<br />'; ?>
                                            		<?=$agent['state'] . '<br />'; ?>
                                            		<?=$agent['country'] . '<br />'; ?>
                                            	</p>
                                            </td>
                                            <td><a href="../admin/deleteThisUser/<?=$agent['login_id']; ?>" class="btn btn-danger confirm"> Delete</a></td> 
                                        </tr>
                                        <?php
                                        }
                                    	?>
				
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <a href="" class="btn btn-primary btn-outline"> Print list <i class="fa fa-arrow-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.row -->