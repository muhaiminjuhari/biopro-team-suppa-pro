<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Stock Management<small> <i class="fa fa-dropbox"></i></small></h1>
	</div>
	<!-- /.col-lg-12 -->
</div>

<div class="row">
	<div class="col-lg-12">
		<div class="row">
			<div class="col-lg-3">
				<div class="panel panel-primary">
					<div class="panel-heading">
						Current Stocks
					</div>
					<div class="panel-body">
						<?=$data['updateMessage']; ?>
						<h1><?=$data['currentStock'] ?>
						<small><i class="fa fa-dropbox"></i> Boxes</small></h1>
					</div>
				</div>
			</div>
			<div class="col-lg-9">
				<div class="panel panel-default">
					<div class="panel-heading">
						Stock Search <b>(IC)</b>
					</div>
					<div class="panel-body">
						<p>
							Search for stock that have been purchased by agents. Search by product ID. 
						</p>
						<div class="form-inline">
							<div class="form-group">
								<input class="form-control" type="text" placeholder="Enter Product ID..." />
								<a href="../admin/stockSearch" class="btn btn-primary"> Search <i class="fa fa-search"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						Add/Remove Stock
					</div>
					<div class="panel-body">
						<p>
							To add stock, simply enter the amount below and click Add.
						</p>
						<div class="well">
							<form id="submitForm" action="../admin/updateStock/add" method="post" class="form-inline" role="form">
								<div class="form-group">
									<input name="newstock" type="number" class="form-control" id="" placeholder="Enter amount.." style="width:120px;" required="required">
								</div>
								<button class="btn btn-primary btn-outline">
									Add <i class="fa fa-plus"></i>
								</button>
							</form>
						</div>
						<p>
							To remove stock, simply enter the desired amount below and click Remove.
						</p>
						<div class="well">
							<form id="submitForm3" action="../admin/updateStock/minus" method="post" class="form-inline" role="form">
								<div class="form-group">
									<input name="newstock" type="number" class="form-control" id="" placeholder="Enter amount.." style="width:120px;" required="required">
								</div>
								<button class="btn btn-primary btn-outline">
									Remove <i class="fa fa-minus"></i>
								</button>
							</form>
						</div>
						<span class="label label-warning"> Warning: All Add/Remove actions cannot be undone!</span>
					</div>
				</div>
			</div>
			<div class="col-lg-8">
				<div class="panel panel-default">
					<div class="panel-heading">
						Disable Stock
					</div>
					<div class="panel-body">
						<div class="well">
							<span class=""> To make all stock unpurchaseable, simply click the disable button below. All stock will be disable, and no merchant activities can be done. </span>
							<br>
							<p>
								<span class="label label-warning">Warning: All user cannot make any transactions!</span>
							</p>
							<br>
							<?php
							if($data['available'] == 'TRUE'){
							?>
							<a href="../admin/updateStatusStock" class="btn btn-success btn-outline confirm">Enable <i class="fa fa-check"></i></a>
							<?php
							}else if($data['available'] == 'FALSE'){
							?>
							<a href="../admin/updateStatusStock" class="btn btn-danger btn-outline confirm">Disable <i class="fa fa-times"></i></a>
							<?php
							}
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /.row -->