<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Commission Management<small> <i class="fa fa-dollar"></i> Log</small></h1>
	</div>
	<!-- /.col-lg-12 -->
</div>

<div class="row">   
    <div class="col-lg-12">
        <div class="row">
            <div class="col-lg-12">
                <a href="../admin/profileCommission/<?=$data['id'];?>" class="btn btn-outline btn-success">Back</a><!-- 
                <a href="../admin/commission" class="btn btn-outline btn-success">Commission List</a>
                <a href="../admin/commissionHistory" class="btn btn-outline btn-success">History</a> -->
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-lg-3">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        Commission List For
                    </div>
                    <div class="panel-body">
                         <h3><?= $data['date']; ?></h3>
                         <!-- <hr /> -->
                          <!-- <div class="form-group form-inline"> 
                         	<input id="default_widget" class="form-control" value="Select new month" />
                         	<button class="btn btn-primary">Go</button>
                         </div> -->
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        Total Commission For <?= $data['date']; ?>
                    </div>
                    <div class="panel-body">
                         <h3>RM <?= ($data['total'] == '' ? '0.00' : number_format((float)$data['total'], 2, '.', '')); ?></h3>    
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        View Commission List For
                    </div>
                    <div class="panel-body">
                          <p>
                          	<div class="form-group form-inline"> 
                          		<form action="" method="get">
	                         		<input id="default_widget" class="form-control" name="month" placeholder="Click to view calendar" />
	                         		<button class="btn btn-primary">Go</button>
                         		</form>
                         	</div>
                         </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        Commission List
                    </div>
                    <div class="panel-body">
                        <!-- <div class="row">
                            <div class="col-lg-8">
                                <div class="well">
                                    <div class="form-inline">
                                        <div class="form-group">Actions for selected</div>
                                        <div class="form-group">      
                                            <a href="" class="btn btn-primary">Paid</a>
                                            <a href="" class="btn btn-primary">Pending</a>
                                        </div>
                                        <div class="form-group pull-right">
                                             <a href="" class="btn btn-primary btn-outline"> Print list <i class="fa fa-arrow-right"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 pull-right">
                                <div class="well">
                                    <form class="form-inline" role="form">
                                        <div class="form-group">     
                                        <select class="form-control" style="width:180px;">
                                            <?php include 'sample-filter.php'; ?>
                                        </select>
                                        </div>
                                        <button type="submit" class="btn btn-primary btn-default">Filter</button>
                                    </form>
                                </div>
                            </div>
                        </div> -->
                        <div class="row">
                            <div class="col-lg-12">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                        	<th>Date</th>
                                            <th>Detail</th>
                                            <th>Total</th>
                                            <th>From</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	<?php
                                    	foreach($data['log']as $log){
                                    	?>
                                        <tr>
                                            <td><?=$log['date_log'];?></td>
                                            <td><?=$log['detail'];?></td>
                                            <td>RM <?=number_format((float)$log['total'], 2, '.', ''); ?></td>
                                            <td><?=$log['fname'] . ' ' . $log['lname'];?></td>
                                        </tr>
                                        <?php
										}
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <a href="" class="btn btn-primary btn-outline"> Print list <i class="fa fa-arrow-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.row -->