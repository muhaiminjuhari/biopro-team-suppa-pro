<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Profile <small><i class="fa fa-user"></i> Commission</small></h1>
	</div>
	<!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <a href="../admin/profile/<?=$data['id'];?>" class="btn btn-primary btn-outline">Edit</a>
        <a href="../admin/profileCommission/<?=$data['id'];?>" class="btn btn-primary btn-outline">Commission</a>
        <!-- <a href="../admin/profileRecruit" class="btn btn-primary btn-outline">Recruit List</a>
        <a href="../admin/profileStock" class="btn btn-primary btn-outline">Stock</a> -->
    </div>    
</div>
<br/>
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-warning">
			<div class="panel-heading">User Information</div>
			<div class="panel-body">
				<div class="col-lg-12">
					<p>
						Name: <b><?=$data['agent']['fname'];?> <?=$data['agent']['lname'];?></b><br/>
						Contact: <b><?=$data['agent']['phone'];?></b><br/>
						Email: <b><?=$data['agent']['email'];?></b><br/>
						Bank Acct: <b><?=$data['agent']['account_bank'];?> <?=$data['agent']['account_number'];?></b>
					
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
	
<br>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Commission List
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-12">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Commission (RM)</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            	<?php
                            	foreach($data['commissionList']  as $com){
                            	?>
                                <tr>
                                    <td><?= date('1 F Y', strtotime($com['date_getCommission'])); ?></td>
                                    <td><?=$com['total_commission']; ?></td>
                                    <td>
                                    	<?php
                                    	if($com['status']=='Pending'){
                                    	?>
                                    	<span class="label label-warning">Pending</span>
                                    	<?php
										}else{
                                    	?>
                                    	<span class="label label-success">Paid</span>
                                    	<?php
										}
                                    	?>
                                    </td>
                                     <td>
                                	<a href="../admin/commissionLog/<?=$data['id'];?>?month=<?= date('m-Y', strtotime($com['date_getCommission'])); ?>" class="btn btn-success">View</a>
                                </td>
                                </tr>
                                <?php
								}
                                ?>
                               
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <a href="" class="btn btn-primary btn-outline"> Print list <i class="fa fa-arrow-right"></i></a>
            </div>
        </div>
    </div>
</div>
