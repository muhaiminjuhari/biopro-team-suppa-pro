<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Stock Management<small> <i class="fa fa-dropbox"></i> Search</small></h1>
	</div>
	<!-- /.col-lg-12 -->
</div>

<div class="row">   
    <div class="col-lg-12">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                       Stock Search Result
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="well">
                                    <div class="form-inline">
                                        <div class="form-group">
                                            <input class="form-control" placeholder="Enter Product ID.." />
                                            <a href="" class="btn btn-primary"> <i class="fa fa-search"> Search</i></a>
                                        </div>
                                        
                                        <div class="form-group pull-right">
                                             <a href="javascript:;" class="btn btn-primary btn-outline"> Print list <i class="fa fa-arrow-right"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Stock ID</th>
                                            <th>Name</th>
                                            <th>Timestamp Purchased</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>BP00001</td>
                                            <td>
                                                <a href="../admin/profile">Muhaimin Juhari</a><br>
                                                <p>
                                                    Contact: 0135440305<br>
                                                    Email: muhaiminjuhari@gmail.com
                                                </p>
                                            </td>
                                            <td>20:20 12/12/2014</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <a href="" class="btn btn-primary btn-outline"> Print list <i class="fa fa-arrow-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            
            
        </div>
    </div>
</div>
<!-- /.row -->