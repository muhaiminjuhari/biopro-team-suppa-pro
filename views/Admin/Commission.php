<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Commission Management<small> <i class="fa fa-dollar"></i> List</small></h1>
	</div>
	<!-- /.col-lg-12 -->
</div>

<div class="row">   
    <div class="col-lg-12">
        <div class="row">
            <div class="col-lg-12">
                <a href="../admin/commissionRate" class="btn btn-outline btn-success">Set Commission Rate</a>
                <a href="../admin/commission" class="btn btn-outline btn-success">Commission List</a>
                <!-- <a href="../admin/commissionHistory" class="btn btn-outline btn-success">History</a> -->
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-lg-3">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        Commission List For
                    </div>
                    <div class="panel-body">
                         <h3><?=$data['date'];?></h3>
                         <!-- <hr /> -->
                          <!-- <div class="form-group form-inline"> 
                         	<input id="default_widget" class="form-control" value="Select new month" />
                         	<button class="btn btn-primary">Go</button>
                         </div> -->
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        Total Commission For <?=$data['date'];?>
                    </div>
                    <div class="panel-body">
                         <h3>RM <?=($data['commTotal']==''?'0.00': number_format((float)$data['commTotal'], 2, '.', '')); ?></h3>    
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        View Commission List For
                    </div>
                    <div class="panel-body">
                          <p>
                          	<div class="form-group form-inline"> 
                          		<form action="" method="get">
	                         		<input id="default_widget" class="form-control" name="month" placeholder="Click to view calendar" />
	                         		<button class="btn btn-primary">Go</button>
                         		</form>
                         	</div>
                         </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        Commission List
                    </div>
                    <div class="panel-body">
                        <!-- <div class="row">
                            <div class="col-lg-8">
                                <div class="well">
                                    <div class="form-inline">
                                        <div class="form-group">Actions for selected</div>
                                        <div class="form-group">      
                                            <a href="" class="btn btn-primary">Paid</a>
                                            <a href="" class="btn btn-primary">Pending</a>
                                        </div>
                                        <div class="form-group pull-right">
                                             <a href="" class="btn btn-primary btn-outline"> Print list <i class="fa fa-arrow-right"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 pull-right">
                                <div class="well">
                                    <form class="form-inline" role="form">
                                        <div class="form-group">     
                                        <select class="form-control" style="width:180px;">
                                            <?php include 'sample-filter.php'; ?>
                                        </select>
                                        </div>
                                        <button type="submit" class="btn btn-primary btn-default">Filter</button>
                                    </form>
                                </div>
                            </div>
                        </div> -->
                        <div class="row">
                            <div class="col-lg-12">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th><input type="checkbox" id="selecctall"></th>
                                            <th>Name</th>
                                            <th>Commission</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	<?php
                                    	foreach($data['commissionList'] as $commissionList){
                                    	?>
                                        <tr>
                                            <td><input type="checkbox" class="checkbox1" name="check[]"></td>
                                            <td>
                                                <a href="../admin/profile/<?=$commissionList['login_id'];?>"><?=$commissionList['fname'] . ' ' . $commissionList['lname'];?></a><br>
                                                <p>
                                                    Account Bank: <?=$commissionList['account_bank']; ?><br>
                                                    Account Number: <?=$commissionList['account_number']; ?><br>
                                                </p>
                                                <p>
                                                    Contact: <?=$commissionList['phone']; ?><br>
                                                    Email: <?=$commissionList['email']; ?>
                                                </p>
                                            </td>
                                            <td>RM <?=number_format((float)$commissionList['total_commission'], 2, '.', ''); ?> <a href="../admin/commissionLog/<?=$commissionList['login_id'];?>?month=<?= date('m-Y', strtotime($data['date'])); ?>">View Detail</a></td>
                                            <td><span class="label label-warning">Pending</span></td>
                                        </tr>
                                        <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <a href="" class="btn btn-primary btn-outline"> Print list <i class="fa fa-arrow-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.row -->