<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Registration Approval<small> <i class="fa fa-list-alt"></i> Request List</small></h1>
	</div>
	<!-- /.col-lg-12 -->
</div>

<div class="row">
	<div class="col-lg-12">
		<div class="row">
			<div class="col-lg-12">
				<!-- <a href="../admin/registrationApproval" class="btn btn-outline btn-success">Transaction List</a> -->
				<!-- <a href="../admin/registrationHist" class="btn btn-outline btn-success">History</a> -->
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-lg-3">
				<div class="panel panel-info">
					<div class="panel-heading">
						Total Pending Approval
					</div>
					<div class="panel-body">
						<h3>20<small> Registration Request</i></small></h3>
					</div>
				</div>
			</div>
			<div class="col-lg-9 hide">
				<div class="panel panel-info">
					<div class="panel-heading">
						Registration Search
					</div>
					<div class="panel-body">
						<div class="form-inline">
							<div class="form-group">
								<p>
									Search for specific registration.
								</p>
								<input class="form-control" type="search" placeholder="Keyword..." style="width:220px;" />
								<a href="" class="btn btn-primary btn-outline"> Search <i class="fa fa-search"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-primary">
					<div class="panel-heading">
						Request Request List
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-12">
								<div class="well">
									<div class="form-inline">
										<div class="form-group">
											Actions for selected item:
										</div>
										<div class="form-group">
											<a href="javascript:;" class="btn btn-primary">Approve</a>
											<a href="javascript:;" class="btn btn-warning">Reject</a>
										</div>
										<div class="form-group pull-right">
											<a href="javascript:;" class="btn btn-primary btn-outline"> Print list <i class="fa fa-arrow-right"></i></a>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-4 pull-right hide">
								<div class="well">
									<form class="form-inline" role="form">
										<div class="form-group">
											<select class="form-control" style="width:180px;">
												<?php
												include 'sample-filter.php';
                                                ?>
											</select>
										</div>
										<button type="submit" class="btn btn-primary btn-default">
											Filter
										</button>
									</form>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12">
								<table class="table table-striped table-bordered table-hover" id="dataTables-example">
									<thead>
										<tr>
											<th>
											<input type="checkbox" id="selecctall">
											</th>
											<th>Name</th>
											<th>Address/Contact</th>
											<th>Bank in(RM)</th>
											<th>Receipt</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<?php
										foreach($data['listRegister'] as $register){
										?>
										<tr>
											<td>
											<input type="checkbox" class="checkbox1" name="check[]">
											</td>
											<td><?= $register['fname'] . ' ' . $register['lname']; ?></td>
											<td>
                                                <b>Address:</b><br/>
                                                <?= $register['address1']; ?>, <?= $register['address2']; ?>, <?= $register['city']; ?> <?= $register['postcode']; ?>, <?= $register['state']; ?>
                                                <br/><b>Phone:</b> <?= $register['phone']; ?>
                                                <br/><b>Email:</b> <?= $register['email']; ?>
                                            </td>
								            <td><?= $register['payment']; ?>
								            	<br /><b>Bank In Time:</b> <?= $register['timeInSlip']; ?>
								            	<br /><b>Bank In Date:</b> <?= $register['dateInSlip']; ?>
								            </td>
                                            <td>
                                                <a href="upload/receipt/<?= $register['imageName']; ?>" class="popup_image"><img width="200px" height="120px" class="" src="upload/receipt/<?= $register['imageName']; ?>" /></a>
                                            </td>
											<td><a href="../admin/approveRegister/<?= $register['id']; ?>?email=<?= $register['email']; ?>&login_id=<?= $register['login_id']; ?>" class="btn btn-primary confirm">Approve</a> <a href="../admin/rejectRegister/<?= $register['id']; ?>?email=<?=$register['email']; ?>&login_id=<?= $register['login_id']; ?>" class="btn btn-warning confirm">Reject</a></td>
										</tr>
										<?php
										}
										?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="panel-footer">
						<a href="" class="btn btn-primary btn-outline"> Print list <i class="fa fa-arrow-right"></i></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /.row -->