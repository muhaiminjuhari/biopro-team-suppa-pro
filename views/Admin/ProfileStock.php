<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Profile <small><i class="fa fa-user"></i> Stock</small></h1>
	</div>
	<!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <a href="../admin/profile" class="btn btn-primary btn-outline">Edit</a>
        <a href="../admin/profileCommission" class="btn btn-primary btn-outline">Commission</a>
        <a href="../admin/profileRecruit" class="btn btn-primary btn-outline">Recruit List</a>
        <a href="../admin/profileStock" class="btn btn-primary btn-outline">Stock</a>
    </div>    
</div>
<br>
<div class="row">
    <div class="col-lg-3">
        <div class="panel panel-default">
            <div class="panel-heading">Total Stock</div>
            <div class="panel-body">
                <h2>3 <small><i class="fa fa-dropbox"></i> Stocks </small></h2>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Stock List
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-12">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                                <tr>
                                    <th>Product ID</th>
                                    <th>Date Purchased</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>BP00001</td>
                                    <td>10:20 6/3/2014</td>
                                </tr>
                                <tr>
                                    <td>BP00003</td>
                                    <td>9:33 5/12/2014</td>
                                </tr>
                               <tr>
                                    <td>BP00423</td>
                                    <td>10:00 6/12/2014</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <a href="" class="btn btn-primary btn-outline"> Print list <i class="fa fa-arrow-right"></i></a>
            </div>
        </div>
    </div>
</div>
