<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Commission Management<small> <i class="fa fa-dollar"></i> Rate</small></h1>
	</div>
	<!-- /.col-lg-12 -->
</div>

<div class="row">   
    <div class="col-lg-12">
        <div class="row">
            <div class="col-lg-12">
                <a href="../admin/commissionRate" class="btn btn-outline btn-success">Set Commission Rate</a>
                <a href="../admin/commission" class="btn btn-outline btn-success">Commission List</a>
                <!-- <a href="../admin/commissionHistory" class="btn btn-outline btn-success">History</a> -->
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-lg-5">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Set Commission Rate
                    </div>
                    <div class="panel-body">
                    	<?=$data['updateMessage']; ?>
                        <div class="row">
                            <div class="col-lg-2">
                                <p>Level</p> 
                                <button class="btn btn-primary btn-circle btn-outline">1</button>
                            </div>
                            <div class="col-lg-10">
                                <div class="well">
                                    <form method="post" action="../admin/updateCommissionRate/1" class="form-inline" role="form" id="submitForm">
                                      <div class="form-group">
                                        <input name="amount" type="number" class="form-control" id="" placeholder="Enter amount.." required="required">
                                      </div>                 
                                      <button class="btn btn-primary btn-outline"><i class="fa fa-check"></i></button>
                                    </form>
                                </div>
                             </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-2">
                                <p>Level</p> 
                                <button class="btn btn-primary btn-circle btn-outline">2</button>
                            </div>
                            <div class="col-lg-10">
                                <div class="well">
                                    <form method="post" action="../admin/updateCommissionRate/2" class="form-inline" role="form" id="submitForm3">
                                      <div class="form-group">
                                        <input name="amount" type="number" class="form-control" id="" placeholder="Enter amount.." required="required">
                                      </div>                 
                                      <button class="btn btn-primary btn-outline"><i class="fa fa-check"></i></button>
                                    </form>
                                </div>
                             </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Current Commission Rate
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <p>Level 1</p>
                                <h3>RM <?=number_format($data['pricing']['comm_lvl2'], 2)?></h3>
                            </div>
                            <div class="col-lg-6">
                                <p>Level 2</p>
                                <h3>RM <?=number_format($data['pricing']['comm_lvl3'], 2)?></h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.row -->