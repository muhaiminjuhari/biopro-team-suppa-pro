<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Commission Management<small> <i class="fa fa-list-alt"></i> History</small></h1>
	</div>
	<!-- /.col-lg-12 -->
</div>

<div class="row">   
    <div class="col-lg-12">
        <div class="row">
            <div class="col-lg-12">
                <a href="../admin/commissionRate" class="btn btn-outline btn-success">Set Commission Rate</a>
                <a href="../admin/commission" class="btn btn-outline btn-success">Commission List</a>
                <a href="../admin/commissionHistory" class="btn btn-outline btn-success">History</a>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Commission History
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-4 pull-right">
                                <div class="well">
                                    <form class="form-inline" role="form">
                                        <div class="form-group">     
                                        <select class="form-control" style="width:180px;">
                                            <?php include 'sample-filter.php'; ?>
                                        </select>
                                        </div>
                                        <button type="submit" class="btn btn-primary btn-default">Filter</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Commission (RM)</th>
                                            <th>Date</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <a href="#">Muhaimin</a><br>
                                                <p>
                                                    MAYBANK<br>
                                                    12591238652<br>
                                                </p>
                                                <p>
                                                    Contact: 0135440305<br>
                                                    Email: muhaiminjuhari@gmail.com
                                                </p>
                                            </td>
                                            <td>200</td>
                                            <td>12/12/2014</td>
                                            <td><span class="label label-warning">Paid</span></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href="#">Nur Fitriah</a><br>
                                                <p>
                                                    CIMB<br>
                                                    100039278<br>
                                                </p>
                                                <p>
                                                    Contact: 0125391715<br>
                                                    Email: nurfitriah@unimap.edu.my
                                                </p>
                                            </td>
                                            <td>4210</td>
                                            <td>12/12/2014</td>
                                            <td>Paid</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <a href="" class="btn btn-primary"> Print list <i class="fa fa-arrow-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.row -->